// This file is part of lightsvc Java Microservices Components (lightsvc).
//
// lightsvc Java Microservices Components (lightsvc) is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lightsvc Java Microservices Components (lightsvc) is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with lightsvc Java Microservices Components (lightsvc). If not, see <http://www.gnu.org/licenses/>.
package lightsvc.err;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.io.Serializable;
import java.util.function.Function;

import javax.annotation.processing.Generated;

import org.junit.Test;

import lightsvc.err.Err.TranslationBuilder;
import lightsvc.util.Arrays;

@SuppressWarnings({
  "javadoc", "static-method"
})
@Generated("junit")
public class ErrTestCase {

  protected Serializable expectedData;
  protected Serializable expectedCausedByData;

  public static enum ErrTest1 {
    someParam1_mustNotHaveError1,
    someParam1_mustNotHaveError2,
    someParam1_mustNotHaveError3,
    identicalParam_mustNotHaveError,
  }

  public static enum ErrTest2 {
    someParam2_mustNotHaveError3,
    someParam2_mustNotHaveError4,
    someParam2_mustNotHaveError5,
    someParam3_mustNotHaveError6,
    identicalParam_mustNotHaveError,
  }

  public ErrTestCase() {
    this.expectedData = "expectedData";
    this.expectedCausedByData = "expectedCausedByData";
  }

  @Test
  public void messageFromEnumValue_mustReturnConcatenatedEnumClassNameAndEnumName_whenValueIsEnumValue() {
    var value = ErrTest1.someParam1_mustNotHaveError1;
    var expectedOutput = "lightsvc.err.ErrTestCase.ErrTest1.someParam1_mustNotHaveError1";
    var returnedValue = Err.messageFromEnumValue(value);
    assertEquals(expectedOutput, returnedValue);
  }

  @Test
  public void messageFromEnumValue_mustReturnNull_whenValueIsNull() {
    var value = (ErrTest1) null;
    var returnedValue = Err.messageFromEnumValue(value);
    assertNull(returnedValue);
  }

  @Test
  public void new_mustReturnInstance_whenAllParametersAreUsed() throws Err {
    var cause = new RuntimeException("Cause");
    var value = ErrTest1.someParam1_mustNotHaveError1;
    var err = new Err(value, this.expectedData, cause);
    assertEquals(Err.messageFromEnumValue(value), err.getMessage());
    assertSame(cause, err.getCause());
    assertSame(value, err.value);
    assertSame(this.expectedData, err.data);
    assertNotNull(err.getStackTrace());
    assertEquals(0, err.getStackTrace().length);
  }

  @Test
  public void new_mustReturnInstance_whenValueAndExtrainfoAreUsed() throws Err {
    var value = ErrTest1.someParam1_mustNotHaveError1;
    var err = new Err(value, this.expectedData, null);
    assertEquals(Err.messageFromEnumValue(value), err.getMessage());
    assertNull(err.getCause());
    assertSame(value, err.value);
    assertSame(this.expectedData, err.data);
    assertNotNull(err.getStackTrace());
    assertEquals(ErrTestCase.class.getName(), err.getStackTrace()[0].getClassName());
    assertEquals("new_mustReturnInstance_whenValueAndExtrainfoAreUsed", err.getStackTrace()[0].getMethodName());
  }

  @Test
  public void new_mustReturnInstance_whenOnlyValueIsUsed() throws Err {
    var value = ErrTest1.someParam1_mustNotHaveError1;
    var err = new Err(value);
    assertEquals(Err.messageFromEnumValue(value), err.getMessage());
    assertNull(err.getCause());
    assertSame(value, err.value);
    assertNull(err.data);
    assertNotNull(err.getStackTrace());
    assertEquals(ErrTestCase.class.getName(), err.getStackTrace()[0].getClassName());
    assertEquals("new_mustReturnInstance_whenOnlyValueIsUsed", err.getStackTrace()[0].getMethodName());
  }

  @Test
  public void new_mustReturnInstance_whenValueAndCauseAreUsed() throws Err {
    var cause = new RuntimeException("Cause");
    var value = ErrTest1.someParam1_mustNotHaveError1;
    var err = new Err(value, null, cause);
    assertEquals(Err.messageFromEnumValue(value), err.getMessage());
    assertSame(cause, err.getCause());
    assertSame(value, err.value);
    assertNull(err.data);
    assertNotNull(err.getStackTrace());
    assertEquals(0, err.getStackTrace().length);
  }

  @Test
  public void new_mustThrowErrNewValueMustNotBeNull_whenValueIsNull() {
    var cause = new RuntimeException("Cause");
    var value = (ErrTest1) null;
    var thrownException = assertThrows(Err.class, ()->new Err(value, "testConstructor.success", cause));
    assertEquals(Err.ErrNew.value_mustNotBeNull, thrownException.value);
  }

  @Test
  public void hasValue_mustThrowErrHasValueValuesmustNotBeEmpty_whenValuesIsNull() {
    var values = (ErrTest1[]) null;
    var thrownException = assertThrows(Err.class, ()->Err.hasValue(values));
    assertEquals(Err.ErrHasValue.values_mustNotBeEmpty, thrownException.value);
  }

  @Test
  public void hasValue_mustThrowErrHasValueValuesmustNotBeEmpty_whenValuesIsEmpty() {
    var values = new ErrTest1[] {};
    var thrownException = assertThrows(Err.class, ()->Err.hasValue(values));
    assertEquals(Err.ErrHasValue.values_mustNotBeEmpty, thrownException.value);
  }

  @Test
  public void hasValue_mustThrowErrHasValueValuesItemsMustBeNotNull_whenValuesItemIsNull() {
    var values = Arrays.of(ErrTest1.someParam1_mustNotHaveError1, null);
    var thrownException = assertThrows(Err.class, ()->Err.hasValue(values));
    assertEquals(Err.ErrHasValue.values_items_mustNotBeNull, thrownException.value);
  }

  @Test
  public void hasValue_mustReturnPredicate_whenValuesIsNotEmptyAndNotContainsNullItems() throws Err {
    var values = Arrays.of(ErrTest1.someParam1_mustNotHaveError1, ErrTest1.someParam1_mustNotHaveError2);
    var returnedPredicate = Err.hasValue(values);
    assertTrue(java.util.function.Predicate.class.isAssignableFrom(returnedPredicate.getClass()));
  }

  @Test
  public void hasValue_test_mustReturnTrue_whenExceptionValueMatchesFirstItemInValues() throws Err {
    var values = Arrays.of(ErrTest1.someParam1_mustNotHaveError1, ErrTest1.someParam1_mustNotHaveError2);
    var exceptionValueMatchesFirstItemInValues = new Err(ErrTest1.someParam1_mustNotHaveError1);
    assertTrue(Err.hasValue(values).test(exceptionValueMatchesFirstItemInValues));
  }

  @Test
  public void hasValue_test_mustReturnTrue_whenExceptionValueMatchesSecondItemInValues() throws Err {
    var values = Arrays.of(ErrTest1.someParam1_mustNotHaveError1, ErrTest1.someParam1_mustNotHaveError2);
    var exceptionValueMatchesSecondItemInValues = new Err(ErrTest1.someParam1_mustNotHaveError2);
    assertTrue(Err.hasValue(values).test(exceptionValueMatchesSecondItemInValues));
  }

  @Test
  public void hasValue_test_mustReturnFalse_whenExceptionValueDoesNotMatchesAnyItemInValues() throws Err {
    var values = Arrays.of(ErrTest1.someParam1_mustNotHaveError1, ErrTest1.someParam1_mustNotHaveError2);
    var exceptionValueDoesNotMatchesAnyItemInValues = new Err(ErrTest2.someParam2_mustNotHaveError3);
    assertFalse(Err.hasValue(values).test(exceptionValueDoesNotMatchesAnyItemInValues));
  }

  @Test
  public void hasValue_test_mustReturnFalse_whenExceptionValueIsNotErr() throws Err {
    var values = Arrays.of(ErrTest1.someParam1_mustNotHaveError1, ErrTest1.someParam1_mustNotHaveError2);
    var exceptionValueIsNotErr = new RuntimeException();
    assertFalse(Err.hasValue(values).test(exceptionValueIsNotErr));
  }

  @Test
  public void from_mustThrowErrFromValuesMustNotBeEmpty_whenValuesIsNull() {
    var fromValue = (ErrTest1[]) null;
    var thrownException = assertThrows(Err.class, ()->Err.from(fromValue));
    assertEquals(Err.ErrFrom.values_mustNotBeEmpty, thrownException.value);
  }

  @Test
  public void from_mustThrowErrFromValuesMustNotBeEmpty_whenValuesIsEmpty() {
    var fromValue = new ErrTest1[] {};
    var thrownException = assertThrows(Err.class, ()->Err.from(fromValue));
    assertEquals(Err.ErrFrom.values_mustNotBeEmpty, thrownException.value);
  }

  @Test
  public void from_mustThrowErrFromValuesItemsMustNotBeNull_whenValuesContainsNullOnFirstItem() {
    var fromValue = new ErrTest1[] {
      ErrTest1.someParam1_mustNotHaveError1, null
    };
    var thrownException = assertThrows(Err.class, ()->Err.from(fromValue));
    assertEquals(Err.ErrFrom.values_items_mustNotBeNull, thrownException.value);
  }

  @Test
  public void from_mustThrowErrFromValuesItemsMustNotBeNull_whenValuesContainsNullOnSecondItem() {
    var fromValue = new ErrTest1[] {
      null, ErrTest1.someParam1_mustNotHaveError1
    };
    var thrownException = assertThrows(Err.class, ()->Err.from(fromValue));
    assertEquals(Err.ErrFrom.values_items_mustNotBeNull, thrownException.value);
  }

  @Test
  public void from_mustReturnBuilder_whenValuesContainsEnums() throws Err {
    var value = ErrTest1.someParam1_mustNotHaveError1;
    var returnedBuilder = Err.from(value);
    assertNotNull(returnedBuilder);
    assertSame(Err.TranslationBuilder.class, returnedBuilder.getClass());
    assertTrue(Exceptions.Translation.Builder.class.isAssignableFrom(returnedBuilder.getClass()));
  }

  @Test
  public void translationBuilder_new_mustThrowErrFromValuesMustNotBeEmpty_whenValuesIsNull() {
    var fromValue = (ErrTest1[]) null;
    var thrownException = assertThrows(Err.class, ()->new TranslationBuilder(fromValue));
    assertEquals(Err.TranslationBuilder.ErrNew.values_mustNotBeEmpty, thrownException.value);
  }

  @Test
  public void translationBuilder_new_mustThrowErrFromValuesMustNotBeEmpty_whenValuesIsEmpty() {
    var fromValue = new ErrTest1[] {};
    var thrownException = assertThrows(Err.class, ()->new TranslationBuilder(fromValue));
    assertEquals(Err.TranslationBuilder.ErrNew.values_mustNotBeEmpty, thrownException.value);
  }

  @Test
  public void translationBuilder_new_mustThrowErrFromValuesItemsMustNotBeNull_whenValuesContainsNullOnFirstItem() {
    var fromValue = new ErrTest1[] {
      ErrTest1.someParam1_mustNotHaveError1, null
    };
    var thrownException = assertThrows(Err.class, ()->new TranslationBuilder(fromValue));
    assertEquals(Err.TranslationBuilder.ErrNew.values_items_mustNotBeNull, thrownException.value);
  }

  @Test
  public void translationBuilder_new_mustThrowErrFromValuesItemsMustNotBeNull_whenValuesContainsNullOnSecondItem() {
    var fromValue = new ErrTest1[] {
      null, ErrTest1.someParam1_mustNotHaveError1
    };
    var thrownException = assertThrows(Err.class, ()->new TranslationBuilder(fromValue));
    assertEquals(Err.TranslationBuilder.ErrNew.values_items_mustNotBeNull, thrownException.value);
  }

  @Test
  public void translationBuilder_test_mustReturnTrue_whenErrMatchesSingletonValues() throws Err {
    var singletonValues = ErrTest1.someParam1_mustNotHaveError1;
    var err = new Err(singletonValues, this.expectedData, null);
    assertTrue(Err.from(singletonValues).to(ErrTest2.someParam2_mustNotHaveError3).test(err));
  }

  @Test
  public void translationBuilder_test_mustReturnFalse_whenErrNotMatchingSingletonValues() throws Err {
    var singletonValue = ErrTest1.someParam1_mustNotHaveError1;
    var notMatchingSingletonValue = ErrTest1.someParam1_mustNotHaveError2;
    var errNotMatchingSingletonValue = new Err(notMatchingSingletonValue, this.expectedData, null);
    assertFalse(Err.from(singletonValue).to(ErrTest2.someParam2_mustNotHaveError3).test(errNotMatchingSingletonValue));
  }

  @Test
  public void translationBuilder_test_mustReturnTrue_whenErrMatchesAValueInValues() throws Err {
    var value1 = ErrTest1.someParam1_mustNotHaveError1;
    var value2 = ErrTest1.someParam1_mustNotHaveError2;
    var errMatchesAValueInValues = new Err(value2, this.expectedData, null);
    assertTrue(Err.from(value1, value2).to(ErrTest2.someParam2_mustNotHaveError3).test(errMatchesAValueInValues));
  }

  @Test
  public void translationBuilder_test_mustReturnFalse_whenErrNotMatchesAValueInValues() throws Err {
    var value1 = ErrTest1.someParam1_mustNotHaveError1;
    var value2 = ErrTest1.someParam1_mustNotHaveError2;
    var notMatchesAValueInValues = ErrTest1.someParam1_mustNotHaveError3;
    var errNotMatchesAValueInValues = new Err(notMatchesAValueInValues, this.expectedData, null);
    assertFalse(Err.from(value1, value2).to(ErrTest2.someParam2_mustNotHaveError3).test(errNotMatchesAValueInValues));
  }

  @Test
  public void translationBuilder_to_mustThrowErrToValueMustBeNotNull_whenToValueIsNullAndDataFnIsNotNull() throws Err {
    var fromValue = ErrTest1.someParam1_mustNotHaveError1;
    var toValue = (ErrTest2) null;
    var toDataFn = (Function<Err, String>) e->"AttachedData";
    var thrownException = assertThrows(Err.class, ()->Err.from(fromValue).to(toValue, toDataFn));
    assertEquals(Err.TranslationBuilder.ErrTo.value_mustNotBeNull, thrownException.value);
  }

  @Test
  public void translationBuilder_to_mustThrowErrToDataFnMustBeNotNull_whenToValueIsNullAndDataFnIsNotNull() throws Err {
    var fromValue = ErrTest1.someParam1_mustNotHaveError1;
    var toValue = ErrTest2.someParam2_mustNotHaveError3;
    var toDataFn = (Function<Err, String>) null;
    var thrownException = assertThrows(Err.class, ()->Err.from(fromValue).to(toValue, toDataFn));
    assertEquals(Err.TranslationBuilder.ErrTo.dataFn_mustNotBeNull, thrownException.value);
  }

  @Test
  public void translationBuilder_to_mustThrowErrToValueMustBeNotNull_whenToValueIsNullAndDataIsNotNull() throws Err {
    var fromValue = ErrTest1.someParam1_mustNotHaveError1;
    var toValue = (ErrTest2) null;
    var toData = "AttachedData";
    var thrownException = assertThrows(Err.class, ()->Err.from(fromValue).to(toValue, toData));
    assertEquals(Err.TranslationBuilder.ErrTo.value_mustNotBeNull, thrownException.value);
  }

  @Test
  public void translationBuilder_to_mustThrowErrToValueMustBeNotNull_whenToValueIsNull() throws Err {
    var fromValue = ErrTest1.someParam1_mustNotHaveError1;
    var toValue = (ErrTest2) null;
    var thrownException = assertThrows(Err.class, ()->Err.from(fromValue).to(toValue));
    assertEquals(Err.TranslationBuilder.ErrTo.value_mustNotBeNull, thrownException.value);
  }

  @Test
  public void translationBuilder_to_mustReturnExceptionsTranslationInstance_whenValueIsNotNullAndDataProviderIsNull() throws Err {
    var value = ErrTest1.someParam1_mustNotHaveError1;
    var translatedValue = ErrTest2.someParam2_mustNotHaveError3;
    var translation = Err.from(value).to(translatedValue);
    assertNotNull(translation);
    assertSame(Exceptions.Translation.class, translation.getClass());
  }

  @Test
  public void translationBuilder_to_mustReturnExceptionsTranslationInstance_whenValueIsNotNullAndDataProviderIsNotNull() throws Err {
    var value = ErrTest1.someParam1_mustNotHaveError1;
    var translatedValue = ErrTest2.someParam2_mustNotHaveError3;
    var translation = Err.from(value).to(translatedValue, err->"Some Metadata");
    assertNotNull(translation);
    assertSame(Exceptions.Translation.class, translation.getClass());
  }

  @Test
  public void translationBuilder_to_mustThrowErrToValueMustNotBeNull_whenValueIsNull() {
    var value = ErrTest1.someParam1_mustNotHaveError1;
    var translatedValue = (ErrTest2) null;
    var thrownException = assertThrows(Err.class, ()->Err.from(value).to(translatedValue));
    assertEquals(Err.TranslationBuilder.ErrTo.value_mustNotBeNull, thrownException.value);
  }

  @Test
  public void translationBuilder_to_apply_mustReturnErrWithValueAndCause_whenTranslatedValueIsNotNullAndDataIsNullAndExceptionIsNotNull() throws Err {
    var value = ErrTest1.someParam1_mustNotHaveError1;
    var translatedValue = ErrTest2.someParam2_mustNotHaveError3;
    var nullData = (String) null;
    var translation = Err.from(value).to(translatedValue);
    var causeException = new Err(value);
    var translatedErr = translation.toFn().apply(causeException);
    assertSame(Err.class, translatedErr.getClass());
    assertSame(translatedValue, translatedErr.value);
    assertNull(nullData);
    assertSame(causeException, translatedErr.getCause());
  }

  @Test
  public void translationBuilder_to_apply_mustReturnErrWithValueAndCause_whenTranslatedValueIsNotNullAndDataIsFunctionAndExceptionIsNotNull() throws Err {
    var value = ErrTest1.someParam1_mustNotHaveError1;
    var translatedValue = ErrTest2.someParam2_mustNotHaveError3;
    var notNullData = "notNullData";
    var translation = Err.from(value).to(translatedValue, err->notNullData);
    var causeException = new Err(value, notNullData, null);
    var translatedErr = translation.toFn().apply(causeException);
    assertSame(Err.class, translatedErr.getClass());
    assertSame(translatedValue, translatedErr.value);
    assertSame(notNullData, translatedErr.data);
    assertSame(causeException, translatedErr.getCause());
  }

  @Test
  public void translationBuilder_to_apply_mustReturnErrWithValueAndCause_whenTranslatedValueIsNotNullAndDataIsSerializableAndExceptionIsNotNull() throws Err {
    var value = ErrTest1.someParam1_mustNotHaveError1;
    var translatedValue = ErrTest2.someParam2_mustNotHaveError3;
    var notNullData = "notNullData";
    var translation = Err.from(value).to(translatedValue, notNullData);
    var causeException = new Err(value, notNullData, null);
    var translatedErr = translation.toFn().apply(causeException);
    assertSame(Err.class, translatedErr.getClass());
    assertSame(translatedValue, translatedErr.value);
    assertSame(notNullData, translatedErr.data);
    assertSame(causeException, translatedErr.getCause());
  }

}
