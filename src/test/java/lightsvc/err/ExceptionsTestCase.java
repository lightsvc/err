// This file is part of lightsvc Java Microservices Components (lightsvc).
//
// lightsvc Java Microservices Components (lightsvc) is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lightsvc Java Microservices Components (lightsvc) is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with lightsvc Java Microservices Components (lightsvc). If not, see <http://www.gnu.org/licenses/>.
package lightsvc.err;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.io.EOFException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import javax.annotation.processing.Generated;

import org.junit.Test;

import lightsvc.err.Exceptions.Supplier;

@SuppressWarnings({
  "javadoc", "static-method"
})
@Generated("junit")
public class ExceptionsTestCase {

  @Test
  public void new_mustThrowUnsupportedOperationException_whenCalled() throws NoSuchMethodException, SecurityException {
    var constructor = Exceptions.class.getDeclaredConstructor();
    constructor.setAccessible(true);
    var ite = assertThrows(InvocationTargetException.class, ()->constructor.newInstance());
    assertSame(UnsupportedOperationException.class, ite.getCause().getClass());
  }

  @Test
  public void rootCause_mustReturnNull_whenThrowableIsNull() {
    var throwable = (Throwable) null;
    assertNull(Exceptions.rootCause(throwable));
  }

  @Test
  public void rootCause_mustReturnNull_whenThrowableHasNoCause() {
    var throwableHasNoCause = new RuntimeException();
    assertSame(throwableHasNoCause, Exceptions.rootCause(throwableHasNoCause));
  }

  @Test
  public void rootCause_mustReturnNull_whenThrowableHasCause() {
    var causeLast = new RuntimeException();
    var causeMiddle = new RuntimeException(causeLast);
    var throwableHasCause = new RuntimeException(causeMiddle);
    assertSame(causeLast, Exceptions.rootCause(throwableHasCause));
  }

  @Test
  public void shortStackTraceString_mustThrowNullPointerException_whenExceptionIsNull() {
    var exception = (Exception) null;
    assertThrows(NullPointerException.class, ()->Exceptions.shortStackTraceString(exception));
    assertThrows(NullPointerException.class, ()->Exceptions.shortStackTraceString(exception, "lightsvc"));
  }

  @Test
  public void shortStackTraceString_mustReturnExpectedString_whenExceptionIsNotNullAndHasCauseFilterPackagesIsEmpty() {
    try {
      try {
        try {
          throw new RuntimeException("root");
        } catch (Exception e) {
          throw new RuntimeException("middle", e);
        }
      } catch (Exception e) {
        throw new RuntimeException("", e);
      }
    } catch (Exception e) {
      var returnedValue = Exceptions.shortStackTraceString(e);
      assertTrue(Pattern.matches("([\\p{Alnum}\\\\.]*(: \\\"[^\\\"]*\\\")?( \\(\\p{Alnum}+\\.java:[\\d-]+\\))*( >> )?)+", returnedValue));
    }
  }

  @Test
  public void shortStackTraceString_mustReturnExpectedString_whenExceptionIsNotNullAndNotHasCauseFilterPackagesIsEmpty() {
    try {
      throw new RuntimeException("root");
    } catch (Exception e) {
      var returnedValue = Exceptions.shortStackTraceString(e);
      assertTrue(Pattern.matches("([\\p{Alnum}\\\\.]*(: \\\"[^\\\"]*\\\")?( \\(\\p{Alnum}+\\.java:[\\d-]+\\))*( >> )?)+", returnedValue));
    }
  }

  @Test
  public void shortStackTraceString_mustReturnExpectedString_whenExceptionIsNotNullAndHasCauseAndFilterPackagesIsNotEmpty() {
    try {
      try {
        try {
          throw new RuntimeException("root");
        } catch (Exception e) {
          throw new RuntimeException("middle", e);
        }
      } catch (Exception e) {
        throw new RuntimeException("", e);
      }
    } catch (Exception e) {
      var returnedValue = Exceptions.shortStackTraceString(e, "lightsvc");
      assertTrue(Pattern.matches("([\\p{Alnum}\\\\.]*(: \\\"[^\\\"]*\\\")?( \\(\\p{Alnum}+\\.java:[\\d-]+\\))*( >> )?)+", returnedValue));
    }
  }

  @Test
  public void notHappen_mustReturnNull_whenExceptionIsNull() {
    var exception = (Exception) null;
    var returnedException = Exceptions.shouldNotHappen(exception);
    assertNull(returnedException);
  }

  @Test
  public void notHappen_mustReturnUncauthExceptionWithWrappedSameException_whenExceptionIsRuntimeException() {
    var exception = new RuntimeException();
    var returnedException = Exceptions.shouldNotHappen(exception);
    assertSame(exception, returnedException.getCause());
  }

  @Test
  public void notHappen_mustReturnUncaughtExceptionWithWrappedException_whenExceptionIsCheckedException() {
    var exception = new Exception();
    var returnedException = Exceptions.shouldNotHappen(exception);
    assertEquals(UncaughtException.class, returnedException.getClass());
    assertSame(exception, returnedException.getCause());
  }

  @Test
  public void notHappen_mustReturnSameUncaughtException_whenExceptionIsUncaughtException() {
    var uncaughtException = new UncaughtException(new RuntimeException());
    var returnedException = Exceptions.shouldNotHappen(uncaughtException);
    assertSame(uncaughtException, returnedException);
  }

  @Test
  public void notHappen_mustReturnNull_whenSupplierIsNull() {
    var supplier = (Supplier<String, Exception>) null;
    var returnedException = Exceptions.shouldNotHappen(supplier);
    assertNull(returnedException);
  }

  @Test
  public void notHappen_mustReturnSupplierValue_whenSupplierDoNotThrowException() {
    var booleanValue = true;
    var returnedBooleanValue = Exceptions.shouldNotHappen(()->booleanValue);
    assertTrue(booleanValue == returnedBooleanValue);
    var byteValue = (byte) 2;
    var returnedByteValue = Exceptions.shouldNotHappen(()->byteValue);
    assertEquals(byteValue, returnedByteValue);
    var shortValue = (short) 3;
    var returnedShortValue = Exceptions.shouldNotHappen(()->shortValue);
    assertEquals(shortValue, returnedShortValue);
    var charValue = 'D';
    var returnedCharValue = Exceptions.shouldNotHappen(()->charValue);
    assertEquals(charValue, returnedCharValue);
    var intValue = 5;
    var returnedIntValue = Exceptions.shouldNotHappen(()->intValue);
    assertEquals(intValue, returnedIntValue);
    var longValue = 6L;
    var returnedLongValue = Exceptions.shouldNotHappen(()->longValue);
    assertEquals(longValue, returnedLongValue);
    var floatValue = 7.5f;
    var returnedFloatValue = Exceptions.shouldNotHappen(()->floatValue);
    assertEquals(floatValue, returnedFloatValue, 0f);
    var doubleValue = 8.5d;
    var returnedDoubleValue = Exceptions.shouldNotHappen(()->doubleValue);
    assertEquals(doubleValue, returnedDoubleValue, 0d);
    var objectValue = new Object();
    var returnedObjectValue = Exceptions.shouldNotHappen(()->objectValue);
    assertEquals(objectValue, returnedObjectValue);
    var stringValue = "10";
    var returnedStringValue = Exceptions.shouldNotHappen(()->stringValue);
    assertEquals(stringValue, returnedStringValue);
  }

  @Test
  public void notHappen_mustThrowUncaughtExceptionWithCauseSupplierRuntimeException_whenSupplierThrowRuntimeException() {
    var alwaysThrowCondition = true;
    var runtimeException = new RuntimeException();
    var booleanValue = true;
    var returnedBooleanException = assertThrows(UncaughtException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw runtimeException;
      }
      return booleanValue;
    }));
    assertSame(UncaughtException.class, returnedBooleanException.getClass());
    assertSame(runtimeException, returnedBooleanException.getCause());
    var byteValue = (byte) 2;
    var returnedByteException = assertThrows(UncaughtException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw runtimeException;
      }
      return byteValue;
    }));
    assertSame(UncaughtException.class, returnedByteException.getClass());
    assertSame(runtimeException, returnedByteException.getCause());
    var shortValue = (short) 3;
    var returnedShortException = assertThrows(UncaughtException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw runtimeException;
      }
      return shortValue;
    }));
    assertSame(UncaughtException.class, returnedShortException.getClass());
    assertSame(runtimeException, returnedShortException.getCause());
    var charValue = 'D';
    var returnedCharException = assertThrows(UncaughtException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw runtimeException;
      }
      return charValue;
    }));
    assertSame(UncaughtException.class, returnedCharException.getClass());
    assertSame(runtimeException, returnedCharException.getCause());
    var intValue = 5;
    var returnedIntException = assertThrows(UncaughtException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw runtimeException;
      }
      return intValue;
    }));
    assertSame(UncaughtException.class, returnedIntException.getClass());
    assertSame(runtimeException, returnedIntException.getCause());
    var longValue = 6L;
    var returnedLongException = assertThrows(UncaughtException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw runtimeException;
      }
      return longValue;
    }));
    assertSame(UncaughtException.class, returnedLongException.getClass());
    assertSame(runtimeException, returnedLongException.getCause());
    var floatValue = 7.5f;
    var returnedFloatException = assertThrows(UncaughtException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw runtimeException;
      }
      return floatValue;
    }));
    assertSame(UncaughtException.class, returnedFloatException.getClass());
    assertSame(runtimeException, returnedFloatException.getCause());
    var doubleValue = 8.5d;
    var returnedDoubleException = assertThrows(UncaughtException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw runtimeException;
      }
      return doubleValue;
    }));
    assertSame(UncaughtException.class, returnedDoubleException.getClass());
    assertSame(runtimeException, returnedDoubleException.getCause());
    var objectValue = new Object();
    var returnedObjectException = assertThrows(UncaughtException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw runtimeException;
      }
      return objectValue;
    }));
    assertSame(UncaughtException.class, returnedObjectException.getClass());
    assertSame(runtimeException, returnedObjectException.getCause());
    var stringValue = "10";
    var returnedStringException = assertThrows(UncaughtException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw runtimeException;
      }
      return stringValue;
    }));
    assertSame(UncaughtException.class, returnedStringException.getClass());
    assertSame(runtimeException, returnedStringException.getCause());
  }

  @Test
  public void notHappen_mustThrowUncaughtExceptionWithCauseSupplierRuntimeException_whenSupplierThrowCheckedException() {
    var alwaysThrowCondition = true;
    var checkedException = new Exception();
    var booleanValue = true;
    var returnedBooleanException = assertThrows(RuntimeException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw checkedException;
      }
      return booleanValue;
    }));
    assertSame(UncaughtException.class, returnedBooleanException.getClass());
    assertSame(checkedException, returnedBooleanException.getCause());
    var byteValue = (byte) 2;
    var returnedByteException = assertThrows(RuntimeException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw checkedException;
      }
      return byteValue;
    }));
    assertSame(UncaughtException.class, returnedByteException.getClass());
    assertSame(checkedException, returnedByteException.getCause());
    var shortValue = (short) 3;
    var returnedShortException = assertThrows(RuntimeException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw checkedException;
      }
      return shortValue;
    }));
    assertSame(UncaughtException.class, returnedShortException.getClass());
    assertSame(checkedException, returnedShortException.getCause());
    var charValue = 'D';
    var returnedCharException = assertThrows(RuntimeException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw checkedException;
      }
      return charValue;
    }));
    assertSame(UncaughtException.class, returnedCharException.getClass());
    assertSame(checkedException, returnedCharException.getCause());
    var intValue = 5;
    var returnedIntException = assertThrows(RuntimeException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw checkedException;
      }
      return intValue;
    }));
    assertSame(UncaughtException.class, returnedIntException.getClass());
    assertSame(checkedException, returnedIntException.getCause());
    var longValue = 6L;
    var returnedLongException = assertThrows(RuntimeException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw checkedException;
      }
      return longValue;
    }));
    assertSame(UncaughtException.class, returnedLongException.getClass());
    assertSame(checkedException, returnedLongException.getCause());
    var floatValue = 7.5f;
    var returnedFloatException = assertThrows(RuntimeException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw checkedException;
      }
      return floatValue;
    }));
    assertSame(UncaughtException.class, returnedFloatException.getClass());
    assertSame(checkedException, returnedFloatException.getCause());
    var doubleValue = 8.5d;
    var returnedDoubleException = assertThrows(RuntimeException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw checkedException;
      }
      return doubleValue;
    }));
    assertSame(UncaughtException.class, returnedDoubleException.getClass());
    assertSame(checkedException, returnedDoubleException.getCause());
    var objectValue = new Object();
    var returnedObjectException = assertThrows(RuntimeException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw checkedException;
      }
      return objectValue;
    }));
    assertSame(UncaughtException.class, returnedObjectException.getClass());
    assertSame(checkedException, returnedObjectException.getCause());
    var stringValue = "10";
    var returnedStringException = assertThrows(RuntimeException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw checkedException;
      }
      return stringValue;
    }));
    assertSame(UncaughtException.class, returnedStringException.getClass());
    assertSame(checkedException, returnedStringException.getCause());
  }

  @Test
  public void notHappen_mustReturn_whenRunnableDoNotThrowException() {
    Exceptions.shouldNotHappen(()-> {
      // Do nothing
    });
  }

  @Test
  public void notHappen_mustThrowUncaughtExceptionWithWrappedRuntimeException_whenRunnableThrowRuntimeException() {
    var alwaysThrowCondition = true;
    var runtimeException = new RuntimeException();
    var thrownUncaughtExceptionWithWrappedRuntimeException = assertThrows(UncaughtException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw runtimeException;
      }
    }));
    assertSame(runtimeException, thrownUncaughtExceptionWithWrappedRuntimeException.getCause());
  }

  @Test
  public void notHappen_mustThrowUncaughtExceptionWithWrappedCheckedException_whenRunnableThrowCheckedException() {
    var alwaysThrowCondition = true;
    var checkedException = new Exception();
    var returnedBooleanException = assertThrows(RuntimeException.class, ()->Exceptions.shouldNotHappen(()-> {
      if (alwaysThrowCondition) {
        throw checkedException;
      }
    }));
    assertSame(checkedException, returnedBooleanException.getCause());
  }

  @Test
  public void translate_mustReturnSupplierReturnValue_whenSupplierNotThrowsException() throws NullPointerException, UncaughtException, Exception {
    var exception = new Exception("Exception");
    var firstExceptionTranslationException = new Exception("firstExceptionTranslationException");
    var secondExceptionTranslationException = new Exception("secondExceptionTranslationException");
    // boolean
    var booleanSupplierReturnValue = true;
    var returnedBooleanValue = Exceptions.translateBoolean(
      ()->booleanSupplierReturnValue,
      new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
      new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
    );
    assertTrue(booleanSupplierReturnValue == returnedBooleanValue);
    // byte
    var byteSupplierReturnValue = (byte) 2;
    var returnedByteValue = Exceptions.translateByte(
      ()->byteSupplierReturnValue,
      new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
      new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
    );
    assertEquals(byteSupplierReturnValue, returnedByteValue);
    // short
    var shortSupplierReturnValue = (short) 3;
    var returnedShortValue = Exceptions.translateShort(
      ()->shortSupplierReturnValue,
      new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
      new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
    );
    assertEquals(shortSupplierReturnValue, returnedShortValue);
    // char
    var charSupplierReturnValue = 'D';
    var returnedCharValue = Exceptions.translateChar(
      ()->charSupplierReturnValue,
      new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
      new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
    );
    assertEquals(charSupplierReturnValue, returnedCharValue);
    // int
    var intSupplierReturnValue = 5;
    var returnedIntValue = Exceptions.translateInt(
      ()->intSupplierReturnValue,
      new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
      new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
    );
    assertEquals(intSupplierReturnValue, returnedIntValue);
    // long
    var longSupplierReturnValue = 6L;
    var returnedLongValue = Exceptions.translateLong(
      ()->longSupplierReturnValue,
      new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
      new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
    );
    assertEquals(longSupplierReturnValue, returnedLongValue);
    // float
    var floatSupplierReturnValue = 7.5f;
    var returnedFloatValue = Exceptions.translateFloat(
      ()->floatSupplierReturnValue,
      new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
      new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
    );
    assertEquals(floatSupplierReturnValue, returnedFloatValue, 0f);
    // double
    var doubleSupplierReturnValue = 8.5d;
    var returnedDoubleValue = Exceptions.translateDouble(
      ()->doubleSupplierReturnValue,
      new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
      new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
    );
    assertEquals(doubleSupplierReturnValue, returnedDoubleValue, 0d);
    // object
    var objectSupplierReturnValue = "9";
    var returnedObjectValue = Exceptions.translate(
      ()->objectSupplierReturnValue,
      new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
      new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
    );
    assertSame(objectSupplierReturnValue, returnedObjectValue);
    Exceptions.translate(()-> {
      // Return Nothing.
    },
      new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
      new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
    );
  }

  @Test
  public void translate_mustReturnTypeDefaultValue_whenSupplierThrowsExceptionAndTranslationToNull() {
    var alwaysThrowCondition = true;
    var exception = new Exception("Exception");
    // boolean
    var booleanSupplierReturnValue = true;
    var returnedBooleanValue = Exceptions.translateBoolean(()-> {
      if (alwaysThrowCondition) {
        throw exception;
      }
      return booleanSupplierReturnValue;
    }, new Exceptions.Translation<>(e->e != exception, e->new RuntimeException()), new Exceptions.Translation<>(e->e == exception, e->null));
    assertFalse(returnedBooleanValue);
    // byte
    var byteSupplierReturnValue = (byte) 2;
    var returnedByteValue = Exceptions.translateByte(()-> {
      if (alwaysThrowCondition) {
        throw exception;
      }
      return byteSupplierReturnValue;
    }, new Exceptions.Translation<>(e->e != exception, e->new RuntimeException()), new Exceptions.Translation<>(e->e == exception, e->null));
    assertEquals((byte) 0, returnedByteValue);
    // short
    var shortSupplierReturnValue = (short) 3;
    var returnedShortValue = Exceptions.translateShort(()-> {
      if (alwaysThrowCondition) {
        throw exception;
      }
      return shortSupplierReturnValue;
    }, new Exceptions.Translation<>(e->e != exception, e->new RuntimeException()), new Exceptions.Translation<>(e->e == exception, e->null));
    assertEquals((short) 0, returnedShortValue);
    // char
    var charSupplierReturnValue = 'D';
    var returnedCharValue = Exceptions.translateChar(()-> {
      if (alwaysThrowCondition) {
        throw exception;
      }
      return charSupplierReturnValue;
    }, new Exceptions.Translation<>(e->e != exception, e->new RuntimeException()), new Exceptions.Translation<>(e->e == exception, e->null));
    assertEquals('\0', returnedCharValue);
    // int
    var intSupplierReturnValue = 5;
    var returnedIntValue = Exceptions.translateInt(()-> {
      if (alwaysThrowCondition) {
        throw exception;
      }
      return intSupplierReturnValue;
    }, new Exceptions.Translation<>(e->e != exception, e->new RuntimeException()), new Exceptions.Translation<>(e->e == exception, e->null));
    assertEquals(0, returnedIntValue);
    // long
    var longSupplierReturnValue = 6L;
    var returnedLongValue = Exceptions.translateLong(()-> {
      if (alwaysThrowCondition) {
        throw exception;
      }
      return longSupplierReturnValue;
    }, new Exceptions.Translation<>(e->e != exception, e->new RuntimeException()), new Exceptions.Translation<>(e->e == exception, e->null));
    assertEquals(0L, returnedLongValue);
    // float
    var floatSupplierReturnValue = 7.5f;
    var returnedFloatValue = Exceptions.translateFloat(()-> {
      if (alwaysThrowCondition) {
        throw exception;
      }
      return floatSupplierReturnValue;
    }, new Exceptions.Translation<>(e->e != exception, e->new RuntimeException()), new Exceptions.Translation<>(e->e == exception, e->null));
    assertEquals(0f, returnedFloatValue, 0f);
    // double
    var doubleSupplierReturnValue = 8.5d;
    var returnedDoubleValue = Exceptions.translateDouble(()-> {
      if (alwaysThrowCondition) {
        throw exception;
      }
      return doubleSupplierReturnValue;
    }, new Exceptions.Translation<>(e->e != exception, e->new RuntimeException()), new Exceptions.Translation<>(e->e == exception, e->null));
    assertEquals(0d, returnedDoubleValue, 0d);
    // object
    var objectSupplierReturnValue = "9";
    var returnedObjectValue = Exceptions.translate(()-> {
      if (alwaysThrowCondition) {
        throw exception;
      }
      return objectSupplierReturnValue;
    }, new Exceptions.Translation<>(e->e != exception, e->new RuntimeException()), new Exceptions.Translation<>(e->e == exception, e->null));
    assertNull(returnedObjectValue);
    // assertNotThrows:
    Exceptions.translate(()-> {
      if (alwaysThrowCondition) {
        throw exception;
      }
    }, new Exceptions.Translation<>(e->e != exception, e->new RuntimeException()), new Exceptions.Translation<>(e->e == exception, e->null));
  }

  @Test
  public void translate_mustThrowUncaughtExceptionWithWrappedException_whenExceptionIsCheckedExceptionAndTranlationsAreEmpty() {
    var exception = new Exception("Exception");
    var returnedException = assertThrows(UncaughtException.class, ()->Exceptions.translate(exception));
    assertSame(exception, returnedException.getCause());
    // boolean
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateBoolean(()-> {
        var always = true;
        if (always) {
          throw exception;
        }
        return true;
      });
    });
    assertSame(exception, returnedException.getCause());
    // byte
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateByte(()-> {
        var byteVar = (byte) 2;
        if (byteVar != (byte) 0) {
          throw exception;
        }
        return byteVar;
      });
    });
    assertSame(exception, returnedException.getCause());
    // short
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateShort(()-> {
        var shortVar = (short) 3;
        if (shortVar != (short) 0) {
          throw exception;
        }
        return shortVar;
      });
    });
    assertSame(exception, returnedException.getCause());
    // char
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateChar(()-> {
        var charVar = 'D';
        if (charVar != 'A') {
          throw exception;
        }
        return charVar;
      });
    });
    assertSame(exception, returnedException.getCause());
    // int
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateInt(()-> {
        var intVar = 5;
        if (intVar != 0) {
          throw exception;
        }
        return intVar;
      });
    });
    assertSame(exception, returnedException.getCause());
    // long
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateLong(()-> {
        var longVar = 6L;
        if (longVar != 0L) {
          throw exception;
        }
        return longVar;
      });
    });
    assertSame(exception, returnedException.getCause());
    // float
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateFloat(()-> {
        var floatVar = 7.5f;
        if (floatVar != 0f) {
          throw exception;
        }
        return floatVar;
      });
    });
    assertSame(exception, returnedException.getCause());
    // double
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateDouble(()-> {
        var doubleVar = 8.5d;
        if (doubleVar != 0d) {
          throw exception;
        }
        return doubleVar;
      });
    });
    assertSame(exception, returnedException.getCause());
    // byte
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translate(()-> {
        var objectVar = "9";
        if (!objectVar.equals("0")) {
          throw exception;
        }
        return objectVar;
      });
    });
    assertSame(exception, returnedException.getCause());
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translate(()-> {
        var always = true;
        if (always) {
          throw exception;
        }
        return;
      });
    });
    assertSame(exception, returnedException.getCause());
  }

  @Test
  public void translate_mustReturnUncaughtExceptionWithWrappedSameException_whenExceptionIsRuntimeExceptionAndTranlationsAreEmpty() {
    var exception = new RuntimeException();
    var returnedException = assertThrows(UncaughtException.class, ()->Exceptions.translate(exception));
    assertSame(exception, returnedException.getCause());
    // boolean
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateBoolean(()-> {
        var always = true;
        if (always) {
          throw exception;
        }
        return true;
      });
    });
    assertSame(exception, returnedException.getCause());
    // byte
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateByte(()-> {
        var byteVar = (byte) 2;
        if (byteVar != (byte) 0) {
          throw exception;
        }
        return byteVar;
      });
    });
    assertSame(exception, returnedException.getCause());
    // short
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateShort(()-> {
        var shortVar = (short) 3;
        if (shortVar != (short) 0) {
          throw exception;
        }
        return shortVar;
      });
    });
    assertSame(exception, returnedException.getCause());
    // char
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateChar(()-> {
        var charVar = 'D';
        if (charVar != 'A') {
          throw exception;
        }
        return charVar;
      });
    });
    assertSame(exception, returnedException.getCause());
    // int
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateInt(()-> {
        var intVar = 5;
        if (intVar != 0) {
          throw exception;
        }
        return intVar;
      });
    });
    assertSame(exception, returnedException.getCause());
    // long
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateLong(()-> {
        var longVar = 6L;
        if (longVar != 0L) {
          throw exception;
        }
        return longVar;
      });
    });
    assertSame(exception, returnedException.getCause());
    // float
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateFloat(()-> {
        var floatVar = 7.5f;
        if (floatVar != 0f) {
          throw exception;
        }
        return floatVar;
      });
    });
    assertSame(exception, returnedException.getCause());
    // double
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateDouble(()-> {
        var doubleVar = 8.5d;
        if (doubleVar != 0d) {
          throw exception;
        }
        return doubleVar;
      });
    });
    assertSame(exception, returnedException.getCause());
    // byte
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translate(()-> {
        var objectVar = "9";
        if (!objectVar.equals("0")) {
          throw exception;
        }
        return objectVar;
      });
    });
    assertSame(exception, returnedException.getCause());
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translate(()-> {
        var always = true;
        if (always) {
          throw exception;
        }
        return;
      });
    });
    assertSame(exception, returnedException.getCause());
  }

  @Test
  public void translate_mustThrowUncaughtExceptionWithWrappedException_whenExceptionIsCheckedExceptionAndTranlationsAreNull() {
    var exception = new Exception("Exception");
    var returnedException = assertThrows(UncaughtException.class, ()->Exceptions.translate(exception, (Exceptions.Translation<Exception>[]) null));
    assertSame(exception, returnedException.getCause());
    // boolean
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateBoolean(()-> {
        var always = true;
        if (always) {
          throw exception;
        }
        return true;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    // byte
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateByte(()-> {
        var byteVar = (byte) 2;
        if (byteVar != (byte) 0) {
          throw exception;
        }
        return byteVar;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    // short
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateShort(()-> {
        var shortVar = (short) 3;
        if (shortVar != (short) 0) {
          throw exception;
        }
        return shortVar;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    // char
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateChar(()-> {
        var charVar = 'D';
        if (charVar != 'A') {
          throw exception;
        }
        return charVar;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    // int
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateInt(()-> {
        var intVar = 5;
        if (intVar != 0) {
          throw exception;
        }
        return intVar;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    // long
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateLong(()-> {
        var longVar = 6L;
        if (longVar != 0L) {
          throw exception;
        }
        return longVar;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    // float
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateFloat(()-> {
        var floatVar = 7.5f;
        if (floatVar != 0f) {
          throw exception;
        }
        return floatVar;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    // double
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateDouble(()-> {
        var doubleVar = 8.5d;
        if (doubleVar != 0d) {
          throw exception;
        }
        return doubleVar;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    // byte
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translate(()-> {
        var objectVar = "9";
        if (!objectVar.equals("0")) {
          throw exception;
        }
        return objectVar;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translate(()-> {
        var always = true;
        if (always) {
          throw exception;
        }
        return;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
  }

  @Test
  public void translate_mustThrowUncaughtExceptionWithExceptionWrapped_whenExceptionIsRuntimeExceptionAndTranlationsAreNull() {
    var exception = new RuntimeException();
    var returnedException = assertThrows(UncaughtException.class, ()->Exceptions.translate(exception, (Exceptions.Translation<Exception>[]) null));
    assertSame(exception, returnedException.getCause());
    // boolean
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateBoolean(()-> {
        var always = true;
        if (always) {
          throw exception;
        }
        return true;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    // byte
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateByte(()-> {
        var byteVar = (byte) 2;
        if (byteVar != (byte) 0) {
          throw exception;
        }
        return byteVar;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    // short
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateShort(()-> {
        var shortVar = (short) 3;
        if (shortVar != (short) 0) {
          throw exception;
        }
        return shortVar;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    // char
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateChar(()-> {
        var charVar = 'D';
        if (charVar != 'A') {
          throw exception;
        }
        return charVar;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    // int
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateInt(()-> {
        var intVar = 5;
        if (intVar != 0) {
          throw exception;
        }
        return intVar;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    // long
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateLong(()-> {
        var longVar = 6L;
        if (longVar != 0L) {
          throw exception;
        }
        return longVar;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    // float
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateFloat(()-> {
        var floatVar = 7.5f;
        if (floatVar != 0f) {
          throw exception;
        }
        return floatVar;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    // double
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateDouble(()-> {
        var doubleVar = 8.5d;
        if (doubleVar != 0d) {
          throw exception;
        }
        return doubleVar;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    // byte
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translate(()-> {
        var objectVar = "9";
        if (!objectVar.equals("0")) {
          throw exception;
        }
        return objectVar;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translate(()-> {
        var always = true;
        if (always) {
          throw exception;
        }
        return;
      }, (Exceptions.Translation<Exception>[]) null);
    });
    assertSame(exception, returnedException.getCause());
  }

  @Test
  public void translate_mustReturnFirstExceptionTranslationException_whenExceptionIsCheckedExceptionAndFirstTranslationPredicateMatchesException() {
    var exception = new Exception("Exception");
    var firstExceptionTranslationException = new RuntimeException("firstExceptionTranslationException");
    var secondExceptionTranslationException = new RuntimeException("secondExceptionTranslationException");
    var returnedException = Exceptions.translate(
      exception,
      new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
      new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
    );
    assertSame(firstExceptionTranslationException, returnedException);
    // boolean
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translateBoolean(()-> {
        var always = true;
        if (always) {
          throw exception;
        }
        return true;
      },
        new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(firstExceptionTranslationException, returnedException);
    // byte
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translateByte(()-> {
        var byteVar = (byte) 2;
        if (byteVar != (byte) 0) {
          throw exception;
        }
        return byteVar;
      },
        new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(firstExceptionTranslationException, returnedException);
    // short
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translateShort(()-> {
        var shortVar = (short) 3;
        if (shortVar != (short) 0) {
          throw exception;
        }
        return shortVar;
      },
        new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(firstExceptionTranslationException, returnedException);
    // char
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translateChar(()-> {
        var charVar = 'D';
        if (charVar != 'A') {
          throw exception;
        }
        return charVar;
      },
        new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(firstExceptionTranslationException, returnedException);
    // int
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translateInt(()-> {
        var intVar = 5;
        if (intVar != 0) {
          throw exception;
        }
        return intVar;
      },
        new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(firstExceptionTranslationException, returnedException);
    // long
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translateLong(()-> {
        var longVar = 6L;
        if (longVar != 0L) {
          throw exception;
        }
        return longVar;
      },
        new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(firstExceptionTranslationException, returnedException);
    // float
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translateFloat(()-> {
        var floatVar = 7.5f;
        if (floatVar != 0f) {
          throw exception;
        }
        return floatVar;
      },
        new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(firstExceptionTranslationException, returnedException);
    // double
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translateDouble(()-> {
        var doubleVar = 8.5d;
        if (doubleVar != 0d) {
          throw exception;
        }
        return doubleVar;
      },
        new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(firstExceptionTranslationException, returnedException);
    // byte
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translate(()-> {
        var objectVar = "9";
        if (!objectVar.equals("0")) {
          throw exception;
        }
        return objectVar;
      },
        new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(firstExceptionTranslationException, returnedException);
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translate(()-> {
        var always = true;
        if (always) {
          throw exception;
        }
        return;
      },
        new Exceptions.Translation<>(e->e == exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
  }

  @Test
  public void translate_mustReturnSecondExceptionTranslationException_whenExceptionIsCheckedExceptionAndFirstTranslationPredicateNotMatchesException() {
    var exception = new Exception("Exception");
    var firstExceptionTranslationException = new RuntimeException("firstExceptionTranslationException");
    var secondExceptionTranslationException = new RuntimeException("secondExceptionTranslationException");
    var returnedException = Exceptions.translate(
      exception,
      new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
      new Exceptions.Translation<>(e->e == exception, e->secondExceptionTranslationException)
    );
    assertSame(secondExceptionTranslationException, returnedException);
    // boolean
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translateBoolean(()-> {
        var always = true;
        if (always) {
          throw exception;
        }
        return true;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e == exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(secondExceptionTranslationException, returnedException);
    // byte
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translateByte(()-> {
        var byteVar = (byte) 2;
        if (byteVar != (byte) 0) {
          throw exception;
        }
        return byteVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e == exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(secondExceptionTranslationException, returnedException);
    // short
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translateShort(()-> {
        var shortVar = (short) 3;
        if (shortVar != (short) 0) {
          throw exception;
        }
        return shortVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e == exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(secondExceptionTranslationException, returnedException);
    // char
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translateChar(()-> {
        var charVar = 'D';
        if (charVar != 'A') {
          throw exception;
        }
        return charVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e == exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(secondExceptionTranslationException, returnedException);
    // int
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translateInt(()-> {
        var intVar = 5;
        if (intVar != 0) {
          throw exception;
        }
        return intVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e == exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(secondExceptionTranslationException, returnedException);
    // long
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translateLong(()-> {
        var longVar = 6L;
        if (longVar != 0L) {
          throw exception;
        }
        return longVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e == exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(secondExceptionTranslationException, returnedException);
    // float
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translateFloat(()-> {
        var floatVar = 7.5f;
        if (floatVar != 0f) {
          throw exception;
        }
        return floatVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e == exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(secondExceptionTranslationException, returnedException);
    // double
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translateDouble(()-> {
        var doubleVar = 8.5d;
        if (doubleVar != 0d) {
          throw exception;
        }
        return doubleVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e == exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(secondExceptionTranslationException, returnedException);
    // byte
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translate(()-> {
        var objectVar = "9";
        if (!objectVar.equals("0")) {
          throw exception;
        }
        return objectVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e == exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(secondExceptionTranslationException, returnedException);
    // boolean
    returnedException = assertThrows(RuntimeException.class, ()-> {
      Exceptions.translate(()-> {
        var always = true;
        if (always) {
          throw exception;
        }
        return;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e == exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(secondExceptionTranslationException, returnedException);
  }

  @Test
  public void translate_mustThrowUncaughtExceptionWithWrappedException_whenExceptionIsCheckedExceptionAndNoTranslationMatches() {
    var exception = new Exception("Exception");
    var firstExceptionTranslationException = new RuntimeException("firstExceptionTranslationException");
    var secondExceptionTranslationException = new RuntimeException("secondExceptionTranslationException");
    var returnedException = assertThrows(
      UncaughtException.class,
      ()->Exceptions.translate(
        exception,
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      )
    );
    assertSame(exception, returnedException.getCause());
    // boolean
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateBoolean(()-> {
        var always = true;
        if (always) {
          throw exception;
        }
        return true;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(exception, returnedException.getCause());
    // byte
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateByte(()-> {
        var byteVar = (byte) 2;
        if (byteVar != (byte) 0) {
          throw exception;
        }
        return byteVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(exception, returnedException.getCause());
    // short
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateShort(()-> {
        var shortVar = (short) 3;
        if (shortVar != (short) 0) {
          throw exception;
        }
        return shortVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(exception, returnedException.getCause());
    // char
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateChar(()-> {
        var charVar = 'D';
        if (charVar != 'A') {
          throw exception;
        }
        return charVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(exception, returnedException.getCause());
    // int
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateInt(()-> {
        var intVar = 5;
        if (intVar != 0) {
          throw exception;
        }
        return intVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(exception, returnedException.getCause());
    // long
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateLong(()-> {
        var longVar = 6L;
        if (longVar != 0L) {
          throw exception;
        }
        return longVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(exception, returnedException.getCause());
    // float
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateFloat(()-> {
        var floatVar = 7.5f;
        if (floatVar != 0f) {
          throw exception;
        }
        return floatVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(exception, returnedException.getCause());
    // double
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateDouble(()-> {
        var doubleVar = 8.5d;
        if (doubleVar != 0d) {
          throw exception;
        }
        return doubleVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(exception, returnedException.getCause());
    // byte
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translate(()-> {
        var objectVar = "9";
        if (!objectVar.equals("0")) {
          throw exception;
        }
        return objectVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(exception, returnedException.getCause());
    // boolean
    returnedException = assertThrows(UncaughtException.class, ()-> {
      Exceptions.translate(()-> {
        var always = true;
        if (always) {
          throw exception;
        }
        return;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    assertSame(exception, returnedException.getCause());
  }

  @Test
  public void translate_mustReturnUncaughtException_whenExceptionIsRuntimeExceptionAndNoTranslationMatches() {
    var exception = new RuntimeException("Exception");
    var firstExceptionTranslationException = new RuntimeException("firstExceptionTranslationException");
    var secondExceptionTranslationException = new RuntimeException("secondExceptionTranslationException");
    assertThrows(
      UncaughtException.class,
      ()->Exceptions.translate(
        exception,
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      )
    );
    // boolean
    assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateBoolean(()-> {
        var always = true;
        if (always) {
          throw exception;
        }
        return true;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    // byte
    assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateByte(()-> {
        var byteVar = (byte) 2;
        if (byteVar != (byte) 0) {
          throw exception;
        }
        return byteVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    // short
    assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateShort(()-> {
        var shortVar = (short) 3;
        if (shortVar != (short) 0) {
          throw exception;
        }
        return shortVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    // char
    assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateChar(()-> {
        var charVar = 'D';
        if (charVar != 'A') {
          throw exception;
        }
        return charVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    // int
    assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateInt(()-> {
        var intVar = 5;
        if (intVar != 0) {
          throw exception;
        }
        return intVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    // long
    assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateLong(()-> {
        var longVar = 6L;
        if (longVar != 0L) {
          throw exception;
        }
        return longVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    // float
    assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateFloat(()-> {
        var floatVar = 7.5f;
        if (floatVar != 0f) {
          throw exception;
        }
        return floatVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    // double
    assertThrows(UncaughtException.class, ()-> {
      Exceptions.translateDouble(()-> {
        var doubleVar = 8.5d;
        if (doubleVar != 0d) {
          throw exception;
        }
        return doubleVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    // byte
    assertThrows(UncaughtException.class, ()-> {
      Exceptions.translate(()-> {
        var objectVar = "9";
        if (!objectVar.equals("0")) {
          throw exception;
        }
        return objectVar;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
    // boolean
    assertThrows(UncaughtException.class, ()-> {
      Exceptions.translate(()-> {
        var always = true;
        if (always) {
          throw exception;
        }
        return;
      },
        new Exceptions.Translation<>(e->e != exception, e->firstExceptionTranslationException),
        new Exceptions.Translation<>(e->e != exception, e->secondExceptionTranslationException)
      );
    });
  }

  @Test
  public void translate_mustReturnNull_whenExceptionIsCheckedExceptionAndTranslationToFnReturnsNull() {
    var exception = new Exception("Exception");
    var returnedValue = Exceptions.translate(exception, new Exceptions.Translation<>(e->e == exception, e->null));
    assertNull(returnedValue);
  }

  @Test
  public void translate_mustReturnNull_whenExceptionIsRuntimeExceptionAndTranslationToFnReturnsNull() {
    var exception = new RuntimeException("Exception");
    var returnedException = Exceptions.translate(exception, new Exceptions.Translation<>(e->e == exception, e->null));
    assertNull(returnedException);
  }

  @Test
  public void translate_mustReturnNull_whenExceptionIsNull() {
    var exception = (Exception) null;
    assertNull(Exceptions.translate(exception, Exceptions.Translation.when(e->true).to(e->new RuntimeException(e))));
  }

  @Test
  public void any_test_mustReturnTrue_always() {
    var translatedException1 = new RuntimeException("Translated Exception");
    assertTrue(Exceptions.any().test(translatedException1));
    var translatedException2 = new Exception("Translated Exception");
    assertTrue(Exceptions.any().test(translatedException2));
  }

  @Test
  public void hasMsg_test_mustReturnTrue_whenExceptionMessageIsEqualsMsg() {
    var msg = "This message must trigger the translation";
    var exceptionMessageIsEqualsMsg = new String(msg.toCharArray()); // Be sure is not the same instance of String;
    var exception = new RuntimeException(exceptionMessageIsEqualsMsg);
    var predicate = Exceptions.hasMsg(msg);
    assertTrue(predicate.test(exception));
  }

  @Test
  public void hasMsg_test_mustReturnFalse_whenExceptionMessageIsNotEqualsMsg() {
    var msg = "This message must trigger the translation";
    var exceptionMessageIsNotEqualsMsg = "This message must NOT trigger the translation";
    var exception = new RuntimeException(exceptionMessageIsNotEqualsMsg);
    var predicate = Exceptions.hasMsg(msg);
    assertFalse(predicate.test(exception));
  }

  @Test
  public void hasMsg_test_mustReturnFalse_whenExceptionMessageIsNullAndMsgIsNotNull() {
    var msgIsNotNull = "This message must trigger the translation";
    var exceptionMessageIsNull = (String) null;
    var exception = new RuntimeException(exceptionMessageIsNull);
    var predicate = Exceptions.hasMsg(msgIsNotNull);
    assertFalse(predicate.test(exception));
  }

  @Test
  public void hasMsg_test_mustReturnTrue_whenExceptionMessageIsNullAndMsgIsNull() {
    var msgIsNull = (String) null;
    var exceptionMessageIsNull = (String) null;
    var exception = new RuntimeException(exceptionMessageIsNull);
    var predicate = Exceptions.hasMsg(msgIsNull);
    assertTrue(predicate.test(exception));
  }

  @Test
  public void hasMsg_mustReturnFalse_whenExceptionMessageIsNotNullAndMsgIsNull() {
    var msgIsNull = (String) null;
    var exceptionMessageIsNotNull = "This message must NOT trigger the translation";
    var exception = new RuntimeException(exceptionMessageIsNotNull);
    var predicate = Exceptions.hasMsg(msgIsNull);
    assertFalse(predicate.test(exception));
  }

  @Test
  public void hasType_test_mustReturnTrue_whenExceptionTypeIsEqualsType() {
    var type = IOException.class;
    var exceptionTypeIsEqualsType = new IOException();
    var predicate = Exceptions.hasType(type);
    assertTrue(predicate.test(exceptionTypeIsEqualsType));
  }

  @Test
  public void hasType_test_mustReturnTrue_whenExceptionIsSubclassOfType() {
    var type = IOException.class;
    var exceptionIsSubclassOfType = new EOFException();
    var predicate = Exceptions.hasType(type);
    assertTrue(predicate.test(exceptionIsSubclassOfType));
  }

  @Test
  public void hasType_test_mustReturnFalse_whenExceptionIsNotSubClassOfType() {
    var type = IOException.class;
    var theSameExceptionOfType = new IOException();
    var exceptionIsNotSubClassOfTypeAndCausedByTheSameExceptionOfType = new RuntimeException(
      "This message must NOT trigger the translation",
      theSameExceptionOfType
    );
    var predicate = Exceptions.hasType(type);
    assertFalse(predicate.test(exceptionIsNotSubClassOfTypeAndCausedByTheSameExceptionOfType));
  }

  @Test
  public void hasType_mustThrowNullPointerException_whenTypeIsNull() {
    var typeIsNull = (Class<Exception>) null;
    assertThrows(NullPointerException.class, ()->Exceptions.hasType(typeIsNull));
  }

  @Test
  public void hasExactType_test_mustReturnTrue_whenExceptionTypeIsEqualsType() {
    var type = IOException.class;
    var exceptionTypeIsEqualsType = new IOException();
    var translation = Exceptions.hasExactType(type);
    assertTrue(translation.test(exceptionTypeIsEqualsType));
  }

  @Test
  public void hasExactType_test_mustReturnFalse_whenTypeIsSuperClassOfTriggerExceptionType() {
    var type = IOException.class;
    var exceptionIsSubclassOfType = new EOFException();
    var predicate = Exceptions.hasExactType(type);
    assertFalse(predicate.test(exceptionIsSubclassOfType));
  }

  @Test
  public void hasExactType_test_mustReturnFalse_whenExceptionIsNotSubclassOfType() {
    var type = IOException.class;
    var exceptionIsNotSubclassOfType = new RuntimeException();
    var predicate = Exceptions.hasExactType(type);
    assertFalse(predicate.test(exceptionIsNotSubclassOfType));
  }

  @Test
  public void hasExactType_mustThrowNullPointerException_whenTypeIsNull() {
    var typeIsNull = (Class<Exception>) null;
    assertThrows(NullPointerException.class, ()->Exceptions.hasExactType(typeIsNull));
  }

  @Test
  public void inCauseChain_test_mustReturnTrue_whenPredicateTestTrueInExceptionWithNoChain() {
    var exceptionWithNoChain = new RuntimeException("mainException");
    var predicate = Exceptions.inCauseChain(e->e == exceptionWithNoChain);
    assertTrue(predicate.test(exceptionWithNoChain));
  }

  @Test
  public void inCauseChain_test_mustReturnTrue_whenPredicateTestTrueInFirstExceptionInChain() {
    var secondExceptionInChain = new RuntimeException("exceptionInChain");
    var firstExceptionInChain = new RuntimeException("mainException", secondExceptionInChain);
    var predicate = Exceptions.inCauseChain(e->e == firstExceptionInChain);
    assertTrue(predicate.test(firstExceptionInChain));
  }

  @Test
  public void inCauseChain_test_mustReturnTrue_whenPredicateTestTrueInSecondExceptionInChain() {
    var secondExceptionInChain = new RuntimeException("exceptionInChain");
    var firstExceptionInChain = new RuntimeException("mainException", secondExceptionInChain);
    var predicate = Exceptions.inCauseChain(e->e == secondExceptionInChain);
    assertTrue(predicate.test(firstExceptionInChain));
  }

  @Test
  public void inCauseChain_test_mustReturnFalse_whenPredicateTestFalseInExceptionWithNoChain() {
    var exceptionWithNoChain = new RuntimeException("mainException");
    var predicate = Exceptions.inCauseChain(e->false);
    assertFalse(predicate.test(exceptionWithNoChain));
  }

  @Test
  public void inCauseChain_test_mustReturnFalse_whenPredicateTestFalseForAllExceptionsInChain() {
    var exceptionInChain = new RuntimeException("exceptionInChain");
    var mainException = new RuntimeException("mainException", exceptionInChain);
    var predicate = Exceptions.inCauseChain(e->false);
    assertFalse(predicate.test(mainException));
  }

  @Test
  public void inCauseChain_test_mustReturnFalse_whenPredicateCouldTestTrueButExceptionIsRawThrowableInChain() {
    var rawThrowableInChain = new Throwable("rawThrowableInChain");
    var mainException = new RuntimeException("mainException", rawThrowableInChain);
    var predicate = Exceptions.inCauseChain(e->e == rawThrowableInChain);
    assertFalse(predicate.test(mainException));
    var predicate2 = Exceptions.inCauseChain(e->false);
    assertFalse(predicate2.test(mainException));
  }

  @Test
  public void inCauseChain_mustThrowNullPointerException_whenExceptionPredicateIsNull() {
    var exceptionPredicate = (Predicate<Exception>) null;
    assertThrows(NullPointerException.class, ()->Exceptions.inCauseChain(exceptionPredicate));
  }

  @Test
  public void translation_new_mustReturnInstance_whenExceptionPredicateAndToFnAreNotNull() {
    var exceptionPredicate = (Predicate<Exception>) e->true;
    var toFn = (Function<Exception, RuntimeException>) e->new RuntimeException();
    var translation = new Exceptions.Translation<>(exceptionPredicate, toFn);
    assertNotNull(translation);
    assertSame(exceptionPredicate, translation.exceptionPredicate());
    assertSame(toFn, translation.toFn());
  }

  @Test
  public void translation_new_throwNullPointerException_whenByPredicateIsNull() {
    var exceptionPredicate = (Predicate<Exception>) null;
    var toFn = (Function<Exception, RuntimeException>) e->new RuntimeException();
    assertThrows(NullPointerException.class, ()->new Exceptions.Translation<>(exceptionPredicate, toFn));
  }

  @Test
  public void translation_new_throwNullPointerException_whenToFnIsNull() {
    var exceptionPredicate = (Predicate<Exception>) e->true;
    var toFn = (Function<Exception, RuntimeException>) null;
    assertThrows(NullPointerException.class, ()->new Exceptions.Translation<>(exceptionPredicate, toFn));
  }

  @Test
  public void translation_test_mustReturnToFnReturnedTrue_whenExceptionPredicateTestReturnTrue() {
    var exceptionPredicate = (Predicate<Exception>) e->true;
    var exception = new Exception();
    var toFnReturnedValue = new RuntimeException();
    var toFn = (Function<Exception, RuntimeException>) e->toFnReturnedValue;
    var translation = new Exceptions.Translation<>(exceptionPredicate, toFn);
    assertTrue(translation.test(exception));
  }

  @Test
  public void translation_test_mustReturnFalse_whenExceptionPredicateTestReturnFalse() {
    var exceptionPredicate = (Predicate<Exception>) e->false;
    var exception = new Exception();
    var toFnReturnedValue = new RuntimeException();
    var toFn = (Function<Exception, RuntimeException>) e->toFnReturnedValue;
    var translation = new Exceptions.Translation<>(exceptionPredicate, toFn);
    assertFalse(translation.test(exception));
  }

  @Test
  public void translation_test_throwNullPointerException_whenExceptionIsNull() {
    var exceptionPredicate = (Predicate<Exception>) e->true;
    var exception = (RuntimeException) null;
    var toFn = (Function<Exception, RuntimeException>) e->new RuntimeException(e);
    assertThrows(NullPointerException.class, ()->new Exceptions.Translation<>(exceptionPredicate, toFn).test(exception));
  }

  @Test
  public void translation_when_mustReturnBuilderInstance_whenExceptionPredicateIsNotNull() {
    var exceptionPredicate = (Predicate<Exception>) e->true;
    var builderInstance = Exceptions.Translation.when(exceptionPredicate);
    assertNotNull(builderInstance);
  }

  @Test
  public void translation_when_mustThrowNullPointerException_whenExceptionPredicateIsNull() {
    var exceptionPredicate = (Predicate<Exception>) null;
    assertThrows(NullPointerException.class, ()->Exceptions.Translation.when(exceptionPredicate));
  }

  @Test
  public void translation_builder_to_mustReturnTranslationInstance_whenToFnIsNotNull() {
    var exceptionPredicate = (Predicate<Exception>) e->true;
    var toFn = (Function<Exception, RuntimeException>) e->null;
    var translation = Exceptions.Translation.when(exceptionPredicate).to(toFn);
    assertNotNull(translation);
    assertSame(exceptionPredicate, translation.exceptionPredicate());
    assertSame(toFn, translation.toFn());
  }

  @Test
  public void translation_builder_to_mustThrowNullPointerException_whenToFnIsNull() {
    var exceptionPredicate = (Predicate<Exception>) e->false;
    var toFn = (Function<Exception, RuntimeException>) null;
    assertThrows(NullPointerException.class, ()->Exceptions.Translation.when(exceptionPredicate).to(toFn));
  }

  @Test
  public void translation_builder_inCauseChain_mustReturnDifferentBuilderInstance() {
    var exceptionPredicate = (Predicate<Exception>) e->true;
    var builder = Exceptions.Translation.when(exceptionPredicate);
    var differentBuilderInstance = builder.inCauseChain();
    assertNotSame(builder, differentBuilderInstance);
  }

  @Test
  public void translation_test_mustReturnTrue_whenPredicateTestTrueInExceptionWithNoChain_usingInCauseChainInBuilder() {
    var exceptionWithNoChain = new RuntimeException("mainException");
    var toFn = (Function<Exception, RuntimeException>) e->new RuntimeException(e);
    var translation = Exceptions.Translation.when(e->e == exceptionWithNoChain).inCauseChain().to(toFn);
    assertTrue(translation.test(exceptionWithNoChain));
  }

  @Test
  public void translation_test_mustReturnTrue_whenPredicateTestTrueInFirstExceptionInChain_usingInCauseChainInBuilder() {
    var secondExceptionInChain = new RuntimeException("exceptionInChain");
    var firstExceptionInChain = new RuntimeException("mainException", secondExceptionInChain);
    var toFn = (Function<Exception, RuntimeException>) e->new RuntimeException(e);
    var tranlation = Exceptions.Translation.when(e->e == firstExceptionInChain).inCauseChain().to(toFn);
    assertTrue(tranlation.test(firstExceptionInChain));
  }

  @Test
  public void translation_test_mustReturnTrue_whenPredicateTestTrueInSecondExceptionInChain_usingInCauseChainInBuilder() {
    var secondExceptionInChain = new RuntimeException("exceptionInChain");
    var firstExceptionInChain = new RuntimeException("mainException", secondExceptionInChain);
    var toFn = (Function<Exception, RuntimeException>) e->new RuntimeException(e);
    var translation = Exceptions.Translation.when(e->e == secondExceptionInChain).inCauseChain().to(toFn);
    assertTrue(translation.test(firstExceptionInChain));
  }

  @Test
  public void translation_test_mustReturnFalse_whenPredicateTestFalseInExceptionWithNoChain_usingInCauseChainInBuilder() {
    var exceptionWithNoChain = new RuntimeException("mainException");
    var toFn = (Function<Exception, RuntimeException>) e->new RuntimeException(e);
    var translation = Exceptions.Translation.when(e->false).inCauseChain().to(toFn);
    assertFalse(translation.test(exceptionWithNoChain));
  }

  @Test
  public void translation_test_mustReturnFalse_whenPredicateTestFalseForAllExceptionsInChain_usingInCauseChainInBuilder() {
    var exceptionInChain = new RuntimeException("exceptionInChain");
    var toFn = (Function<Exception, RuntimeException>) e->new RuntimeException(e);
    var translation = Exceptions.Translation.when(e->false).inCauseChain().to(toFn);
    assertFalse(translation.test(exceptionInChain));
  }

}
