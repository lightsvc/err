// This file is part of lightsvc Java Microservices Components (lightsvc).
//
// lightsvc Java Microservices Components (lightsvc) is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lightsvc Java Microservices Components (lightsvc) is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with lightsvc Java Microservices Components (lightsvc). If not, see <http://www.gnu.org/licenses/>.
package lightsvc.err;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;

import javax.annotation.processing.Generated;

import org.junit.Test;

import lightsvc.err.Exceptions.Supplier;
import lightsvc.util.Maps;

@SuppressWarnings({
  "javadoc", "static-method"
})
@Generated("junit")
public class ValidateTestCase {

  private RuntimeException exception;
  private Supplier<RuntimeException, RuntimeException> exceptionSupplier;

  public ValidateTestCase() {
    this.exception = new RuntimeException();
    this.exceptionSupplier = ()->this.exception;
  }

  @Test
  public void new_Validate_throwsUnsupportedOperationException_whenCalled() throws NoSuchMethodException, SecurityException {
    var constructor = Validate.class.getDeclaredConstructor();
    constructor.setAccessible(true);
    var ite = assertThrows(InvocationTargetException.class, ()->constructor.newInstance());
    assertEquals(UnsupportedOperationException.class, ite.getCause().getClass());
  }

  @Test
  public void is_returnsValue_whenValueIsTrue() {
    var value = true;
    var returnedValue = Validate.is(value, this.exceptionSupplier);
    assertTrue(returnedValue);
  }

  @Test
  public void is_throwsException_whenValueIsFalse() {
    var value = false;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.is(value, this.exceptionSupplier)));
  }

  @Test
  public void is_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var value = true;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.is(value, nullExceptionSupplier));
  }

  @Test
  public void not_returnsValue_whenValueIsFalse() {
    var value = false;
    var returnedValue = Validate.not(value, this.exceptionSupplier);
    assertFalse(returnedValue);
  }

  @Test
  public void not_throwsException_whenValueIsTrue() {
    var value = true;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.not(value, this.exceptionSupplier)));
  }

  @Test
  public void not_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var value = true;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.not(value, nullExceptionSupplier));
  }

  @Test
  public void minValue_byteMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var minValue = (byte) 1;
    var value = (byte) 0;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.minValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void minValue_byteMinValueAndValue_throwExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var minValue = (byte) 1;
    var value = (byte) 0;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void minValue_byteMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var minValue = (byte) 1;
    var value = (byte) 1;
    var returnedValue = Validate.minValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void minValue_byteMinValueAndValue_returnValue_whenValueIsGreaterThannMinValue() {
    var minValue = (byte) 0;
    var value = (byte) 1;
    var returnedValue = Validate.minValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void minValue_shortMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var minValue = (short) 1;
    var value = (short) 0;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.minValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void minValue_shortMinValueAndValue_throwExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var minValue = (short) 1;
    var value = (short) 0;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void minValue_shortMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var minValue = (short) 1;
    var value = (short) 1;
    var returnedValue = Validate.minValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void minValue_shortMinValueAndValue_returnValue_whenValueIsGreaterThannMinValue() {
    var minValue = (short) 0;
    var value = (short) 1;
    var returnedValue = Validate.minValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void minValue_intMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var minValue = 1;
    var value = 0;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.minValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void minValue_intMinValueAndValue_throwExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var minValue = 1;
    var value = 0;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void minValue_intMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var minValue = 1;
    var value = 1;
    var returnedValue = Validate.minValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void minValue_intMinValueAndValue_returnValue_whenValueIsGreaterThannMinValue() {
    var minValue = 0;
    var value = 1;
    var returnedValue = Validate.minValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void minValue_longMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var minValue = 1L;
    var value = 0L;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.minValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void minValue_longMinValueAndValue_throwExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var minValue = 1L;
    var value = 0L;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void minValue_longMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var minValue = 1L;
    var value = 1L;
    var returnedValue = Validate.minValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void minValue_longMinValueAndValue_returnValue_whenValueIsGreaterThannMinValue() {
    var minValue = 0L;
    var value = 1L;
    var returnedValue = Validate.minValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void minValue_floatMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var minValueLong = 1.0f;
    var valueLong = 0.0f;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.minValue(minValueLong, valueLong, nullExceptionSupplier));
  }

  @Test
  public void minValue_floatMinValueAndValue_throwExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var minValue = 1.0f;
    var value = 0.0f;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void minValue_floatMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var minValue = 1.0f;
    var value = 1.0f;
    var returnedValue = Validate.minValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue, 0.0f);
  }

  @Test
  public void minValue_floatMinValueAndValue_returnValue_whenValueIsGreaterThannMinValue() {
    var minValue = 0.0f;
    var value = 1.0f;
    var returnedValue = Validate.minValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue, 0.0f);
  }

  @Test
  public void minValue_doubleMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var minValueLong = 1.0;
    var valueLong = 0.0;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.minValue(minValueLong, valueLong, nullExceptionSupplier));
  }

  @Test
  public void minValue_doubleMinValueAndValue_throwExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var minValue = 1.0;
    var value = 0.0;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void minValue_doubleMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var minValue = 1.0;
    var value = 1.0;
    var returnedValue = Validate.minValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue, 0.0f);
  }

  @Test
  public void minValue_doubleMinValueAndValue_returnValue_whenValueIsGreaterThannMinValue() {
    var minValue = 0.0;
    var value = 1.0;
    var returnedValue = Validate.minValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue, 0.0f);
  }

  @Test
  public void minValue_bigIntegerMinValueAndValue_throwsNullPointerException_whenMinValueIsNull() {
    var minValue = (BigInteger) null;
    var value = (BigInteger) null;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.minValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void minValue_bigIntegerMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var minValue = BigInteger.valueOf(1);
    var value = BigInteger.valueOf(0);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.minValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void minValue_bigIntegerMinValueAndNullValue_throwsExceptionSupplierReturnedException_whenValueIsNull() {
    var minValue = BigInteger.valueOf(1);
    var value = (BigInteger) null;
    Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void minValue_bigIntegerMinValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var minValue = BigInteger.valueOf(1);
    var value = BigInteger.valueOf(0);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void minValue_bigIntegerMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var minValue = BigInteger.valueOf(1);
    var value = BigInteger.valueOf(1);
    var returnedValue = Validate.minValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void minValue_bigIntegerMinValueAndValue_returnValue_whenValueIsGreaterThanMinValue() {
    var minValue = BigInteger.valueOf(0);
    var value = BigInteger.valueOf(1);
    var returnedValue = Validate.minValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void minValue_bigDecimalMinValueAndValue_throwsNullPointerException_whenMinValueIsNull() {
    var minValue = (BigDecimal) null;
    var value = (BigDecimal) null;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.minValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void minValue_bigDecimalMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var minValue = BigDecimal.valueOf(1.0);
    var value = BigDecimal.valueOf(0.0);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.minValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void minValue_bigDecimalMinValueAndValue_throwsExceptionSupplierReturnedException_whenValueINull() {
    var minValue = BigDecimal.valueOf(1.0);
    var value = (BigDecimal) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void minValue_bigDecimalMinValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var minValue = BigDecimal.valueOf(1.0);
    var value = BigDecimal.valueOf(0.0);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void minValue_bigDecimalMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var minValue = BigDecimal.valueOf(1.0);
    var value = BigDecimal.valueOf(1.0);
    var returnedValue = Validate.minValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void minValue_bigDecimalMinValueAndValue_returnValue_whenValueIsGreaterThanMinValue() {
    var minValue = BigDecimal.valueOf(0.0);
    var value = BigDecimal.valueOf(1.0);
    var returnedValue = Validate.minValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void maxValue_byteMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var maxValue = (byte) 0;
    var value = (byte) 1;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.maxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void maxValue_byteMinValueAndValue_throwExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var maxValue = (byte) 0;
    var value = (byte) 1;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void maxValue_byteMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var maxValue = (byte) 1;
    var value = (byte) 1;
    var returnedValue = Validate.maxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue, 0.0f);
  }

  @Test
  public void maxValue_byteMinValueAndValue_returnValue_whenValueIsGreaterThannMinValue() {
    var maxValue = (byte) 1;
    var value = (byte) 0;
    var returnedValue = Validate.maxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue, 0.0f);
  }

  @Test
  public void maxValue_shortMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var maxValue = (short) 0;
    var value = (short) 1;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.maxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void maxValue_shortMinValueAndValue_throwExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var maxValue = (short) 0;
    var value = (short) 1;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void maxValue_shortMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var maxValue = (short) 1;
    var value = (short) 1;
    var returnedValue = Validate.maxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue, 0.0f);
  }

  @Test
  public void maxValue_shortMinValueAndValue_returnValue_whenValueIsGreaterThannMinValue() {
    var maxValue = (short) 1;
    var value = (short) 0;
    var returnedValue = Validate.maxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue, 0.0f);
  }

  @Test
  public void maxValue_intMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var maxValue = 0;
    var value = 1;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.maxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void maxValue_intMinValueAndValue_throwExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var maxValue = 0;
    var value = 1;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void maxValue_intMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var maxValue = 1;
    var value = 1;
    var returnedValue = Validate.maxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue, 0.0f);
  }

  @Test
  public void maxValue_intMinValueAndValue_returnValue_whenValueIsGreaterThannMinValue() {
    var maxValue = 1;
    var value = 0;
    var returnedValue = Validate.maxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue, 0.0f);
  }

  @Test
  public void maxValue_longMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var maxValue = 0L;
    var value = 1L;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.maxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void maxValue_longMinValueAndValue_throwExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var maxValue = 0L;
    var value = 1L;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void maxValue_longMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var maxValue = 1L;
    var value = 1L;
    var returnedValue = Validate.maxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue, 0.0f);
  }

  @Test
  public void maxValue_longMinValueAndValue_returnValue_whenValueIsGreaterThannMinValue() {
    var maxValue = 1L;
    var value = 0L;
    var returnedValue = Validate.maxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue, 0.0f);
  }

  @Test
  public void maxValue_floatMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var maxValue = 0.0f;
    var value = 1.0f;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.maxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void maxValue_floatMinValueAndValue_throwExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var maxValue = 0.0f;
    var value = 1.0f;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void maxValue_floatMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var maxValue = 1.0f;
    var value = 1.0f;
    var returnedValue = Validate.maxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue, 0.0f);
  }

  @Test
  public void maxValue_floatMinValueAndValue_returnValue_whenValueIsGreaterThannMinValue() {
    var maxValue = 1.0f;
    var value = 0.0f;
    var returnedValue = Validate.maxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue, 0.0f);
  }

  @Test
  public void maxValue_doubleMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var maxValue = 0.0;
    var value = 1.0;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.maxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void maxValue_doubleMinValueAndValue_throwExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var maxValue = 0.0;
    var value = 1.0;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void maxValue_doubleMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var maxValue = 1.0;
    var value = 1.0;
    var returnedValue = Validate.maxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue, 0.0f);
  }

  @Test
  public void maxValue_doubleMinValueAndValue_returnValue_whenValueIsGreaterThannMinValue() {
    var maxValue = 1.0;
    var value = 0.0;
    var returnedValue = Validate.maxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue, 0.0f);
  }

  @Test
  public void maxValue_bigIntegerMaxValueAndValue_throwsNullPointerException_whenMaxValueIsNull() {
    var maxValue = (BigInteger) null;
    var value = (BigInteger) null;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.maxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void maxValue_bigIntegerMaxValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var maxValue = BigInteger.valueOf(0);
    var value = BigInteger.valueOf(1);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.maxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void maxValue_bigIntegerMaxValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsNull() {
    var maxValue = BigInteger.valueOf(0);
    var value = (BigInteger) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void maxValue_bigIntegerMaxValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsGreaterThanMaxValue() {
    var maxValue = BigInteger.valueOf(0);
    var value = BigInteger.valueOf(1);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void maxValue_bigIntegerMaxValueAndValue_returnValue_whenValueIsEqualsMaxValue() {
    var maxValue = BigInteger.valueOf(1);
    var value = BigInteger.valueOf(1);
    var returnedValue = Validate.maxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void maxValue_bigIntegerMaxValueAndValue_returnValue_whenValueIsSmallerThanMaxValue() {
    var maxValue = BigInteger.valueOf(1);
    var value = BigInteger.valueOf(0);
    var returnedValue = Validate.maxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void maxValue_bigDecimalMaxValueAndValue_throwsNullPointerException_whenMaxValueIsNull() {
    var maxValue = (BigDecimal) null;
    var value = (BigDecimal) null;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.maxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void maxValue_bigDecimalMaxValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var maxValue = BigDecimal.valueOf(0.0);
    var value = BigDecimal.valueOf(1.0);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.maxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void maxValue_bigDecimalMaxValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsNull() {
    var maxValue = BigDecimal.valueOf(0.0);
    var value = (BigDecimal) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void maxValue_bigDecimalMaxValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsGreaterThanMaxValue() {
    var maxValue = BigDecimal.valueOf(0.0);
    var value = BigDecimal.valueOf(1.0);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void maxValue_bigDecimalMaxValueAndValue_returnValue_whenValueIsEqualsMaxValue() {
    var maxValue = BigDecimal.valueOf(1.0);
    var value = BigDecimal.valueOf(1.0);
    var returnedValue = Validate.maxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void maxValue_bigDecimalMaxValueAndValue_returnValue_whenValueIsSmallerThanMaxValue() {
    var maxValue = BigDecimal.valueOf(1.0);
    var value = BigDecimal.valueOf(0.0);
    var returnedValue = Validate.maxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void isNull_returnValue_whenValueIsNull() {
    var returnedValue = Validate.isNull(null, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void isNull_throwsException_whenValueIsNotNull() {
    var value = "Not Null";
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.isNull(value, this.exceptionSupplier)));
  }

  @Test
  public void notNull_returnValue_whenValueIsNotNull() {
    var stringValue = "Not Null";
    var stringReturnedValue = Validate.notNull(stringValue, this.exceptionSupplier);
    assertSame(stringValue, stringReturnedValue);
    var integerValue = Integer.valueOf(1);
    var integerReturnedValue = Validate.notNull(integerValue, this.exceptionSupplier);
    assertSame(integerValue, integerReturnedValue);
  }

  @Test
  public void notNull_throwsException_whenValueIsNull() {
    var value = (String) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notNull(value, this.exceptionSupplier)));
  }

  @Test
  public void isPresent_returnsOptionalInternalValue_whenIsValuedOptional() {
    var value = "Present";
    var returnedValue = Validate.isPresent(Optional.of(value), this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void isPresent_throwsException_whenValueIsNull() {
    var value = (Optional<String>) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.isPresent(value, this.exceptionSupplier)));
  }

  @Test
  public void isPresent_throwsException_whenValueIsEmptyOptional() {
    var value = Optional.empty();
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.isPresent(value, this.exceptionSupplier)));
  }

  @Test
  public void notEmpty_returnsValue_whenValueIsNotEmpty() {
    var value = "Not Empty";
    var returnedValue = Validate.notEmpty(value, this.exceptionSupplier);
    assertSame(value, returnedValue);
    value = " ";
    returnedValue = Validate.notEmpty(value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void notEmpty_throwsException_whenValueIsNullString() {
    var value = (String) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(value, this.exceptionSupplier)));
  }

  @Test
  public void notEmpty_throwsException_whenValueIsEmptyString() {
    var value = "";
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(value, this.exceptionSupplier)));
  }

  @Test
  public void notEmpty_returnsValue_whenValueIsNotEmptyArray() {
    var booleanArrayValue = new boolean[] {
      true, false
    };
    var returnedBooleanArrayValue = Validate.notEmpty(booleanArrayValue, this.exceptionSupplier);
    assertSame(booleanArrayValue, returnedBooleanArrayValue);
    var byteArrayValue = new byte[] {
      0, 1, 2
    };
    var returnedByteArrayValue = Validate.notEmpty(byteArrayValue, this.exceptionSupplier);
    assertSame(byteArrayValue, returnedByteArrayValue);
    var shortArrayValue = new short[] {
      0, 1, 2
    };
    var returnedShortArrayValue = Validate.notEmpty(shortArrayValue, this.exceptionSupplier);
    assertSame(shortArrayValue, returnedShortArrayValue);
    var charArrayValue = new char[] {
      0, 1, 2
    };
    var returnedCharArrayValue = Validate.notEmpty(charArrayValue, this.exceptionSupplier);
    assertSame(charArrayValue, returnedCharArrayValue);
    var intArrayValue = new int[] {
      0, 1, 2
    };
    var returnedIntArrayValue = Validate.notEmpty(intArrayValue, this.exceptionSupplier);
    assertSame(intArrayValue, returnedIntArrayValue);
    var longArrayValue = new long[] {
      0, 1, 2
    };
    var returnedLongArrayValue = Validate.notEmpty(longArrayValue, this.exceptionSupplier);
    assertSame(longArrayValue, returnedLongArrayValue);
    var floatArrayValue = new float[] {
      0.5f, 1.5f, 2.5f
    };
    var returnedFloatArrayValue = Validate.notEmpty(floatArrayValue, this.exceptionSupplier);
    assertSame(floatArrayValue, returnedFloatArrayValue);
    var doubleArrayValue = new double[] {
      0.5d, 1.5d, 2.5d
    };
    var returnedDoubleArrayValue = Validate.notEmpty(doubleArrayValue, this.exceptionSupplier);
    assertSame(doubleArrayValue, returnedDoubleArrayValue);
    var objectArrayValue = new String[] {
      "A", "B", "C"
    };
    var objectsValues = Validate.notEmpty(objectArrayValue, this.exceptionSupplier);
    assertSame(objectArrayValue, objectsValues);
  }

  @Test
  public void notEmpty_throwsException_whenValueIsNullArray() {
    var booleanArrayValue = (boolean[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(booleanArrayValue, this.exceptionSupplier)));
    var byteArrayValue = (byte[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(byteArrayValue, this.exceptionSupplier)));
    var shortArrayValue = (short[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(shortArrayValue, this.exceptionSupplier)));
    var charArrayValue = (char[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(charArrayValue, this.exceptionSupplier)));
    var intArrayValue = (int[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(intArrayValue, this.exceptionSupplier)));
    var longArrayValue = (long[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(longArrayValue, this.exceptionSupplier)));
    var floatArrayValue = (float[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(floatArrayValue, this.exceptionSupplier)));
    var doubleArrayValue = (double[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(doubleArrayValue, this.exceptionSupplier)));
    var objectsArrayValue = (Object[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(objectsArrayValue, this.exceptionSupplier)));
    var stringArrayValue = (String[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(stringArrayValue, this.exceptionSupplier)));
  }

  @Test
  public void notEmpty_throwsException_whenValueIsEmptyArray() {
    var emptyBooleanArrayValue = new boolean[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(emptyBooleanArrayValue, this.exceptionSupplier)));
    var emptyByteArrayValue = new byte[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(emptyByteArrayValue, this.exceptionSupplier)));
    var emptyShortArrayValue = new short[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(emptyShortArrayValue, this.exceptionSupplier)));
    var emptyCharArrayValue = new char[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(emptyCharArrayValue, this.exceptionSupplier)));
    var emptyIntArrayValue = new int[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(emptyIntArrayValue, this.exceptionSupplier)));
    var emptyLongArrayValue = new long[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(emptyLongArrayValue, this.exceptionSupplier)));
    var emptyFloatArrayValue = new float[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(emptyFloatArrayValue, this.exceptionSupplier)));
    var emptyDoubleArrayValue = new double[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(emptyDoubleArrayValue, this.exceptionSupplier)));
    var emptyObjectArrayValue = new String[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(emptyObjectArrayValue, this.exceptionSupplier)));
    var emptyStringArrayValue = new String[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(emptyStringArrayValue, this.exceptionSupplier)));
  }

  @Test
  public void notEmpty_returnsValue_whenValueIsNotEmptyCollection() {
    var listValue = List.of("Not Empty");
    var returnedListValue = Validate.notEmpty(listValue, this.exceptionSupplier);
    assertSame(listValue, returnedListValue);
    var setValue = Set.of("Not Empty", "Really Not Empty");
    var returnedSetValue = Validate.notEmpty(setValue, this.exceptionSupplier);
    assertSame(setValue, returnedSetValue);
  }

  @Test
  public void notEmpty_throwsException_whenValueIsNullCollection() {
    var nullListValue = (List<String>) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(nullListValue, this.exceptionSupplier)));
    var nullSetValue = (Set<String>) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(nullSetValue, this.exceptionSupplier)));
  }

  @Test
  public void notEmpty_throwsException_whenValueIsEmptyCollection() {
    var emptyListValue = List.of();
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(emptyListValue, this.exceptionSupplier)));
    var emptySetValue = List.of();
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(emptySetValue, this.exceptionSupplier)));
  }

  @Test
  public void notEmpty_returnsValue_whenValueIsNotEmptyMap() {
    var mapValue = Map.ofEntries(Map.entry("Not Empty Key", "Not Empty Value"));
    var returnedMapValue = Validate.notEmpty(mapValue, this.exceptionSupplier);
    assertSame(mapValue, returnedMapValue);
    var treeMapValue = Maps.of(new TreeMap<>(), Maps.entry("Not Empty Key", "Not Empty Value"));
    var returnedTreeMapValue = Validate.notEmpty(treeMapValue, this.exceptionSupplier);
    assertSame(treeMapValue, returnedTreeMapValue);
  }

  @Test
  public void notEmpty_throwsException_whenValueIsNullMap() {
    var value = (Map<String, String>) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(value, this.exceptionSupplier)));
  }

  @Test
  public void notEmpty_throwsException_whenValueIsEmptyMap() {
    var value = Map.of();
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notEmpty(value, this.exceptionSupplier)));
  }

  @Test
  public void notBlank_returnsValue_whenValueIsNotEmptyString() {
    var value = "Not Blank";
    var returnedValue = Validate.notBlank(value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void notBlank_returnsStrippedValue_whenValueIsNotEmptyStringAndContainsBlanks() {
    var value = "\tNot Blank ";
    var returnedValue = Validate.notBlank(value, this.exceptionSupplier);
    assertEquals(value.strip(), returnedValue);
  }

  @Test
  public void notBlank_throwsException_whenValueIsNullString() {
    var value = (String) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notBlank(value, this.exceptionSupplier)));
  }

  @Test
  public void notBlank_throwsException_whenValueIsEmptyString() {
    var value = "";
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notBlank(value, this.exceptionSupplier)));
  }

  @Test
  public void notBlank_throwsException_whenValueContainsOnlyBlanks() {
    var value = " \r\n\t\f ";
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notBlank(value, this.exceptionSupplier)));
  }

  @Test
  public void notContainsControlCharacters_returnsValue_whenValueNotContainsIsoControlCodes() {
    var value = "Not contains iso control codes";
    var returnedValue = Validate.notContainsControlCharacters(value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void notContainsControlCharacters_throwsException_whenValueContainsIsoControlCodes() {
    var controlCodes = new char[] {
      '\r', '\n', '\b', '\t', '\0'
    };
    for (var c : controlCodes) {
      var value = "Something " + c + " Something";
      assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notContainsControlCharacters(value, this.exceptionSupplier)));
    }
  }

  @Test
  public void notContainsControlCharacters_throwsException_whenValueIsNull() {
    var value = (String) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.notContainsControlCharacters(value, this.exceptionSupplier)));
  }

  @Test
  public void type_returnsValue_whenValueIsExactType() {
    var type = String.class;
    var value = "Test";
    var returnedvalue = Validate.type(type, value, this.exceptionSupplier);
    assertSame(value, returnedvalue);
  }

  @Test
  public void type_returnsValue_whenValueIsInheritedType() {
    var type = Map.class;
    var value = new TreeMap<String, String>();
    var returnedValue = Validate.type(type, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void type_throwsException_whenValueIsNotTypeOrNotInheritType() {
    var type = Integer.class;
    var value = "Test";
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.type(type, value, this.exceptionSupplier)));
  }

  @Test
  public void type_throwsNullPointerException_whenTypeIsNull() {
    var type = (Class<Object>) null;
    var value = (String) null;
    assertThrows(NullPointerException.class, ()->Validate.type(type, value, this.exceptionSupplier));
  }

  @Test
  public void type_throwsException_whenValueIsNull() {
    var value = (String) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.type(Integer.class, value, this.exceptionSupplier)));
  }

  public static enum TestEnumValue {
    enumValue1,
    enumValue2,
  }

  @Test
  public void enumValue_returnValue_whenExistsAnItemNamedAsValue() {
    var type = TestEnumValue.class;
    var value = TestEnumValue.enumValue1;
    var returnedValue = Validate.enumValueName(type, value.name(), this.exceptionSupplier);
    assertSame(value, returnedValue);
    type = TestEnumValue.class;
    value = TestEnumValue.enumValue2;
    returnedValue = Validate.enumValueName(type, value.name(), this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void enumValue_throwsException_whenAnItemNamedAsValueIsNotFound() {
    var type = TestEnumValue.class;
    var value = "invalidValue";
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.enumValueName(type, value, this.exceptionSupplier)));
  }

  @Test
  public void enumValue_throwsNullPointerException_whenAnTypeIsNull() {
    var type = (Class<TestEnumValue>) null;
    var value = (String) null;
    assertThrows(NullPointerException.class, ()->Validate.enumValueName(type, value, this.exceptionSupplier));
  }

  @Test
  public void enumValue_throwsException_whenAnValueIsNull() {
    var type = TestEnumValue.class;
    var value = (String) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.enumValueName(type, value, this.exceptionSupplier)));
  }

  @Test
  public void singleton_returnValue_whenArrayValueContainsOnlyOneValue() {
    var booleanArrayValue = new boolean[] {
      true
    };
    var returnedBooleanArrayValue = Validate.singleton(booleanArrayValue, this.exceptionSupplier);
    assertTrue(booleanArrayValue[0] == returnedBooleanArrayValue);
    var byteArrayValue = new byte[] {
      2
    };
    var returnedByteArrayValue = Validate.singleton(byteArrayValue, this.exceptionSupplier);
    assertEquals(byteArrayValue[0], returnedByteArrayValue);
    var shortArrayValue = new short[] {
      3
    };
    var returnedShortArrayValue = Validate.singleton(shortArrayValue, this.exceptionSupplier);
    assertEquals(shortArrayValue[0], returnedShortArrayValue);
    var charArrayValue = new char[] {
      '4'
    };
    var returnedCharArrayValue = Validate.singleton(charArrayValue, this.exceptionSupplier);
    assertEquals(charArrayValue[0], returnedCharArrayValue);
    var intArrayValue = new int[] {
      5
    };
    var returnedIntArrayValue = Validate.singleton(intArrayValue, this.exceptionSupplier);
    assertEquals(intArrayValue[0], returnedIntArrayValue);
    var longArrayValue = new long[] {
      6L
    };
    var returnedLongArrayValue = Validate.singleton(longArrayValue, this.exceptionSupplier);
    assertEquals(longArrayValue[0], returnedLongArrayValue);
    var floatArrayValue = new float[] {
      7.5f
    };
    var returnedFloatArrayValue = Validate.singleton(floatArrayValue, this.exceptionSupplier);
    assertEquals(floatArrayValue[0], returnedFloatArrayValue, 0f);
    var doubleArrayValue = new double[] {
      8.5d
    };
    var returnedDoubleArrayValue = Validate.singleton(doubleArrayValue, this.exceptionSupplier);
    assertEquals(doubleArrayValue[0], returnedDoubleArrayValue, 0d);
    var objectArrayValue = new String[] {
      "A"
    };
    var objectsValues = Validate.singleton(objectArrayValue, this.exceptionSupplier);
    assertEquals(objectArrayValue[0], objectsValues);
  }

  @Test
  public void singleton_throwsException_whenValueIsNullArray() {
    var nullBooleanArrayValue = (boolean[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(nullBooleanArrayValue, this.exceptionSupplier)));
    var nullByteArrayValue = (byte[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(nullByteArrayValue, this.exceptionSupplier)));
    var nullShortArrayValue = (short[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(nullShortArrayValue, this.exceptionSupplier)));
    var nullCharArrayValue = (char[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(nullCharArrayValue, this.exceptionSupplier)));
    var nullIntArrayValue = (int[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(nullIntArrayValue, this.exceptionSupplier)));
    var nullLongArrayValue = (long[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(nullLongArrayValue, this.exceptionSupplier)));
    var nullFloatArrayValue = (float[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(nullFloatArrayValue, this.exceptionSupplier)));
    var nullDoubleArrayValue = (double[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(nullDoubleArrayValue, this.exceptionSupplier)));
    var nullObjectArrayValue = (Object[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(nullObjectArrayValue, this.exceptionSupplier)));
    var nullStringArrayValue = (String[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(nullStringArrayValue, this.exceptionSupplier)));
  }

  @Test
  public void singleton_throwsException_whenValueIsEmptyArray() {
    var emptyBooleanArrayValue = new boolean[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(emptyBooleanArrayValue, this.exceptionSupplier)));
    var emptyByteArrayValue = new byte[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(emptyByteArrayValue, this.exceptionSupplier)));
    var emptyShortArrayValue = new short[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(emptyShortArrayValue, this.exceptionSupplier)));
    var emptyCharArrayValue = new char[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(emptyCharArrayValue, this.exceptionSupplier)));
    var emptyIntArrayValue = new int[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(emptyIntArrayValue, this.exceptionSupplier)));
    var emptyLongArrayValue = new long[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(emptyLongArrayValue, this.exceptionSupplier)));
    var emptyFloatArrayValue = new float[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(emptyFloatArrayValue, this.exceptionSupplier)));
    var emptyDoubleArrayValue = new double[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(emptyDoubleArrayValue, this.exceptionSupplier)));
    var emptyObjectArrayValue = new String[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(emptyObjectArrayValue, this.exceptionSupplier)));
    var emptyStringArrayValue = new String[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(emptyStringArrayValue, this.exceptionSupplier)));
  }

  @Test
  public void singleton_throwsException_whenValueIsNotSingletonArray() {
    var notSingletonBooleanArrayValue = new boolean[] {
      true, false
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(notSingletonBooleanArrayValue, this.exceptionSupplier)));
    var notSingletonByteArrayValue = new byte[] {
      2, 3
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(notSingletonByteArrayValue, this.exceptionSupplier)));
    var notSingletonShortArrayValue = new short[] {
      3, 4
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(notSingletonShortArrayValue, this.exceptionSupplier)));
    var notSingletonCharArrayValue = new char[] {
      '4', '5'
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(notSingletonCharArrayValue, this.exceptionSupplier)));
    var notSingletonIntArrayValue = new int[] {
      5, 6
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(notSingletonIntArrayValue, this.exceptionSupplier)));
    var notSingletonLongArrayValue = new long[] {
      6L, 7L
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(notSingletonLongArrayValue, this.exceptionSupplier)));
    var notSingletonFloatArrayValue = new float[] {
      7.5f, 8.5f
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(notSingletonFloatArrayValue, this.exceptionSupplier)));
    var notSingletonDoubleArrayValue = new double[] {
      8.5d, 9.5d
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(notSingletonDoubleArrayValue, this.exceptionSupplier)));
    var notSingletonObjectArrayValue = new Object[] {
      new Object(), "String is an Object"
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(notSingletonObjectArrayValue, this.exceptionSupplier)));
    var notSingletonStringArrayValue = new String[] {
      "11", "12"
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(notSingletonStringArrayValue, this.exceptionSupplier)));
  }

  @Test
  public void singleton_throwsException_whenArrayValueIsNull() {
    var value = (String[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(value, this.exceptionSupplier)));
  }

  @Test
  public void singleton_throwsException_whenArrayValueIsEmpty() {
    var value = new String[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(value, this.exceptionSupplier)));
  }

  @Test
  public void singleton_throwsException_whenArrayValueContainsMultipleValues() {
    var value = new String[] {
      "Not Singleton", "Really Not a Singleton"
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(value, this.exceptionSupplier)));
  }

  @Test
  public void singleton_returnValue_whenCollectionValueContainsOnlyOneValue() {
    var value = List.of("Singleton");
    var returnedValue = Validate.singleton(value, this.exceptionSupplier);
    assertSame(value.iterator().next(), returnedValue);
  }

  @Test
  public void singleton_throwsException_whenCollectionValueIsNull() {
    var value = (List<String>) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(value, this.exceptionSupplier)));
  }

  @Test
  public void singleton_throwsException_whenCollectionValueIsEmpty() {
    var value = List.of();
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(value, this.exceptionSupplier)));
  }

  @Test
  public void singleton_throwsException_whenCollectionValueContainsMultipleValues() {
    var value = List.of("Not Singleton", "Really Not a Singleton");
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.singleton(value, this.exceptionSupplier)));
  }

  @Test
  public void minLength_returnValue_whenStringValueHasExactMinLengthSize() {
    var minLength = 2;
    var value = "AB";
    var returnedValue = Validate.minLength(minLength, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void minLength_returnValue_whenStringValueIsLargerThanMinLength() {
    var minLength = 2;
    var value = "ABC";
    var returnedValue = Validate.minLength(minLength, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void minLength_throwsException_whenStringValueIsNull() {
    var minLength = 2;
    var value = (String) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, value, this.exceptionSupplier)));
  }

  @Test
  public void minLength_throwsException_whenStringValueIsSmallerThanMinLength() {
    var minLength = 2;
    var value = "A";
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, value, this.exceptionSupplier)));
  }

  @Test
  public void minLength_returnValue_whenArrayValueHasExactMinLengthSize() {
    var minLength = 2;
    var booleanArrayValue = new boolean[] {
      true, false
    };
    assertSame(booleanArrayValue, Validate.minLength(minLength, booleanArrayValue, this.exceptionSupplier));
    var byteArrayValue = new byte[] {
      2, 3
    };
    assertSame(byteArrayValue, Validate.minLength(minLength, byteArrayValue, this.exceptionSupplier));
    var shortArrayValue = new short[] {
      3, 4
    };
    assertSame(shortArrayValue, Validate.minLength(minLength, shortArrayValue, this.exceptionSupplier));
    var charArrayValue = new char[] {
      '4', '5'
    };
    assertSame(charArrayValue, Validate.minLength(minLength, charArrayValue, this.exceptionSupplier));
    var intArrayValue = new int[] {
      5, 6
    };
    assertSame(intArrayValue, Validate.minLength(minLength, intArrayValue, this.exceptionSupplier));
    var longArrayValue = new long[] {
      6L, 7L
    };
    assertSame(longArrayValue, Validate.minLength(minLength, longArrayValue, this.exceptionSupplier));
    var floatArrayValue = new float[] {
      7.5f, 8.5f
    };
    assertSame(floatArrayValue, Validate.minLength(minLength, floatArrayValue, this.exceptionSupplier));
    var doubleArrayValue = new double[] {
      8.5d, 9.5d
    };
    assertSame(doubleArrayValue, Validate.minLength(minLength, doubleArrayValue, this.exceptionSupplier));
    var objectArrayValue = new Object[] {
      new Object(), "String is an Object"
    };
    assertSame(objectArrayValue, Validate.minLength(minLength, objectArrayValue, this.exceptionSupplier));
    var stringArrayValue = new String[] {
      "10", "11"
    };
    assertSame(stringArrayValue, Validate.minLength(minLength, stringArrayValue, this.exceptionSupplier));
  }

  @Test
  public void minLength_returnValue_whenArrayValueIsLargerThanMinLength() {
    var minLength = 2;
    var booleanArrayValue = new boolean[] {
      true, false, true
    };
    assertSame(booleanArrayValue, Validate.minLength(minLength, booleanArrayValue, this.exceptionSupplier));
    var byteArrayValue = new byte[] {
      2, 3, 4
    };
    assertSame(byteArrayValue, Validate.minLength(minLength, byteArrayValue, this.exceptionSupplier));
    var shortArrayValue = new short[] {
      3, 4, 5
    };
    assertSame(shortArrayValue, Validate.minLength(minLength, shortArrayValue, this.exceptionSupplier));
    var charArrayValue = new char[] {
      '4', '5', '6'
    };
    assertSame(charArrayValue, Validate.minLength(minLength, charArrayValue, this.exceptionSupplier));
    var intArrayValue = new int[] {
      5, 6, 7
    };
    assertSame(intArrayValue, Validate.minLength(minLength, intArrayValue, this.exceptionSupplier));
    var longArrayValue = new long[] {
      6L, 7L, 8L
    };
    assertSame(longArrayValue, Validate.minLength(minLength, longArrayValue, this.exceptionSupplier));
    var floatArrayValue = new float[] {
      7.5f, 8.5f, 9.5f
    };
    assertSame(floatArrayValue, Validate.minLength(minLength, floatArrayValue, this.exceptionSupplier));
    var doubleArrayValue = new double[] {
      8.5d, 9.5d, 10.5f
    };
    assertSame(doubleArrayValue, Validate.minLength(minLength, doubleArrayValue, this.exceptionSupplier));
    var objectArrayValue = new Object[] {
      new Object(), "String is an Object", Long.valueOf(11L)
    };
    assertSame(objectArrayValue, Validate.minLength(minLength, objectArrayValue, this.exceptionSupplier));
    var stringArrayValue = new String[] {
      "10", "11", "12"
    };
    assertSame(stringArrayValue, Validate.minLength(minLength, stringArrayValue, this.exceptionSupplier));
  }

  @Test
  public void minLength_throwsException_whenArrayValueIsNull() {
    var minLength = 0;
    var booleanArrayValue = (boolean[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, booleanArrayValue, this.exceptionSupplier)));
    var byteArrayValue = (byte[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, byteArrayValue, this.exceptionSupplier)));
    var shortArrayValue = (short[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, shortArrayValue, this.exceptionSupplier)));
    var charArrayValue = (char[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, charArrayValue, this.exceptionSupplier)));
    var intArrayValue = (int[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, intArrayValue, this.exceptionSupplier)));
    var longArrayValue = (long[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, longArrayValue, this.exceptionSupplier)));
    var floatArrayValue = (float[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, floatArrayValue, this.exceptionSupplier)));
    var doubleArrayValue = (double[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, doubleArrayValue, this.exceptionSupplier)));
    var objectsArrayValue = (Object[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, objectsArrayValue, this.exceptionSupplier)));
    var stringArrayValue = (String[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, stringArrayValue, this.exceptionSupplier)));
  }

  @Test
  public void minLength_throwsException_whenArrayValueIsSmallerThanMinLength() {
    var minLength = 3;
    var booleanArrayValue = new boolean[] {
      true, false
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, booleanArrayValue, this.exceptionSupplier)));
    var byteArrayValue = new byte[] {
      2, 3
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, byteArrayValue, this.exceptionSupplier)));
    var shortArrayValue = new short[] {
      3, 4
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, shortArrayValue, this.exceptionSupplier)));
    var charArrayValue = new char[] {
      '4', '5'
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, charArrayValue, this.exceptionSupplier)));
    var intArrayValue = new int[] {
      5, 6
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, intArrayValue, this.exceptionSupplier)));
    var longArrayValue = new long[] {
      6L, 7L
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, longArrayValue, this.exceptionSupplier)));
    var floatArrayValue = new float[] {
      7.5f, 8.5f
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, floatArrayValue, this.exceptionSupplier)));
    var doubleArrayValue = new double[] {
      8.5d, 9.5d
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, doubleArrayValue, this.exceptionSupplier)));
    var objectArrayValue = new Object[] {
      new Object(), "String is an Object"
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, objectArrayValue, this.exceptionSupplier)));
    var stringArrayValue = new String[] {
      "11", "12"
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, stringArrayValue, this.exceptionSupplier)));
  }

  @Test
  public void minLength_returnValue_whenCollectionValueHasExactMinLengthSize() {
    var minLength = 2;
    var value = List.of("A", "B");
    var returnedValue = Validate.minLength(minLength, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void minLength_returnValue_whenMapValueIsLargerThanMinLength() {
    var minLength = 2;
    var value = List.of("A", "B", "C");
    var returnedValue = Validate.minLength(minLength, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void minLength_throwsException_whenCollectionValueIsNull() {
    var minLength = 2;
    var value = (List<String>) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, value, this.exceptionSupplier)));
  }

  @Test
  public void minLength_throwsException_whenCollectionValueIsSmallerThanMinLength() {
    var minLength = 2;
    var value = List.of("A");
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.minLength(minLength, value, this.exceptionSupplier)));
  }

  @Test
  public void maxLength_returnValue_whenStringValueHasExactMaxLengthSize() {
    var maxLength = 2;
    var value = "AB";
    var returnedValue = Validate.maxLength(maxLength, value, this.exceptionSupplier);
    assertSame("AB", returnedValue);
  }

  @Test
  public void maxLength_returnValue_whenStringValueIsSmallerThanMaxLength() {
    var maxLength = 2;
    var value = "A";
    var returnedValue = Validate.maxLength(maxLength, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void maxLength_throwsException_whenStringValueIsNull() {
    var maxLength = 2;
    var value = (String) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, value, this.exceptionSupplier)));
  }

  @Test
  public void maxLength_throwsException_whenStringValueIsLargerThanMaxLength() {
    var maxLength = 2;
    var value = "ABC";
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, value, this.exceptionSupplier)));
  }

  @Test
  public void maxLength_returnValue_whenArrayValueHasExactMaxLengthSize() {
    var maxLength = 3;
    var booleanArrayValue = new boolean[] {
      true, false, true
    };
    assertSame(booleanArrayValue, Validate.maxLength(maxLength, booleanArrayValue, this.exceptionSupplier));
    var byteArrayValue = new byte[] {
      2, 3, 4
    };
    assertSame(byteArrayValue, Validate.maxLength(maxLength, byteArrayValue, this.exceptionSupplier));
    var shortArrayValue = new short[] {
      3, 4, 5
    };
    assertSame(shortArrayValue, Validate.maxLength(maxLength, shortArrayValue, this.exceptionSupplier));
    var charArrayValue = new char[] {
      '4', '5', '6'
    };
    assertSame(charArrayValue, Validate.maxLength(maxLength, charArrayValue, this.exceptionSupplier));
    var intArrayValue = new int[] {
      5, 6, 7
    };
    assertSame(intArrayValue, Validate.maxLength(maxLength, intArrayValue, this.exceptionSupplier));
    var longArrayValue = new long[] {
      6L, 7L, 8L
    };
    assertSame(longArrayValue, Validate.maxLength(maxLength, longArrayValue, this.exceptionSupplier));
    var floatArrayValue = new float[] {
      7.5f, 8.5f, 9.5f
    };
    assertSame(floatArrayValue, Validate.maxLength(maxLength, floatArrayValue, this.exceptionSupplier));
    var doubleArrayValue = new double[] {
      8.5d, 9.5d, 10.5f
    };
    assertSame(doubleArrayValue, Validate.maxLength(maxLength, doubleArrayValue, this.exceptionSupplier));
    var objectArrayValue = new Object[] {
      new Object(), "String is an Object", Long.valueOf(11L)
    };
    assertSame(objectArrayValue, Validate.maxLength(maxLength, objectArrayValue, this.exceptionSupplier));
    var stringArrayValue = new String[] {
      "10", "11", "12"
    };
    assertSame(stringArrayValue, Validate.maxLength(maxLength, stringArrayValue, this.exceptionSupplier));
  }

  @Test
  public void maxLength_returnValue_whenArrayValueIsSmallerThanMaxLength() {
    var maxLength = 3;
    var booleanArrayValue = new boolean[] {
      true, false
    };
    assertSame(booleanArrayValue, Validate.maxLength(maxLength, booleanArrayValue, this.exceptionSupplier));
    var byteArrayValue = new byte[] {
      2, 3
    };
    assertSame(byteArrayValue, Validate.maxLength(maxLength, byteArrayValue, this.exceptionSupplier));
    var shortArrayValue = new short[] {
      3, 4
    };
    assertSame(shortArrayValue, Validate.maxLength(maxLength, shortArrayValue, this.exceptionSupplier));
    var charArrayValue = new char[] {
      '4', '5'
    };
    assertSame(charArrayValue, Validate.maxLength(maxLength, charArrayValue, this.exceptionSupplier));
    var intArrayValue = new int[] {
      5, 6
    };
    assertSame(intArrayValue, Validate.maxLength(maxLength, intArrayValue, this.exceptionSupplier));
    var longArrayValue = new long[] {
      6L, 7L
    };
    assertSame(longArrayValue, Validate.maxLength(maxLength, longArrayValue, this.exceptionSupplier));
    var floatArrayValue = new float[] {
      7.5f, 8.5f
    };
    assertSame(floatArrayValue, Validate.maxLength(maxLength, floatArrayValue, this.exceptionSupplier));
    var doubleArrayValue = new double[] {
      8.5d, 9.5d
    };
    assertSame(doubleArrayValue, Validate.maxLength(maxLength, doubleArrayValue, this.exceptionSupplier));
    var objectArrayValue = new Object[] {
      new Object(), "String is an Object"
    };
    assertSame(objectArrayValue, Validate.maxLength(maxLength, objectArrayValue, this.exceptionSupplier));
    var stringArrayValue = new String[] {
      "11", "12"
    };
    assertSame(stringArrayValue, Validate.maxLength(maxLength, stringArrayValue, this.exceptionSupplier));
  }

  @Test
  public void maxLength_throwsException_whenArrayValueIsNull() {
    var maxLength = 2;
    var booleanArrayValue = (boolean[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, booleanArrayValue, this.exceptionSupplier)));
    var byteArrayValue = (byte[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, byteArrayValue, this.exceptionSupplier)));
    var shortArrayValue = (short[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, shortArrayValue, this.exceptionSupplier)));
    var charArrayValue = (char[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, charArrayValue, this.exceptionSupplier)));
    var intArrayValue = (int[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, intArrayValue, this.exceptionSupplier)));
    var longArrayValue = (long[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, longArrayValue, this.exceptionSupplier)));
    var floatArrayValue = (float[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, floatArrayValue, this.exceptionSupplier)));
    var doubleArrayValue = (double[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, doubleArrayValue, this.exceptionSupplier)));
    var objectsArrayValue = (Object[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, objectsArrayValue, this.exceptionSupplier)));
    var stringArrayValue = (String[]) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, stringArrayValue, this.exceptionSupplier)));
  }

  @Test
  public void maxLength_throwsException_whenArrayValueIsLargerThanMaxLength() {
    var maxLength = 2;
    var booleanArrayValue = new boolean[] {
      true, false, true
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, booleanArrayValue, this.exceptionSupplier)));
    var byteArrayValue = new byte[] {
      2, 3, 4
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, byteArrayValue, this.exceptionSupplier)));
    var shortArrayValue = new short[] {
      3, 4, 5
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, shortArrayValue, this.exceptionSupplier)));
    var charArrayValue = new char[] {
      '4', '5', '6'
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, charArrayValue, this.exceptionSupplier)));
    var intArrayValue = new int[] {
      5, 6, 7
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, intArrayValue, this.exceptionSupplier)));
    var longArrayValue = new long[] {
      6L, 7L, 8L
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, longArrayValue, this.exceptionSupplier)));
    var floatArrayValue = new float[] {
      7.5f, 8.5f, 9.5f
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, floatArrayValue, this.exceptionSupplier)));
    var doubleArrayValue = new double[] {
      8.5d, 9.5d, 10.5d
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, doubleArrayValue, this.exceptionSupplier)));
    var objectArrayValue = new Object[] {
      new Object(), "String is an Object", Long.valueOf(11L)
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, objectArrayValue, this.exceptionSupplier)));
    var stringArrayValue = new String[] {
      "11", "12", "13"
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, stringArrayValue, this.exceptionSupplier)));
  }

  @Test
  public void maxLength_returnValue_whenCollectionValueHasExactMaxLengthSize() {
    var maxLength = 2;
    var value = List.of("A", "B");
    var returnedValue = Validate.maxLength(maxLength, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void maxLength_returnValue_whenCollectionValueIsSmallerThanMaxLength() {
    var maxLength = 2;
    var value = List.of("A");
    var returnedValue = Validate.maxLength(maxLength, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void maxLength_throwsException_whenCollectionValueIsNull() {
    var maxLength = 2;
    var value = (List<String>) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, value, this.exceptionSupplier)));
  }

  @Test
  public void maxLength_throwsException_whenCollectionValueIsLargerThanMaxLength() {
    var maxLength = 2;
    var value = List.of("A", "B", "C");
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.maxLength(maxLength, value, this.exceptionSupplier)));
  }

  @Test
  public void regex_returnValue_whenValueMatchesContentPattern() {
    var stringContentPattern = "\\d{3}";
    var contentPattern = Pattern.compile(stringContentPattern);
    var value = "123";
    var returnedValue = Validate.regex(stringContentPattern, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
    returnedValue = Validate.regex(contentPattern, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void regex_throwsException_whenValueNotMatchesContentPattern() {
    var stringContentPattern = "\\d{3}";
    var contentPattern = Pattern.compile(stringContentPattern);
    var value = "ABC";
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.regex(stringContentPattern, value, this.exceptionSupplier)));
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.regex(contentPattern, value, this.exceptionSupplier)));
  }

  @Test
  public void regex_throwsException_whenValueIsNull() {
    var stringContentPattern = "\\d{3}";
    var contentPattern = Pattern.compile(stringContentPattern);
    var value = (String) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.regex(stringContentPattern, value, this.exceptionSupplier)));
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.regex(contentPattern, value, this.exceptionSupplier)));
  }

  @Test
  public void maxLength_throwsNullPointerException_whenStringContentPatternIsNull() {
    var stringContentPattern = (String) null;
    var contentPattern = (Pattern) null;
    var value = "123";
    assertThrows(NullPointerException.class, ()->Validate.regex(stringContentPattern, value, this.exceptionSupplier));
    assertThrows(NullPointerException.class, ()->Validate.regex(contentPattern, value, this.exceptionSupplier));
  }

  @Test
  public void maxLength_throwsException_whenValueIsNull() {
    var stringContentPattern = "\\d{3}";
    var contentPattern = Pattern.compile("\\d{3}");
    var value = (String) null;
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.regex(stringContentPattern, value, this.exceptionSupplier)));
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.regex(contentPattern, value, this.exceptionSupplier)));
  }

  @Test
  public void maxLength_throwsException_whenValueDoesNotMatchContentPattern() {
    var stringContentPattern = "\\d{3}";
    var contentPattern = Pattern.compile("\\d{3}");
    var value = "ABC";
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.regex(stringContentPattern, value, this.exceptionSupplier)));
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.regex(contentPattern, value, this.exceptionSupplier)));
  }

  @Test
  public void optional_throwsNullPointerExceptions_whenotherValidateMethodIsNull() {
    var value = (String) null;
    var otherValidateMethodIsNull = (Exceptions.Supplier<String, RuntimeException>) null;
    assertThrows(this.exception.getClass(), ()->Validate.optional(value, otherValidateMethodIsNull));
  }

  @Test
  public void optionalMinValue_byteMinValueAndNullValue_returnNull_whenValueIsNull() {
    var minValue = (byte) 1;
    var value = (Byte) null;
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMinValue_byteMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var minValue = (byte) 1;
    var value = Byte.valueOf((byte) 0);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMinValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMinValue_byteMinValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var minValue = (byte) 1;
    var value = Byte.valueOf((byte) 0);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMinValue_byteMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var minValue = (byte) 1;
    var value = Byte.valueOf((byte) 1);
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMinValue_byteMinValueAndValue_returnValue_whenValueIsGreaterThanMinValue() {
    var minValue = (byte) 0;
    var value = Byte.valueOf((byte) 1);
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMinValue_shortMinValueAndNullValue_returnNull_whenValueIsNull() {
    var minValue = (short) 1;
    var value = (Short) null;
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMinValue_shortMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var minValue = (short) 1;
    var value = Short.valueOf((short) 0);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMinValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMinValue_shortMinValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var minValue = (short) 1;
    var value = Short.valueOf((short) 0);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMinValue_shortMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var minValue = (short) 1;
    var value = Short.valueOf((short) 1);
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMinValue_shortMinValueAndValue_returnValue_whenValueIsGreaterThanMinValue() {
    var minValue = (short) 0;
    var value = Short.valueOf((short) 1);
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMinValue_integerMinValueAndNullValue_returnNull_whenValueIsNull() {
    var minValue = 1;
    var value = (Integer) null;
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMinValue_integerMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var minValue = 1;
    var value = Integer.valueOf(0);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMinValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMinValue_integerMinValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var minValue = 1;
    var value = Integer.valueOf(0);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMinValue_integerMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var minValue = 1;
    var value = Integer.valueOf(1);
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMinValue_integerMinValueAndValue_returnValue_whenValueIsGreaterThanMinValue() {
    var minValue = 0;
    var value = Integer.valueOf(1);
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMinValue_longMinValueAndNullValue_returnNull_whenValueIsNull() {
    var minValue = 1L;
    var value = (Long) null;
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMinValue_longMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var minValue = 1L;
    var value = Long.valueOf(0L);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMinValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMinValue_longMinValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var minValue = 1L;
    var value = Long.valueOf(0L);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMinValue_longMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var minValue = 1L;
    var value = Long.valueOf(1L);
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMinValue_longMinValueAndValue_returnValue_whenValueIsGreaterThanMinValue() {
    var minValue = 0L;
    var value = Long.valueOf(1L);
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMinValue_floatMinValueAndNullValue_returnNull_whenValueIsNull() {
    var minValue = 1.0f;
    var value = (Float) null;
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMinValue_floatMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var minValue = 1.0f;
    var value = Float.valueOf(0.0f);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMinValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMinValue_floatMinValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var minValue = 1.0f;
    var value = Float.valueOf(0.0f);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMinValue_floatMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var minValue = 1.0f;
    var value = Float.valueOf(1.0f);
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMinValue_floatMinValueAndValue_returnValue_whenValueIsGreaterThanMinValue() {
    var minValue = 0.0f;
    var value = Float.valueOf(1.0f);
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMinValue_doubleMinValueAndNullValue_returnNull_whenValueIsNull() {
    var minValue = 1.0;
    var value = (Double) null;
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMinValue_doubleMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var minValue = 1.0;
    var value = Double.valueOf(0.0);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMinValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMinValue_doubleMinValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var minValue = 1.0;
    var value = Double.valueOf(0.0);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMinValue_doubleMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var minValue = 1.0;
    var value = Double.valueOf(1.0);
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMinValue_doubleMinValueAndValue_returnValue_whenValueIsGreaterThanMinValue() {
    var minValue = 0.0;
    var value = Double.valueOf(1.0);
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMinValue_bigIntegerMinValueAndValue_throwsNullPointerException_whenMinValueIsNull() {
    var minValue = (BigInteger) null;
    var value = (BigInteger) null;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMinValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMinValue_bigIntegerMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var minValue = BigInteger.valueOf(1);
    var value = BigInteger.valueOf(0);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMinValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMinValue_bigIntegerMinValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var minValue = BigInteger.valueOf(1);
    var value = BigInteger.valueOf(0);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMinValue_bigIntegerMinValueAndNullValue_returnNull_whenValueIsNull() {
    var minValue = BigInteger.valueOf(1);
    var value = (BigInteger) null;
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMinValue_bigIntegerMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var minValue = BigInteger.valueOf(1);
    var value = BigInteger.valueOf(1);
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMinValue_bigIntegerMinValueAndValue_returnValue_whenValueIsGreaterThanMinValue() {
    var minValue = BigInteger.valueOf(0);
    var value = BigInteger.valueOf(1);
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMinValue_bigDecimalMinValueAndValue_throwsNullPointerException_whenMinValueIsNull() {
    var minValue = (BigDecimal) null;
    var value = (BigDecimal) null;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMinValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMinValue_bigDecimalMinValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var minValue = BigDecimal.valueOf(1.0);
    var value = BigDecimal.valueOf(0.0);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMinValue(minValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMinValue_bigDecimalMinValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsSmallerThanMinValue() {
    var minValue = BigDecimal.valueOf(1.0);
    var value = BigDecimal.valueOf(0.0);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinValue(minValue, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMinValue_bigDecimalMinValueAndNullValue_returnNull_whenValueIsNull() {
    var minValue = BigDecimal.valueOf(1.0);
    var value = (BigDecimal) null;
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMinValue_bigDecimalMinValueAndValue_returnValue_whenValueIsEqualsMinValue() {
    var minValue = BigDecimal.valueOf(1.0);
    var value = BigDecimal.valueOf(1.0);
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMinValue_bigDecimalMinValueAndValue_returnValue_whenValueIsGreaterThanMinValue() {
    var minValue = BigDecimal.valueOf(0.0);
    var value = BigDecimal.valueOf(1.0);
    var returnedValue = Validate.optionalMinValue(minValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMaxValue_byteMaxValueAndNullValue_returnNull_whenValueIsNull() {
    var maxValue = (byte) 1;
    var value = (Byte) null;
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMaxValue_byteMaxValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var maxValue = (byte) 1;
    var value = Byte.valueOf((byte) 2);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMaxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMaxValue_byteMaxValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsGreaterThanMaxValue() {
    var maxValue = (byte) 1;
    var value = Byte.valueOf((byte) 2);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMaxValue_byteMaxValueAndValue_returnValue_whenValueIsEqualsMaxValue() {
    var maxValue = (byte) 1;
    var value = Byte.valueOf((byte) 1);
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMaxValue_byteMaxValueAndValue_returnValue_whenValueIsSmallerThanMaxValue() {
    var maxValue = (byte) 2;
    var value = Byte.valueOf((byte) 1);
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMaxValue_shortMaxValueAndNullValue_returnNull_whenValueIsNull() {
    var maxValue = (short) 1;
    var value = (Short) null;
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMaxValue_shortMaxValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var maxValue = (short) 1;
    Short value = Short.valueOf((short) 2);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMaxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMaxValue_shortMaxValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsGreaterThanMaxValue() {
    var maxValue = (short) 1;
    var value = Short.valueOf((short) 2);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMaxValue_shortMaxValueAndValue_returnValue_whenValueIsEqualsMaxValue() {
    var maxValue = (short) 1;
    var value = Short.valueOf((short) 1);
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMaxValue_shortMaxValueAndValue_returnValue_whenValueIsSmallerThanMaxValue() {
    var maxValue = (short) 2;
    Short value = Short.valueOf((short) 1);
    Short returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMaxValue_integerMaxValueAndNullValue_returnNull_whenValueIsNull() {
    var maxValue = 1;
    var value = (Integer) null;
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMaxValue_integerMaxValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var maxValue = 1;
    var value = Integer.valueOf(2);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMaxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMaxValue_integerMaxValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsGreaterThanMaxValue() {
    var maxValue = 1;
    var value = Integer.valueOf(2);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMaxValue_integerMaxValueAndValue_returnValue_whenValueIsEqualsMaxValue() {
    var maxValue = 1;
    var value = Integer.valueOf(1);
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMaxValue_integerMaxValueAndValue_returnValue_whenValueIsSmallerThanMaxValue() {
    var maxValue = 2;
    var value = Integer.valueOf(1);
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMaxValue_longMaxValueAndNullValue_returnNull_whenValueIsNull() {
    var maxValue = 1L;
    var value = (Long) null;
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMaxValue_longMaxValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var maxValue = 1L;
    var value = Long.valueOf(2L);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMaxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMaxValue_longMaxValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsGreaterThanMaxValue() {
    var maxValue = 1L;
    var value = Long.valueOf(2L);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMaxValue_longMaxValueAndValue_returnValue_whenValueIsEqualsMaxValue() {
    var maxValue = 1L;
    var value = Long.valueOf(1L);
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMaxValue_longMaxValueAndValue_returnValue_whenValueIsSmallerThanMaxValue() {
    var maxValue = 2L;
    var value = Long.valueOf(1L);
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMaxValue_floatMaxValueAndNullValue_returnNull_whenValueIsNull() {
    var maxValue = 1.0f;
    var value = (Float) null;
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMaxValue_floatMaxValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var maxValue = 1.0f;
    var value = Float.valueOf(2.0f);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMaxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMaxValue_floatMaxValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsGreaterThanMaxValue() {
    var maxValue = 1.0f;
    var value = Float.valueOf(2.0f);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMaxValue_floatMaxValueAndValue_returnValue_whenValueIsEqualsMaxValue() {
    var maxValue = 1.0f;
    var value = Float.valueOf(1.0f);
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMaxValue_floatMaxValueAndValue_returnValue_whenValueIsSmallerThanMaxValue() {
    var maxValue = 2.0f;
    var value = Float.valueOf(1.0f);
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMaxValue_doubleMaxValueAndNullValue_returnNull_whenValueIsNull() {
    var maxValue = 1.0;
    var value = (Double) null;
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMaxValue_doubleMaxValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var maxValue = 1.0;
    var value = Double.valueOf(2.0);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMaxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMaxValue_doubleMaxValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsGreaterThanMaxValue() {
    var maxValue = 1.0;
    var value = Double.valueOf(2.0);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMaxValue_doubleMaxValueAndValue_returnValue_whenValueIsEqualsMaxValue() {
    var maxValue = 1.0;
    var value = Double.valueOf(1.0);
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMaxValue_doubleMaxValueAndValue_returnValue_whenValueIsSmallerThanMaxValue() {
    var maxValue = 2.0;
    var value = Double.valueOf(1.0);
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMaxValue_bigIntegerMaxValueAndValue_throwsNullPointerException_whenMaxValueIsNull() {
    var maxValue = (BigInteger) null;
    var value = (BigInteger) null;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMaxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMaxValue_bigIntegerMaxValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var maxValue = BigInteger.valueOf(0);
    var value = BigInteger.valueOf(1);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMaxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMaxValue_bigIntegerMaxValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsGreaterThanMaxValue() {
    var maxValue = BigInteger.valueOf(0);
    var value = BigInteger.valueOf(1);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMaxValue_bigIntegerMaxValueAndNullValue_returnNull_whenValueIsNull() {
    var maxValue = BigInteger.valueOf(1);
    var value = (BigInteger) null;
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMaxValue_bigIntegerMaxValueAndValue_returnValue_whenValueIsEqualsMaxValue() {
    var maxValue = BigInteger.valueOf(1);
    var value = BigInteger.valueOf(1);
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMaxValue_bigIntegerMaxValueAndValue_returnValue_whenValueIsSmallerThanMaxValue() {
    var maxValue = BigInteger.valueOf(1);
    var value = BigInteger.valueOf(0);
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMaxValue_bigDecimalMaxValueAndValue_throwsNullPointerException_whenMaxValueIsNull() {
    var maxValue = (BigDecimal) null;
    var value = (BigDecimal) null;
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMaxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMaxValue_bigDecimalMaxValueAndValue_throwsNullPointerException_whenExceptionSupplierIsNull() {
    var maxValue = BigDecimal.valueOf(0.0);
    var value = BigDecimal.valueOf(1.0);
    var nullExceptionSupplier = (Supplier<Exception, Exception>) null;
    assertThrows(NullPointerException.class, ()->Validate.optionalMaxValue(maxValue, value, nullExceptionSupplier));
  }

  @Test
  public void optionalMaxValue_bigDecimalMaxValueAndValue_throwsExceptionSupplierReturnedException_whenValueIsGreaterThanMaxValue() {
    var maxValue = BigDecimal.valueOf(0.0);
    var value = BigDecimal.valueOf(1.0);
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMaxValue_bigDecimalMaxValueAndNullValue_returnNull_whenValueIsNull() {
    var maxValue = BigDecimal.valueOf(1.0);
    var value = (BigDecimal) null;
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMaxValue_bigDecimalMaxValueAndValue_returnValue_whenValueIsEqualsMaxValue() {
    var maxValue = BigDecimal.valueOf(1.0);
    var value = BigDecimal.valueOf(1.0);
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMaxValue_bigDecimalMaxValueAndValue_returnValue_whenValueIsSmallerThanMaxValue() {
    var maxValue = BigDecimal.valueOf(1.0);
    var value = BigDecimal.valueOf(0.0);
    var returnedValue = Validate.optionalMaxValue(maxValue, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalNotEmpty_returnValue_whenValueIsNullOrNotEmpty() {
    var value = (String) null;
    var returnedValue = Validate.optionalNotEmpty(value, this.exceptionSupplier);
    assertNull(returnedValue);
    value = "Not Empty";
    returnedValue = Validate.optionalNotEmpty(value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void optionalNotEmpty_throwsException_whenValueIsEmpty() {
    var value = "";
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalNotEmpty(value, this.exceptionSupplier)));
  }

  @Test
  public void optionalNotEmpty_returnValue_whenValueIsNullArray() {
    var nullBooleanArrayValue = (boolean[]) null;
    var returnedBooleanArrayValue = Validate.optionalNotEmpty(nullBooleanArrayValue, this.exceptionSupplier);
    assertNull(returnedBooleanArrayValue);
    var nullByteArrayValue = (byte[]) null;
    var returnedByteArrayValue = Validate.optionalNotEmpty(nullByteArrayValue, this.exceptionSupplier);
    assertNull(returnedByteArrayValue);
    var nullShortArrayValue = (short[]) null;
    var returnedShortArrayValue = Validate.optionalNotEmpty(nullShortArrayValue, this.exceptionSupplier);
    assertNull(returnedShortArrayValue);
    var nullCharArrayValue = (char[]) null;
    var returnedCharArrayValue = Validate.optionalNotEmpty(nullCharArrayValue, this.exceptionSupplier);
    assertNull(returnedCharArrayValue);
    var nullIntArrayValue = (int[]) null;
    var returnedIntArrayValue = Validate.optionalNotEmpty(nullIntArrayValue, this.exceptionSupplier);
    assertNull(returnedIntArrayValue);
    var nullLongArrayValue = (long[]) null;
    var returnedLongArrayValue = Validate.optionalNotEmpty(nullLongArrayValue, this.exceptionSupplier);
    assertNull(returnedLongArrayValue);
    var nullFloatArrayValue = (float[]) null;
    var returnedFloatArrayValue = Validate.optionalNotEmpty(nullFloatArrayValue, this.exceptionSupplier);
    assertNull(returnedFloatArrayValue);
    var nullDoubleArrayValue = (double[]) null;
    var returnedDoubleArrayValue = Validate.optionalNotEmpty(nullDoubleArrayValue, this.exceptionSupplier);
    assertNull(returnedDoubleArrayValue);
    var nullObjectArrayValue = (Object[]) null;
    var returnedObjectArrayValue = Validate.optionalNotEmpty(nullObjectArrayValue, this.exceptionSupplier);
    assertNull(returnedObjectArrayValue);
  }

  @Test
  public void optionalNotEmpty_returnValue_whenValueIsNotEmptyArray() {
    var booleanArrayValue = new boolean[] {
      true, false
    };
    var returnedBooleanArrayValue = Validate.optionalNotEmpty(booleanArrayValue, this.exceptionSupplier);
    assertSame(booleanArrayValue, returnedBooleanArrayValue);
    var byteArrayValue = new byte[] {
      1, 2, 3
    };
    var returnedByteArrayValue = Validate.optionalNotEmpty(byteArrayValue, this.exceptionSupplier);
    assertSame(byteArrayValue, returnedByteArrayValue);
    var shortArrayValue = new short[] {
      1, 2, 3
    };
    var returnedShortArrayValue = Validate.optionalNotEmpty(shortArrayValue, this.exceptionSupplier);
    assertSame(shortArrayValue, returnedShortArrayValue);
    var charArrayValue = new char[] {
      'A', 'B', 'C'
    };
    var returnedCharArrayValue = Validate.optionalNotEmpty(charArrayValue, this.exceptionSupplier);
    assertSame(charArrayValue, returnedCharArrayValue);
    var intArrayValue = new int[] {
      1, 2, 3
    };
    var returnedIntArrayValue = Validate.optionalNotEmpty(intArrayValue, this.exceptionSupplier);
    assertSame(intArrayValue, returnedIntArrayValue);
    var longArrayValue = new long[] {
      1, 2, 3
    };
    var returnedLongArrayValue = Validate.optionalNotEmpty(longArrayValue, this.exceptionSupplier);
    assertSame(longArrayValue, returnedLongArrayValue);
    var floatArrayValue = new float[] {
      0.5f, 1.0f, 1.5f
    };
    var returnedFloatArrayValue = Validate.optionalNotEmpty(floatArrayValue, this.exceptionSupplier);
    assertSame(floatArrayValue, returnedFloatArrayValue);
    var doubleArrayValue = new double[] {
      0.5d, 1.0d, 1.5d
    };
    var returnedDoubleArrayValue = Validate.optionalNotEmpty(doubleArrayValue, this.exceptionSupplier);
    assertSame(doubleArrayValue, returnedDoubleArrayValue);
    var objectArrayValue = new Object[] {
      "A", "B", "C"
    };
    var returnedObjectArrayValue = Validate.optionalNotEmpty(objectArrayValue, this.exceptionSupplier);
    assertSame(objectArrayValue, returnedObjectArrayValue);
  }

  @Test
  public void optionalNotEmpty_throwsException_whenValueIsEmptyArray() {
    var emptyBooleanArrayValue = new boolean[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalNotEmpty(emptyBooleanArrayValue, this.exceptionSupplier)));
    var emptyByteArrayValue = new byte[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalNotEmpty(emptyByteArrayValue, this.exceptionSupplier)));
    var emptyShortArrayValue = new short[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalNotEmpty(emptyShortArrayValue, this.exceptionSupplier)));
    var emptyCharArrayValue = new char[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalNotEmpty(emptyCharArrayValue, this.exceptionSupplier)));
    var emptyIntArrayValue = new int[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalNotEmpty(emptyIntArrayValue, this.exceptionSupplier)));
    var emptyLongArrayValue = new long[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalNotEmpty(emptyLongArrayValue, this.exceptionSupplier)));
    var emptyFloatArrayValue = new float[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalNotEmpty(emptyFloatArrayValue, this.exceptionSupplier)));
    var emptyDoubleArrayValue = new double[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalNotEmpty(emptyDoubleArrayValue, this.exceptionSupplier)));
    var emptyObjectArrayValue = new String[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalNotEmpty(emptyObjectArrayValue, this.exceptionSupplier)));
    var emptyStringArrayValue = new String[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalNotEmpty(emptyStringArrayValue, this.exceptionSupplier)));
  }

  @Test
  public void optionalNotEmpty_returnValue_whenValueIsNullCollection() {
    var value = (List<String>) null;
    var returnedValue = Validate.optionalNotEmpty(value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalNotEmpty_returnValue_whenValueIsNotEmptyCollection() {
    var value = List.of("Not Empty");
    var returnedValue = Validate.optionalNotEmpty(value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void optionalNotEmpty_throwsException_whenValueIsEmptyCollection() {
    var emptyListValue = List.of();
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalNotEmpty(emptyListValue, this.exceptionSupplier)));
    var emptySetValue = List.of();
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalNotEmpty(emptySetValue, this.exceptionSupplier)));
  }

  @Test
  public void optionalNotEmpty_returnValue_whenValueIsNullMap() {
    var value = (Map<String, String>) null;
    var returnedValue = Validate.optionalNotEmpty(value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalNotEmpty_returnValue_whenValueIsNotEmptyMap() {
    var value = Map.of("Not Empty Key", "Not Empty Value");
    var returnedValue = Validate.optionalNotEmpty(value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void optionalNotEmpty_throwsException_whenValueIsEmptyMap() {
    var emptyMapValue = Map.of();
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalNotEmpty(emptyMapValue, this.exceptionSupplier)));
  }

  public void optionalNotBlank_returnValue_whenValueIsNull() {
    var value = (String) null;
    var returnedValue = Validate.optionalNotBlank(value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalNotBlank_returnValue_whenValueIsNotBlank() {
    var value = "ABC";
    var returnedValue = Validate.optionalNotBlank(value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void optionalNotBlank_throwsException_whenValueIsBlank() {
    var value = " ";
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalNotBlank(value, this.exceptionSupplier)));
  }

  @Test
  public void optionalNotContainsControlCharacters_returnValue_whenValueIsNull() {
    var value = (String) null;
    var returnedValue = Validate.optionalNotContainsControlCharacters(value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalNotContainsControlCharacters_returnValue_whenValueotContainsControlCharacters() {
    var value = "Not contains iso control codes";
    var returnedValue = Validate.optionalNotContainsControlCharacters(value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void optionalNotContainsIsoControlCodes_throwsException_whenValueContainsControlCharacters() {
    var controlCodes = new char[] {
      '\r', '\n', '\b', '\t', '\0'
    };
    for (var c : controlCodes) {
      var value = "Something " + c + " Something";
      assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalNotContainsControlCharacters(value, this.exceptionSupplier)));
    }
  }

  @Test
  public void optionalType_returnValue_whenValueIsNull() {
    var type = String.class;
    var value = (String) null;
    var returnedValue = Validate.optionalType(type, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalType_returnValue_whenValueMatchesType() {
    var type = String.class;
    var value = "Test";
    var returnedValue = Validate.optionalType(type, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void optionalType_throwsException_whenValueIsNotTypeOrNotInheritType() {
    var type = Integer.class;
    var value = "Test";
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalType(type, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalEnumValue_returnValue_whenValueIsNull() {
    var type = TestEnumValue.class;
    var value = (String) null;
    var returnedValue = Validate.optionalEnumValueName(type, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalEnumValue_returnValue_whenValueIsFound() {
    var type = TestEnumValue.class;
    var value = TestEnumValue.enumValue1;
    var returnedValue = Validate.optionalEnumValueName(type, value.name(), this.exceptionSupplier);
    assertSame(value, returnedValue);
    type = TestEnumValue.class;
    value = TestEnumValue.enumValue2;
    returnedValue = Validate.optionalEnumValueName(type, value.name(), this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void optionalEnumValue_throwsException_whenAnItemNamedAsValueIsNotFound() {
    var type = TestEnumValue.class;
    var value = "invalidValue";
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalEnumValueName(type, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalSingletonSuccess_returnValue_whenArrayValueIsNull() {
    var value = (String[]) null;
    var returnedValue = Validate.optionalSingleton(value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalSingletonSuccess_returnValue_whenArrayValueIsSingleton() {
    var value = new String[] {
      "Singleton"
    };
    var returnedValue = Validate.optionalSingleton(value, this.exceptionSupplier);
    assertSame(value[0], returnedValue);
  }

  @Test
  public void optionalSingleton_throwsException_whenArrayValueIsEmpty() {
    var value = new String[] {};
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalSingleton(value, this.exceptionSupplier)));
  }

  @Test
  public void optionalSingleton_throwsException_whenArrayValueContainsMultipleValues() {
    var value = new String[] {
      "Not Singleton", "Really Not a Singleton"
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalSingleton(value, this.exceptionSupplier)));
  }

  @Test
  public void optionalSingleton_returnValue_whenCollectionValueIsNull() {
    var value = (List<String>) null;
    var returnedValue = Validate.optionalSingleton(value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalSingleton_returnValue_whenCollectionValueContainsOnlyOneValue() {
    var value = List.of("Singleton");
    var returnedValue = Validate.optionalSingleton(value, this.exceptionSupplier);
    assertSame(value.iterator().next(), returnedValue);
  }

  @Test
  public void optionalSingleton_throwsException_whenCollectionValueIsEmpty() {
    var value = List.of();
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalSingleton(value, this.exceptionSupplier)));
  }

  @Test
  public void optionalSingleton_throwsException_whenCollectionValueContainsMultipleValues() {
    var value = List.of("Not Singleton", "Really Not a Singleton");
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalSingleton(value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMinLength_returnValue_whenStringValueIsNull() {
    var minLength = 2;
    var value = (String) null;
    var returnedValue = Validate.optionalMinLength(minLength, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMinLength_returnValue_whenStringValueHasExactMinLengthSize() {
    var minLength = 2;
    var value = "AB";
    var returnedValue = Validate.optionalMinLength(minLength, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void optionalMinLength_returnValue_whenStringValueIsLargerThanMinLength() {
    var minLength = 2;
    var value = "ABC";
    var returnedValue = Validate.optionalMinLength(minLength, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void optionalMinLength_throwsException_whenStringValueIsSmallerThanMinLength() {
    var minLength = 2;
    var value = "A";
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinLength(minLength, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMinLength_returnValue_whenArrayValueIsNull() {
    var minLength = 2;
    var nullBooleanArrayValue = (boolean[]) null;
    var returnedBooleanArrayValue = Validate.optionalMinLength(minLength, nullBooleanArrayValue, this.exceptionSupplier);
    assertNull(returnedBooleanArrayValue);
    var nullByteArrayValue = (byte[]) null;
    var returnedByteArrayValue = Validate.optionalMinLength(minLength, nullByteArrayValue, this.exceptionSupplier);
    assertNull(returnedByteArrayValue);
    var nullShortArrayValue = (short[]) null;
    var returnedShortArrayValue = Validate.optionalMinLength(minLength, nullShortArrayValue, this.exceptionSupplier);
    assertNull(returnedShortArrayValue);
    var nullCharArrayValue = (char[]) null;
    var returnedCharArrayValue = Validate.optionalMinLength(minLength, nullCharArrayValue, this.exceptionSupplier);
    assertNull(returnedCharArrayValue);
    var nullIntArrayValue = (int[]) null;
    var returnedIntArrayValue = Validate.optionalMinLength(minLength, nullIntArrayValue, this.exceptionSupplier);
    assertNull(returnedIntArrayValue);
    var nullLongArrayValue = (long[]) null;
    var returnedLongArrayValue = Validate.optionalMinLength(minLength, nullLongArrayValue, this.exceptionSupplier);
    assertNull(returnedLongArrayValue);
    var nullFloatArrayValue = (float[]) null;
    var returnedFloatArrayValue = Validate.optionalMinLength(minLength, nullFloatArrayValue, this.exceptionSupplier);
    assertNull(returnedFloatArrayValue);
    var nullDoubleArrayValue = (double[]) null;
    var returnedDoubleArrayValue = Validate.optionalMinLength(minLength, nullDoubleArrayValue, this.exceptionSupplier);
    assertNull(returnedDoubleArrayValue);
    var nullObjectArrayValue = (Object[]) null;
    var returnedObjectArrayValue = Validate.optionalMinLength(minLength, nullObjectArrayValue, this.exceptionSupplier);
    assertNull(returnedObjectArrayValue);
  }

  @Test
  public void optionalMinLength_returnValue_whenArrayValueHasExactMinLengthSize() {
    var minLength = 2;
    var booleanArrayValue = new boolean[] {
      true, false
    };
    var returnedBooleanArrayValue = Validate.optionalMinLength(minLength, booleanArrayValue, this.exceptionSupplier);
    assertSame(booleanArrayValue, returnedBooleanArrayValue);
    var byteArrayValue = new byte[] {
      2, 3
    };
    var returnedByteArrayValue = Validate.optionalMinLength(minLength, byteArrayValue, this.exceptionSupplier);
    assertSame(byteArrayValue, returnedByteArrayValue);
    var shortArrayValue = new short[] {
      3, 4
    };
    var returnedShortArrayValue = Validate.optionalMinLength(minLength, shortArrayValue, this.exceptionSupplier);
    assertSame(shortArrayValue, returnedShortArrayValue);
    var charArrayValue = new char[] {
      'D', 'E'
    };
    var returnedCharArrayValue = Validate.optionalMinLength(minLength, charArrayValue, this.exceptionSupplier);
    assertSame(charArrayValue, returnedCharArrayValue);
    var intArrayValue = new int[] {
      5, 6
    };
    var returnedIntArrayValue = Validate.optionalMinLength(minLength, intArrayValue, this.exceptionSupplier);
    assertSame(intArrayValue, returnedIntArrayValue);
    var longArrayValue = new long[] {
      6L, 7L
    };
    var returnedLongArrayValue = Validate.optionalMinLength(minLength, longArrayValue, this.exceptionSupplier);
    assertSame(longArrayValue, returnedLongArrayValue);
    var floatArrayValue = new float[] {
      7.5f, 8.5f
    };
    var returnedFloatArrayValue = Validate.optionalMinLength(minLength, floatArrayValue, this.exceptionSupplier);
    assertSame(floatArrayValue, returnedFloatArrayValue);
    var doubleArrayValue = new double[] {
      8.5d, 9.5d
    };
    var returnedDoubleArrayValue = Validate.optionalMinLength(minLength, doubleArrayValue, this.exceptionSupplier);
    assertSame(doubleArrayValue, returnedDoubleArrayValue);
    var objectArrayValue = new Object[] {
      "J", "K"
    };
    var returnedObjectArrayValue = Validate.optionalMinLength(minLength, objectArrayValue, this.exceptionSupplier);
    assertSame(objectArrayValue, returnedObjectArrayValue);
  }

  @Test
  public void optionalMinLength_returnValue_whenArrayValueIsLargerThanMinLength() {
    var minLength = 2;
    var booleanArrayValue = new boolean[] {
      true, false
    };
    var returnedBooleanArrayValue = Validate.optionalMinLength(minLength, booleanArrayValue, this.exceptionSupplier);
    assertSame(booleanArrayValue, returnedBooleanArrayValue);
    var byteArrayValue = new byte[] {
      2, 3, 4
    };
    var returnedByteArrayValue = Validate.optionalMinLength(minLength, byteArrayValue, this.exceptionSupplier);
    assertSame(byteArrayValue, returnedByteArrayValue);
    var shortArrayValue = new short[] {
      3, 4, 5
    };
    var returnedShortArrayValue = Validate.optionalMinLength(minLength, shortArrayValue, this.exceptionSupplier);
    assertSame(shortArrayValue, returnedShortArrayValue);
    var charArrayValue = new char[] {
      'D', 'E', 'F'
    };
    var returnedCharArrayValue = Validate.optionalMinLength(minLength, charArrayValue, this.exceptionSupplier);
    assertSame(charArrayValue, returnedCharArrayValue);
    var intArrayValue = new int[] {
      5, 6, 7
    };
    var returnedIntArrayValue = Validate.optionalMinLength(minLength, intArrayValue, this.exceptionSupplier);
    assertSame(intArrayValue, returnedIntArrayValue);
    var longArrayValue = new long[] {
      6L, 7L, 8L
    };
    var returnedLongArrayValue = Validate.optionalMinLength(minLength, longArrayValue, this.exceptionSupplier);
    assertSame(longArrayValue, returnedLongArrayValue);
    var floatArrayValue = new float[] {
      7.5f, 8.5f, 9.5f
    };
    var returnedFloatArrayValue = Validate.optionalMinLength(minLength, floatArrayValue, this.exceptionSupplier);
    assertSame(floatArrayValue, returnedFloatArrayValue);
    var doubleArrayValue = new double[] {
      8.5d, 9.5d, 10.5d
    };
    var returnedDoubleArrayValue = Validate.optionalMinLength(minLength, doubleArrayValue, this.exceptionSupplier);
    assertSame(doubleArrayValue, returnedDoubleArrayValue);
    var objectArrayValue = new Object[] {
      "J", "K", "L"
    };
    var returnedObjectArrayValue = Validate.optionalMinLength(minLength, objectArrayValue, this.exceptionSupplier);
    assertSame(objectArrayValue, returnedObjectArrayValue);
  }

  @Test
  public void optionalMinLength_throwsException_whenArrayValueIsSmallerThanMinLength() {
    var minLength = 2;
    var booleanArrayValue = new boolean[] {
      true
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinLength(minLength, booleanArrayValue, this.exceptionSupplier)));
    var byteArrayValue = new byte[] {
      2
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinLength(minLength, byteArrayValue, this.exceptionSupplier)));
    var shortArrayValue = new short[] {
      3
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinLength(minLength, shortArrayValue, this.exceptionSupplier)));
    var charArrayValue = new char[] {
      'D'
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinLength(minLength, charArrayValue, this.exceptionSupplier)));
    var intArrayValue = new int[] {
      5
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinLength(minLength, intArrayValue, this.exceptionSupplier)));
    var longArrayValue = new long[] {
      6L
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinLength(minLength, longArrayValue, this.exceptionSupplier)));
    var floatArrayValue = new float[] {
      7.5f
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinLength(minLength, floatArrayValue, this.exceptionSupplier)));
    var doubleArrayValue = new double[] {
      8.5d
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinLength(minLength, doubleArrayValue, this.exceptionSupplier)));
    var objectArrayValue = new Object[] {
      "J"
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinLength(minLength, objectArrayValue, this.exceptionSupplier)));
  }

  @Test
  public void optionalMinLength_returnValue_whenCollectionValueIsNull() {
    var minLength = 2;
    var value = (List<String>) null;
    var returnedValue = Validate.optionalMinLength(minLength, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMinLength_returnValue_whenCollectionValueHasExactMinLengthSize() {
    var minLength = 2;
    var value = List.of("A", "B");
    var returnedValue = Validate.optionalMinLength(minLength, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void optionalMinLength_returnValue_whenMapValueIsLargerThanMinLength() {
    var minLength = 2;
    var value = List.of("A", "B", "C");
    var returnedValue = Validate.optionalMinLength(minLength, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void optionalMinLength_throwsException_whenCollectionValueIsSmallerThanMinLength() {
    var minLength = 2;
    var value = List.of("A");
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMinLength(minLength, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMaxLength_returnValue_whenStringValueIsNull() {
    var maxLength = 2;
    var value = (String) null;
    var returnedValue = Validate.optionalMaxLength(maxLength, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMaxLength_returnValue_whenStringValueHasExactMaxLengthSize() {
    var maxLength = 2;
    var value = "AB";
    var returnedValue = Validate.optionalMaxLength(maxLength, value, this.exceptionSupplier);
    assertSame("AB", returnedValue);
  }

  @Test
  public void optionalMaxLength_returnValue_whenStringValueIsSmallerThanMaxLength() {
    var maxLength = 2;
    var value = "A";
    var returnedValue = Validate.optionalMaxLength(maxLength, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void optionalMaxLength_throwsException_whenStringValueIsLargerThanMaxLength() {
    var maxLength = 2;
    var value = "ABC";
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxLength(maxLength, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalMaxLength_returnValue_whenArrayValueIsNull() {
    var minLength = 2;
    var nullBooleanArrayValue = (boolean[]) null;
    var returnedBooleanArrayValue = Validate.optionalMaxLength(minLength, nullBooleanArrayValue, this.exceptionSupplier);
    assertNull(returnedBooleanArrayValue);
    var nullByteArrayValue = (byte[]) null;
    var returnedByteArrayValue = Validate.optionalMaxLength(minLength, nullByteArrayValue, this.exceptionSupplier);
    assertNull(returnedByteArrayValue);
    var nullShortArrayValue = (short[]) null;
    var returnedShortArrayValue = Validate.optionalMaxLength(minLength, nullShortArrayValue, this.exceptionSupplier);
    assertNull(returnedShortArrayValue);
    var nullCharArrayValue = (char[]) null;
    var returnedCharArrayValue = Validate.optionalMaxLength(minLength, nullCharArrayValue, this.exceptionSupplier);
    assertNull(returnedCharArrayValue);
    var nullIntArrayValue = (int[]) null;
    var returnedIntArrayValue = Validate.optionalMaxLength(minLength, nullIntArrayValue, this.exceptionSupplier);
    assertNull(returnedIntArrayValue);
    var nullLongArrayValue = (long[]) null;
    var returnedLongArrayValue = Validate.optionalMaxLength(minLength, nullLongArrayValue, this.exceptionSupplier);
    assertNull(returnedLongArrayValue);
    var nullFloatArrayValue = (float[]) null;
    var returnedFloatArrayValue = Validate.optionalMaxLength(minLength, nullFloatArrayValue, this.exceptionSupplier);
    assertNull(returnedFloatArrayValue);
    var nullDoubleArrayValue = (double[]) null;
    var returnedDoubleArrayValue = Validate.optionalMaxLength(minLength, nullDoubleArrayValue, this.exceptionSupplier);
    assertNull(returnedDoubleArrayValue);
    var nullObjectArrayValue = (Object[]) null;
    var returnedObjectArrayValue = Validate.optionalMaxLength(minLength, nullObjectArrayValue, this.exceptionSupplier);
    assertNull(returnedObjectArrayValue);
  }

  @Test
  public void optionalMaxLength_returnValue_whenArrayValueHasExactMaxLengthSize() {
    var maxLength = 2;
    var booleanArrayValue = new boolean[] {
      true, false
    };
    var returnedBooleanArrayValue = Validate.optionalMaxLength(maxLength, booleanArrayValue, this.exceptionSupplier);
    assertSame(booleanArrayValue, returnedBooleanArrayValue);
    var byteArrayValue = new byte[] {
      2, 3
    };
    var returnedByteArrayValue = Validate.optionalMaxLength(maxLength, byteArrayValue, this.exceptionSupplier);
    assertSame(byteArrayValue, returnedByteArrayValue);
    var shortArrayValue = new short[] {
      3, 4
    };
    var returnedShortArrayValue = Validate.optionalMaxLength(maxLength, shortArrayValue, this.exceptionSupplier);
    assertSame(shortArrayValue, returnedShortArrayValue);
    var charArrayValue = new char[] {
      'D', 'E'
    };
    var returnedCharArrayValue = Validate.optionalMaxLength(maxLength, charArrayValue, this.exceptionSupplier);
    assertSame(charArrayValue, returnedCharArrayValue);
    var intArrayValue = new int[] {
      5, 6
    };
    var returnedIntArrayValue = Validate.optionalMaxLength(maxLength, intArrayValue, this.exceptionSupplier);
    assertSame(intArrayValue, returnedIntArrayValue);
    var longArrayValue = new long[] {
      6L, 7L
    };
    var returnedLongArrayValue = Validate.optionalMaxLength(maxLength, longArrayValue, this.exceptionSupplier);
    assertSame(longArrayValue, returnedLongArrayValue);
    var floatArrayValue = new float[] {
      7.5f, 8.5f
    };
    var returnedFloatArrayValue = Validate.optionalMaxLength(maxLength, floatArrayValue, this.exceptionSupplier);
    assertSame(floatArrayValue, returnedFloatArrayValue);
    var doubleArrayValue = new double[] {
      8.5d, 9.5d
    };
    var returnedDoubleArrayValue = Validate.optionalMaxLength(maxLength, doubleArrayValue, this.exceptionSupplier);
    assertSame(doubleArrayValue, returnedDoubleArrayValue);
    var objectArrayValue = new Object[] {
      "J", "K"
    };
    var returnedObjectArrayValue = Validate.optionalMaxLength(maxLength, objectArrayValue, this.exceptionSupplier);
    assertSame(objectArrayValue, returnedObjectArrayValue);
  }

  @Test
  public void optionalMaxLength_returnValue_whenArrayValueIsSmallerThanMaxLength() {
    var maxLength = 2;
    var booleanArrayValue = new boolean[] {
      true
    };
    var returnedBooleanArrayValue = Validate.optionalMaxLength(maxLength, booleanArrayValue, this.exceptionSupplier);
    assertSame(booleanArrayValue, returnedBooleanArrayValue);
    var byteArrayValue = new byte[] {
      2
    };
    var returnedByteArrayValue = Validate.optionalMaxLength(maxLength, byteArrayValue, this.exceptionSupplier);
    assertSame(byteArrayValue, returnedByteArrayValue);
    var shortArrayValue = new short[] {
      3
    };
    var returnedShortArrayValue = Validate.optionalMaxLength(maxLength, shortArrayValue, this.exceptionSupplier);
    assertSame(shortArrayValue, returnedShortArrayValue);
    var charArrayValue = new char[] {
      'D'
    };
    var returnedCharArrayValue = Validate.optionalMaxLength(maxLength, charArrayValue, this.exceptionSupplier);
    assertSame(charArrayValue, returnedCharArrayValue);
    var intArrayValue = new int[] {
      5
    };
    var returnedIntArrayValue = Validate.optionalMaxLength(maxLength, intArrayValue, this.exceptionSupplier);
    assertSame(intArrayValue, returnedIntArrayValue);
    var longArrayValue = new long[] {
      6L
    };
    var returnedLongArrayValue = Validate.optionalMaxLength(maxLength, longArrayValue, this.exceptionSupplier);
    assertSame(longArrayValue, returnedLongArrayValue);
    var floatArrayValue = new float[] {
      7.5f
    };
    var returnedFloatArrayValue = Validate.optionalMaxLength(maxLength, floatArrayValue, this.exceptionSupplier);
    assertSame(floatArrayValue, returnedFloatArrayValue);
    var doubleArrayValue = new double[] {
      8.5d
    };
    var returnedDoubleArrayValue = Validate.optionalMaxLength(maxLength, doubleArrayValue, this.exceptionSupplier);
    assertSame(doubleArrayValue, returnedDoubleArrayValue);
    var objectArrayValue = new Object[] {
      "J"
    };
    var returnedObjectArrayValue = Validate.optionalMaxLength(maxLength, objectArrayValue, this.exceptionSupplier);
    assertSame(objectArrayValue, returnedObjectArrayValue);
  }

  @Test
  public void optionalMaxLength_throwsException_whenArrayValueIsLargerThanMaxLength() {
    var maxLength = 2;
    var booleanArrayValue = new boolean[] {
      true, false, true
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxLength(maxLength, booleanArrayValue, this.exceptionSupplier)));
    var byteArrayValue = new byte[] {
      2, 3, 4
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxLength(maxLength, byteArrayValue, this.exceptionSupplier)));
    var shortArrayValue = new short[] {
      3, 4, 5
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxLength(maxLength, shortArrayValue, this.exceptionSupplier)));
    var charArrayValue = new char[] {
      'D', 'E', 'F'
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxLength(maxLength, charArrayValue, this.exceptionSupplier)));
    var intArrayValue = new int[] {
      5, 6, 7
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxLength(maxLength, intArrayValue, this.exceptionSupplier)));
    var longArrayValue = new long[] {
      6L, 7L, 8L
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxLength(maxLength, longArrayValue, this.exceptionSupplier)));
    var floatArrayValue = new float[] {
      7.5f, 8.5f, 9.5f
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxLength(maxLength, floatArrayValue, this.exceptionSupplier)));
    var doubleArrayValue = new double[] {
      8.5d, 9.5d, 10.5d
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxLength(maxLength, doubleArrayValue, this.exceptionSupplier)));
    var objectArrayValue = new Object[] {
      "J", "K", "L"
    };
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxLength(maxLength, objectArrayValue, this.exceptionSupplier)));
  }

  @Test
  public void optionalMaxLength_returnValue_whenCollectionValueHasExactMaxLengthSize() {
    var maxLength = 2;
    var value = List.of("A", "B");
    var returnedValue = Validate.optionalMaxLength(maxLength, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void optionalMaxLength_returnValue_whenCollectionValueIsSmallerThanMaxLength() {
    var maxLength = 2;
    var value = List.of("A");
    var returnedValue = Validate.optionalMaxLength(maxLength, value, this.exceptionSupplier);
    assertEquals(value, returnedValue);
  }

  @Test
  public void optionalMaxLength_returnValue_whenCollectionValueIsNull() {
    var maxLength = 2;
    var value = (List<String>) null;
    var returnedValue = Validate.optionalMaxLength(maxLength, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalMaxLength_throwsException_whenCollectionValueIsLargerThanMaxLength() {
    var maxLength = 2;
    var value = List.of("A", "B", "C");
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalMaxLength(maxLength, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalRegex_returnValue_whenValueIsNull() {
    var stringContentPattern = "\\d{3}";
    var contentPattern = Pattern.compile(stringContentPattern);
    var value = (String) null;
    var returnedValue = Validate.optionalRegex(stringContentPattern, value, this.exceptionSupplier);
    assertNull(returnedValue);
    returnedValue = Validate.optionalRegex(contentPattern, value, this.exceptionSupplier);
    assertNull(returnedValue);
  }

  @Test
  public void optionalRegex_returnValue_whenValueMatchesContentPattern() {
    var stringContentPattern = "\\d{3}";
    var contentPattern = Pattern.compile(stringContentPattern);
    var value = "123";
    var returnedValue = Validate.optionalRegex(stringContentPattern, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
    returnedValue = Validate.optionalRegex(contentPattern, value, this.exceptionSupplier);
    assertSame(value, returnedValue);
  }

  @Test
  public void optionalRegex_throwsException_whenValueNotMatchesContentPattern() {
    var stringContentPattern = "\\d{3}";
    var contentPattern = Pattern.compile(stringContentPattern);
    var value = "ABC";
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalRegex(stringContentPattern, value, this.exceptionSupplier)));
    assertSame(this.exception, assertThrows(this.exception.getClass(), ()->Validate.optionalRegex(contentPattern, value, this.exceptionSupplier)));
  }

  @Test
  public void optionalRegex_throwsNullPointerException_whenPatternIsNull() {
    var stringContentPattern = (String) null;
    var contentPattern = (Pattern) null;
    var value = "ABC";
    assertThrows(NullPointerException.class, ()->Validate.optionalRegex(stringContentPattern, value, this.exceptionSupplier));
    assertThrows(NullPointerException.class, ()->Validate.optionalRegex(contentPattern, value, this.exceptionSupplier));
  }

}
