// This file is part of lightsvc Java Microservices Components (lightsvc).
//
// lightsvc Java Microservices Components (lightsvc) is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lightsvc Java Microservices Components (lightsvc) is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with lightsvc Java Microservices Components (lightsvc). If not, see <http://www.gnu.org/licenses/>.
package lightsvc.err;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import lightsvc.util.Arrays;
import lightsvc.util.Escape;
import lightsvc.util.Strings;

/**
 * <p>
 * A Utility class that aggregates a series of utilities to handle exceptions.
 * </p>
 * <p>
 * These utilities are meant to replace the Java standard error handling {@code try...catch} statement in cases of simple {@link Exception} translations.
 * There are methods to avoid unnecessary exceptions handling where is assured that exceptions do not occur inside a block of code.
 * </p>
 * 
 * @author Tiago van den Berg
 */
// SonarLint: Classes should not be coupled to too many other classes. (This is an utility class (full of static methods) that uses many other utility classes).
@SuppressWarnings("java:S1200")
public final class Exceptions {

  private Exceptions() {
    throw new UnsupportedOperationException("lightsvc.err.Exceptions.Exceptions cannot be instantiated.");
  }

  /**
   * Traverse deep in throwable stack causes until find the last (root) cause of an exception and return it.
   * 
   * @param throwable
   *   The exception that will be traversed.
   * 
   * @return the root cause of an exception stack.
   */
  public static Throwable rootCause(final Throwable throwable) {
    if (throwable == null) {
      return null;
    }
    var nextThrowable = throwable;
    while (nextThrowable.getCause() != null) {
      nextThrowable = nextThrowable.getCause();
    }
    return nextThrowable;
  }

  /**
   * Return a one-line {@link String} representation of an exception filtered stack trace of the root cause and the most recent thrown exception. The root
   * cause is showed with all (filtered) stack trace lines, the subsequent exceptions show only the line where the exception is thrown. Optional package filters
   * can be applied, so only the relevant stack trace parts are shown.
   * 
   * @param exception
   *   The exception to generate
   * @param filterPackages
   *   (Optional) packages to filter (inclusive) to show only relevant stack trace parts.
   * 
   * @return a one-line {@link String} representation of an exception filtered stack trace from the root cause to the most recent thrown exception.
   */
  public static String shortStackTraceString(final Throwable exception, final String... filterPackages) {
    Objects.requireNonNull(exception, "exception must not be null.");
    var rootCause = Exceptions.rootCause(exception);
    var rootStackTrace = rootCause.getStackTrace();
    var sb = new StringBuilder(nameAndMessage(rootCause));
    IntStream.rangeClosed(1, rootStackTrace.length)
      .mapToObj(i->rootStackTrace[rootStackTrace.length - i])
      .filter(se->Arrays.isNullOrEmpty(filterPackages) || java.util.Arrays.stream(filterPackages).anyMatch(p->se.getClassName().startsWith(p + ".")))
      .forEach(se->sb.append(" (" + se.getFileName() + ":" + se.getLineNumber() + ")"));
    if (rootCause == exception) {
      return sb.toString();
    }
    sb.append(" >> ");
    sb.append(nameAndMessage(exception));
    return sb.toString();
  }

  private static String nameAndMessage(Throwable exception) {
    return exception.getClass().getSimpleName() +
      (Strings.isNullOrEmpty(exception.getMessage()) ? "" : (": \"" + Escape.javaScript(exception.getMessage()) + "\""));
  }

  /**
   * <p>
   * Returns an predicate that returns {@code true} for any exception tested.
   * </p>
   * <p>
   * This method is meant to be used in conjunction with {@link Exceptions#translate} set of methods and {@link Exceptions.Translation#when(Predicate)}.
   * </p>
   * 
   * @return an {@link Predicate} that returns {@code true} for any {@link Exception} tested.
   */
  public static Predicate<Exception> any() {
    return e->true;
  }

  /**
   * <p>
   * Returns an predicate that returns {@code true} when the exception tested has a specified message.
   * </p>
   * <p>
   * This method is meant to be used in conjunction with {@link Exceptions#translate} set of methods and {@link Exceptions.Translation#when(Predicate)}.
   * </p>
   * 
   * @param msg
   *   the exception message that will be evaluated in the returned {@link Predicate}.
   * 
   * @return an {@link Predicate} that returns {@code true} when the {@link Exception} evaluated has the same message as parameter {@code msg}, otherwise
   *   returns {@code false}.
   */
  public static Predicate<Exception> hasMsg(final String msg) {
    return e->(msg == null && e.getMessage() == null) || ((msg != null) && msg.equals(e.getMessage()));
  }

  /**
   * <p>
   * Returns an predicate that returns {@code true} when the exception tested has a specified type (including inherited types).
   * </p>
   * <p>
   * This method is meant to be used in conjunction with {@link Exceptions#translate} set of methods and {@link Exceptions.Translation#when(Predicate)}.
   * </p>
   * 
   * @param <ExceptionType>
   *   The type of parameter {@code type} (automatically determined).
   * @param type
   *   the exception class type that will be evaluated in the returned {@link Predicate}.
   * 
   * @return an {@link Predicate} that returns {@code true} when the {@link Exception} evaluated has the same class type (including inherited types) as
   *   parameter {@code type}, otherwise returns {@code false}.
   * 
   * @throws NullPointerException
   *   if {@code type} is {@code null}.
   */
  public static <ExceptionType extends Exception> Predicate<Exception> hasType(final Class<ExceptionType> type) throws NullPointerException {
    Objects.requireNonNull(type, "type must not be null.");
    return e->type.isAssignableFrom(e.getClass());
  }

  /**
   * <p>
   * Returns an predicate that returns {@code true} when the exception tested has a specified type (not including inherited types).
   * </p>
   * <p>
   * This method is meant to be used in conjunction with {@link Exceptions#translate} set of methods and {@link Exceptions.Translation#when(Predicate)}.
   * </p>
   * 
   * @param <ExceptionType>
   *   The type of parameter {@code type} (automatically determined).
   * @param type
   *   the exception class type that will be evaluated in the returned {@link Predicate}.
   * 
   * @return an {@link Predicate} that returns {@code true} when the {@link Exception} evaluated has the same class type (not including inherited types) as
   *   parameter {@code type}, otherwise returns {@code false}.
   * 
   * @throws NullPointerException
   *   if {@code type} is {@code null}.
   */
  public static <ExceptionType extends Exception> Predicate<Exception> hasExactType(final Class<ExceptionType> type) throws NullPointerException {
    Objects.requireNonNull(type, "type must not be null.");
    return e->type == e.getClass();
  }

  /**
   * <p>
   * Returns an predicate that tests against all exception cause chain.
   * </p>
   * <p>
   * Prefer using the more semantical friendly {@link Exceptions.Translation.Builder#inCauseChain()} method.
   * </p>
   * 
   * @param exceptionPredicate
   *   the exception {@link Predicate} type that will be evaluated in all cause chain in the returned {@link Predicate}.
   * 
   * @return an {@link Predicate} that returns {@code true} when the {@link Exception} evaluated has the same class type (not including inherited types) as
   *   parameter {@code type}, otherwise returns {@code false}.
   * 
   * @throws NullPointerException
   *   if {@code type} is {@code null}.
   */
  // SonarLint S1192: String literals should not be duplicated. (For local contexts they should).
  // SonarLint S6201: Pattern Matching instanceof should be used. (Codecov does not understand pattern matching instanceof)
  @SuppressWarnings({
    "java:S1192", "java:S6201"
  })
  public static Predicate<Exception> inCauseChain(final Predicate<Exception> exceptionPredicate) throws NullPointerException {
    Objects.requireNonNull(exceptionPredicate, "exceptionPredicate must not be null.");
    return (Exception e)-> {
      for (var eLoop = (Throwable) e; eLoop instanceof Exception; eLoop = eLoop.getCause()) {
        var eLoopTyped = (Exception) eLoop;
        if (exceptionPredicate.test(eLoopTyped)) {
          return true;
        }
      }
      return false;
    };
  }

  /**
   * <p>
   * This method could be used when a piece of code declares a checked {@link Exception}, but never throws any, so it is required to handle it. This offer a
   * shorter implementation without the need to use a @{code try...catch} statement.
   * </p>
   * <p>
   * Wrap an exception inside a {@link UncaughtException} and return it or return the same exception if it is already of {@link UncaughtException} type.
   * </p>
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import java.io.IOException;
   *   import java.io.PrintWriter;
   *   import java.io.StringWriter;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       String result;
   *       try(var sw = new StringWriter(); var out = new PrintWriter(sw)) {
   *         out.println("something");
   *         result = out.toString();
   *       } catch(IOException e) {
   *         throw Exceptions.<strong>shouldNotHappen</strong>(e);
   *       }
   *     }
   *   }
   *   </code>
   *   </pre>
   * 
   * @param exception
   *   a required, but never thrown, checked exception.
   * 
   * @return {@code null} if {@code exception} is {@code null}, the same {@code exception} is it is an {@link UncaughtException}, or a new
   *   {@link UncaughtException} wrapping {@code exception} if it is any different exception type.
   */
  @SuppressWarnings("java:S6201") // SonarLint: Pattern Matching instanceof should be used. (Codecov does not understand pattern matching instanceof)
  public static UncaughtException shouldNotHappen(final Exception exception) {
    if (exception == null) {
      return null;
    }
    if (exception instanceof UncaughtException) { // new instanceOf auto cast bug does not work with code coverage.
      return (UncaughtException) exception;
    }
    return new UncaughtException(exception);
  }

  /**
   * <p>
   * This method could be used when a piece of code declares a checked {@link Exception}, but never throws any, so it is required to handle it. This offer a
   * shorter implementation without the need to use a @{code try...catch} statement.
   * </p>
   * <p>
   * Executes the code inside {@code supplier} and if an exception is thrown, wraps it inside a {@link UncaughtException} and throw it or throw the same
   * exception if it is already of {@link UncaughtException} type.
   * </p>
   * 
   * @param supplier
   *   lambda code that will be executed.
   * 
   * @return value that {@code supplier} returns.
   * 
   * @throws NullPointerException
   *   if supplier is {@code null}.
   * @throws UncaughtException
   *   if {@code supplier} throws any exception (That is considered a development error).
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       Exceptions.<strong>shouldNotHappen</strong>(()->methodThatDeclareCheckedExceptionButNeverThrowsIt());
   *     }
   *     
   *     public static <strong>boolean</strong> methodThatDeclareCheckedExceptionButNeverThrowsIt() throws IOException {
   *       return true;
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  @SuppressWarnings("java:S1192") // SonarLint: String literals should not be duplicated. (For local contexts they should).
  public static boolean shouldNotHappen(final BooleanSupplier<Exception> supplier) throws NullPointerException, UncaughtException {
    Objects.requireNonNull(supplier, "supplier must not be null.");
    try {
      return supplier.get();
    } catch (Exception e) {
      throw Exceptions.shouldNotHappen(e);
    }
  }

  /**
   * <p>
   * This method could be used when a piece of code declares a checked {@link Exception}, but never throws any, so it is required to handle it. This offer a
   * shorter implementation without the need to use a @{code try...catch} statement.
   * </p>
   * <p>
   * Executes the code inside {@code supplier} and if an exception is thrown, wraps it inside a {@link UncaughtException} and throw it or throw the same
   * exception if it is already of {@link UncaughtException} type.
   * </p>
   * 
   * @param supplier
   *   lambda code that will be executed.
   * 
   * @return value that {@code supplier} returns.
   * 
   * @throws NullPointerException
   *   if supplier is {@code null}.
   * @throws UncaughtException
   *   if {@code supplier} throws any exception (That is considered a development error).
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       Exceptions.<strong>shouldNotHappen</strong>(()->methodThatDeclareCheckedExceptionButNeverThrowsIt());
   *     }
   *     
   *     public static <strong>byte</strong> methodThatDeclareCheckedExceptionButNeverThrowsIt() throws IOException {
   *       return (byte) 0;
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  public static byte shouldNotHappen(final ByteSupplier<Exception> supplier) throws NullPointerException, UncaughtException {
    Objects.requireNonNull(supplier, "supplier must not be null.");
    try {
      return supplier.get();
    } catch (Exception e) {
      throw Exceptions.shouldNotHappen(e);
    }
  }

  /**
   * <p>
   * This method could be used when a piece of code declares a checked {@link Exception}, but never throws any, so it is required to handle it. This offer a
   * shorter implementation without the need to use a @{code try...catch} statement.
   * </p>
   * <p>
   * Executes the code inside {@code supplier} and if an exception is thrown, wraps it inside a {@link UncaughtException} and throw it or throw the same
   * exception if it is already of {@link UncaughtException} type.
   * </p>
   * 
   * @param supplier
   *   lambda code that will be executed.
   * 
   * @return value that {@code supplier} returns.
   * 
   * @throws NullPointerException
   *   if supplier is {@code null}.
   * @throws UncaughtException
   *   if {@code supplier} throws any exception (That is considered a development error).
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       Exceptions.<strong>shouldNotHappen</strong>(()->methodThatDeclareCheckedExceptionButNeverThrowsIt());
   *     }
   *     
   *     public static short methodThatDeclareCheckedExceptionButNeverThrowsIt() throws IOException {
   *       return (short) 0;
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  public static short shouldNotHappen(final ShortSupplier<Exception> supplier) throws NullPointerException, UncaughtException {
    Objects.requireNonNull(supplier, "supplier must not be null.");
    try {
      return supplier.get();
    } catch (Exception e) {
      throw Exceptions.shouldNotHappen(e);
    }
  }

  /**
   * <p>
   * This method could be used when a piece of code declares a checked {@link Exception}, but never throws any, so it is required to handle it. This offer a
   * shorter implementation without the need to use a @{code try...catch} statement.
   * </p>
   * <p>
   * Executes the code inside {@code supplier} and if an exception is thrown, wraps it inside a {@link UncaughtException} and throw it or throw the same
   * exception if it is already of {@link UncaughtException} type.
   * </p>
   * 
   * @param supplier
   *   lambda code that will be executed.
   * 
   * @return value that {@code supplier} returns.
   * 
   * @throws NullPointerException
   *   if supplier is {@code null}.
   * @throws UncaughtException
   *   if {@code supplier} throws any exception (That is considered a development error).
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       Exceptions.<strong>shouldNotHappen</strong>(()->methodThatDeclareCheckedExceptionButNeverThrowsIt());
   *     }
   *     
   *     public static <strong>char</strong> methodThatDeclareCheckedExceptionButNeverThrowsIt() throws IOException {
   *       return '\0';
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  public static char shouldNotHappen(final CharSupplier<Exception> supplier) throws NullPointerException, UncaughtException {
    Objects.requireNonNull(supplier, "supplier must not be null.");
    try {
      return supplier.get();
    } catch (Exception e) {
      throw Exceptions.shouldNotHappen(e);
    }
  }

  /**
   * <p>
   * This method could be used when a piece of code declares a checked {@link Exception}, but never throws any, so it is required to handle it. This offer a
   * shorter implementation without the need to use a @{code try...catch} statement.
   * </p>
   * <p>
   * Executes the code inside {@code supplier} and if an exception is thrown, wraps it inside a {@link UncaughtException} and throw it or throw the same
   * exception if it is already of {@link UncaughtException} type.
   * </p>
   * 
   * @param supplier
   *   lambda code that will be executed.
   * 
   * @return value that {@code supplier} returns.
   * 
   * @throws NullPointerException
   *   if supplier is {@code null}.
   * @throws UncaughtException
   *   if {@code supplier} throws any exception (That is considered a development error).
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       Exceptions.<strong>shouldNotHappen</strong>(()->methodThatDeclareCheckedExceptionButNeverThrowsIt());
   *     }
   *     
   *     public static <strong>int</strong> methodThatDeclareCheckedExceptionButNeverThrowsIt() throws IOException {
   *       return 0;
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  public static int shouldNotHappen(final IntSupplier<Exception> supplier) throws NullPointerException, UncaughtException {
    Objects.requireNonNull(supplier, "supplier must not be null.");
    try {
      return supplier.get();
    } catch (Exception e) {
      throw Exceptions.shouldNotHappen(e);
    }
  }

  /**
   * <p>
   * This method could be used when a piece of code declares a checked {@link Exception}, but never throws any, so it is required to handle it. This offer a
   * shorter implementation without the need to use a @{code try...catch} statement.
   * </p>
   * <p>
   * Executes the code inside {@code supplier} and if an exception is thrown, wraps it inside a {@link UncaughtException} and throw it or throw the same
   * exception if it is already of {@link UncaughtException} type.
   * </p>
   * 
   * @param supplier
   *   lambda code that will be executed.
   * 
   * @return value that {@code supplier} returns.
   * 
   * @throws NullPointerException
   *   if supplier is {@code null}.
   * @throws UncaughtException
   *   if {@code supplier} throws any exception (That is considered a development error).
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       Exceptions.<strong>shouldNotHappen</strong>(()->methodThatDeclareCheckedExceptionButNeverThrowsIt());
   *     }
   *     
   *     public static <strong>long</strong> methodThatDeclareCheckedExceptionButNeverThrowsIt() throws IOException {
   *       return 0l;
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  public static long shouldNotHappen(final LongSupplier<Exception> supplier) throws NullPointerException, UncaughtException {
    Objects.requireNonNull(supplier, "supplier must not be null.");
    try {
      return supplier.get();
    } catch (Exception e) {
      throw Exceptions.shouldNotHappen(e);
    }
  }

  /**
   * <p>
   * This method could be used when a piece of code declares a checked {@link Exception}, but never throws any, so it is required to handle it. This offer a
   * shorter implementation without the need to use a @{code try...catch} statement.
   * </p>
   * <p>
   * Executes the code inside {@code supplier} and if an exception is thrown, wraps it inside a {@link UncaughtException} and throw it or throw the same
   * exception if it is already of {@link UncaughtException} type.
   * </p>
   * 
   * @param supplier
   *   lambda code that will be executed.
   * 
   * @return value that {@code supplier} returns.
   * 
   * @throws NullPointerException
   *   if supplier is {@code null}.
   * @throws UncaughtException
   *   if {@code supplier} throws any exception (That is considered a development error).
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       Exceptions.<strong>shouldNotHappen</strong>(()->methodThatDeclareCheckedExceptionButNeverThrowsIt());
   *     }
   *     
   *     public static <strong>float</strong> methodThatDeclareCheckedExceptionButNeverThrowsIt() throws IOException {
   *       return 0f;
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  public static float shouldNotHappen(final FloatSupplier<Exception> supplier) throws NullPointerException, UncaughtException {
    Objects.requireNonNull(supplier, "supplier must not be null.");
    try {
      return supplier.get();
    } catch (Exception e) {
      throw Exceptions.shouldNotHappen(e);
    }
  }

  /**
   * <p>
   * This method could be used when a piece of code declares a checked {@link Exception}, but never throws any, so it is required to handle it. This offer a
   * shorter implementation without the need to use a @{code try...catch} statement.
   * </p>
   * <p>
   * Executes the code inside {@code supplier} and if an exception is thrown, wraps it inside a {@link UncaughtException} and throw it or throw the same
   * exception if it is already of {@link UncaughtException} type.
   * </p>
   * 
   * @param supplier
   *   lambda code that will be executed.
   * 
   * @return value that {@code supplier} returns.
   * 
   * @throws NullPointerException
   *   if supplier is {@code null}.
   * @throws UncaughtException
   *   if {@code supplier} throws any exception (That is considered a development error).
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       Exceptions.<strong>shouldNotHappen</strong>(()->methodThatDeclareCheckedExceptionButNeverThrowsIt());
   *     }
   *     
   *     public static <strong>double</strong> methodThatDeclareCheckedExceptionButNeverThrowsIt() throws IOException {
   *       return 0d;
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  public static double shouldNotHappen(final DoubleSupplier<Exception> supplier) throws NullPointerException, UncaughtException {
    Objects.requireNonNull(supplier, "supplier must not be null.");
    try {
      return supplier.get();
    } catch (Exception e) {
      throw Exceptions.shouldNotHappen(e);
    }
  }

  /**
   * <p>
   * This method could be used when a piece of code declares a checked {@link Exception}, but never throws any, so it is required to handle it. This offer a
   * shorter implementation without the need to use a @{code try...catch} statement.
   * </p>
   * <p>
   * Executes the code inside {@code runnable} and if an exception is thrown, wraps it inside a {@link UncaughtException} and throw it or throw the same
   * exception if it is already of {@link UncaughtException} type.
   * </p>
   * 
   * @param runnable
   *   lambda code that will be executed.
   * 
   * @throws NullPointerException
   *   if supplier is {@code null}.
   * @throws UncaughtException
   *   if {@code supplier} throws any exception (That is considered a development error).
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       Exceptions.<strong>shouldNotHappen</strong>(()->methodThatDeclareCheckedExceptionButNeverThrowsIt());
   *     }
   *     
   *     public static <strong>void</strong> methodThatDeclareCheckedExceptionButNeverThrowsIt() throws IOException {
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  public static void shouldNotHappen(final Runnable<Exception> runnable) throws NullPointerException, UncaughtException {
    Objects.requireNonNull(runnable, "runnable must not be null.");
    try {
      runnable.run();
    } catch (Exception e) {
      throw Exceptions.shouldNotHappen(e);
    }
  }

  /**
   * <p>
   * This method could be used when a piece of code declares a checked {@link Exception}, but never throws any, so it is required to handle it. This offer a
   * shorter implementation without the need to use a @{code try...catch} statement.
   * </p>
   * <p>
   * Executes the code inside {@code supplier} and if an exception is thrown, wraps it inside a {@link UncaughtException} and throw it or throw the same
   * exception if it is already of {@link UncaughtException} type.
   * </p>
   * 
   * @param <SupplierReturnType>
   *   The return type of {@code supplier} (automatically determined).
   * @param supplier
   *   lambda code that will be executed.
   * 
   * @return the value that {@code supplier} returns or {@code null} if {@code supplier} is {@code null}.
   * 
   * @throws UncaughtException
   *   if {@code supplier} throws any exception (That is considered a development error).
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       Exceptions.<strong>shouldNotHappen</strong>(()->methodThatDeclareCheckedExceptionButNeverThrowsIt());
   *     }
   *     
   *     // <strong>Any Object Type</strong> instead of String return type still applies for <strong>Supplier</strong>.
   *     public static <strong>String</strong> methodThatDeclareCheckedExceptionButNeverThrowsIt() throws IOException {
   *       return "";
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  public static <SupplierReturnType> SupplierReturnType shouldNotHappen(final Supplier<SupplierReturnType, Exception> supplier) throws UncaughtException {
    if (supplier == null) {
      return null;
    }
    try {
      return supplier.get();
    } catch (Exception e) {
      throw Exceptions.shouldNotHappen(e);
    }
  }

  /**
   * <p>
   * Tests if an {@link Exception} matches one of a set of {@link Translation} rules and if it matches, translate it to another {@link RuntimeException}
   * according to that rule.
   * </p>
   * <p>
   * This method is mostly used internally by the other forms of {@link #translate(Supplier, Translation...)} method, but it has its uses when the case needs to
   * use the {@code try... catch} statement.
   * </p>
   * 
   * @param <TranslatedException>
   *   The type of exception that will be returned by a matched translation. (Automatically determined.)
   * @param exception
   *   that will be evaluated by the translation rules.
   * @param translations
   *   is a set of rules that translate exceptions. The {@link Translation} predicate is tested and triggered in the order of this parameter.
   * 
   * @return the translated exception if {@code exception} matches one of a set of {@link Translation} rules or {@code null} if {@code exception} is
   *   {@code null}.
   * 
   * @throws UncaughtException
   *   wrapping {@code exception} if no translation rules matches it.
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import static lightsvc.err.Exceptions.Translation.when;
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       try {
   *         myMethod();
   *       } catch(Exception e) {
   *         <strong>throw</strong> Exceptions.<strong>translate</strong>(
   *           e,
   *           when(Exceptions.hasType(IOException.class)).to(cause->new MyMethodException("Something gone wrong!", cause))
   *         );
   *       }
   *     }
   *     
   *     public static class MyMethodException extends RuntimeException {
   *       public MyMethodException(String message, Throwable cause) {
   *         super(message, cause);
   *       }
   *     }
   *     
   *     public static String myMethod() throws IOException {
   *       throw new IOException();
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  @SafeVarargs
  public static <TranslatedException extends Exception> TranslatedException translate(
    final Exception exception,
    final Translation<TranslatedException>... translations
  ) throws UncaughtException {
    if (exception == null) {
      return null;
    }
    if (Arrays.isNullOrEmpty(translations)) {
      throw Exceptions.shouldNotHappen(exception);
    }
    for (var exceptionTranslation : translations) {
      Objects.requireNonNull(exceptionTranslation, "translations items must not be null.");
      if (exceptionTranslation.test(exception)) {
        return exceptionTranslation.toFn.apply(exception);
      }
    }
    throw Exceptions.shouldNotHappen(exception);
  }

  /**
   * If {@code supplier} throws an exception, tests if it matches one of a set of {@link Translation} rules and if it matches, translate it to another
   * exception according to that rule.
   * 
   * @param <TranslatedException>
   *   The type of exception that could be thrown by a matched translation. (Automatically determined.)
   * @param supplier
   *   the lambda code that possible could throw an exception.
   * @param translations
   *   is a set of rules that translate exceptions. The {@link Translation} predicate is tested and triggered in the order of this parameter.
   * 
   * @return boolean value that {@code supplier} returns if it not throws any exception, or {@code false} if the translated exception is {@code null}.
   * 
   * @throws TranslatedException
   *   the translated exception if the {@code supplier} throws an {@link Exception} that matches one of a set of {@link Translation} rules.
   * @throws NullPointerException
   *   if {@code supplier} is {@code null}
   * @throws UncaughtException
   *   wrapping the {@link Exception} thrown by {@code supplier} if no translation rules matches it.
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import static lightsvc.err.Exceptions.Translation.when;
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       var value = Exceptions.<strong>translateBoolean</strong>(
   *         ()->myMethod(),
   *         when(Exceptions.hasType(IOException.class)).to(cause->new MyMethodException("Something gone wrong!", cause))
   *       );
   *     }
   *     
   *     public static class MyMethodException extends RuntimeException {
   *       public MyMethodException(String message, Throwable cause) {
   *         super(message, cause);
   *       }
   *     }
   *     
   *     public static <strong>boolean</strong> myMethod() throws IOException {
   *       throw new IOException();
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  @SafeVarargs
  // SonarLint: Checked exceptions should not be thrown. (This method does not limit the user choice. Is due to the user the type of exception it can use.)
  @SuppressWarnings("java:S1162")
  public static <TranslatedException extends Exception> boolean translateBoolean(
    final BooleanSupplier<Exception> supplier,
    final Translation<TranslatedException>... translations
  ) throws NullPointerException, TranslatedException, UncaughtException {
    Objects.requireNonNull(supplier, "supplier must not be null.");
    try {
      return supplier.get();
    } catch (Exception e) {
      var translatedException = Exceptions.translate(e, translations);
      if (translatedException == null) {
        return false;
      }
      throw translatedException;
    }
  }

  /**
   * If {@code supplier} throws an exception, tests if it matches one of a set of {@link Translation} rules and if it matches, translate it to another
   * exception according to that rule.
   * 
   * @param <TranslatedException>
   *   The type of exception that could be thrown by a matched translation. (Automatically determined.)
   * @param supplier
   *   the lambda code that possible could throw an exception.
   * @param translations
   *   is a set of rules that translate exceptions. The {@link Translation} predicate is tested and triggered in the order of this parameter.
   * 
   * @return byte value that {@code supplier} returns if it not throws any exception, or {@code 0} if the translated exception is {@code null}.
   * 
   * @throws TranslatedException
   *   the translated exception if the {@code supplier} throws an {@link Exception} that matches one of a set of {@link Translation} rules.
   * @throws NullPointerException
   *   if {@code supplier} is {@code null}
   * @throws UncaughtException
   *   wrapping the {@link Exception} thrown by {@code supplier} if no translation rules matches it.
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import static lightsvc.err.Exceptions.Translation.when;
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       var value = Exceptions.<strong>translateByte</strong>(
   *         ()->myMethod(),
   *         when(Exceptions.hasType(IOException.class)).to(cause->new MyMethodException("Something gone wrong!", cause))
   *       );
   *     }
   *     
   *     public static class MyMethodException extends RuntimeException {
   *       public MyMethodException(String message, Throwable cause) {
   *         super(message, cause);
   *       }
   *     }
   *     
   *     public static <strong>byte</strong> myMethod() throws IOException {
   *       throw new IOException();
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  @SafeVarargs
  // SonarLint: Checked exceptions should not be thrown. (This method does not limit the user choice. Is due to the user the type of exception it can use.)
  @SuppressWarnings("java:S1162")
  public static <TranslatedException extends Exception> byte translateByte(
    final ByteSupplier<Exception> supplier,
    final Translation<TranslatedException>... translations
  ) throws NullPointerException, TranslatedException, UncaughtException {
    Objects.requireNonNull(supplier, "supplier must not be null.");
    try {
      return supplier.get();
    } catch (Exception e) {
      var translatedException = Exceptions.translate(e, translations);
      if (translatedException == null) {
        return (byte) 0;
      }
      throw translatedException;
    }
  }

  /**
   * If {@code supplier} throws an exception, tests if it matches one of a set of {@link Translation} rules and if it matches, translate it to another
   * exception according to that rule.
   * 
   * @param <TranslatedException>
   *   The type of exception that could be thrown by a matched translation. (Automatically determined.)
   * @param supplier
   *   the lambda code that possible could throw an exception.
   * @param translations
   *   is a set of rules that translate exceptions. The {@link Translation} predicate is tested and triggered in the order of this parameter.
   * 
   * @return short value that {@code supplier} returns if it not throws any exception, or {@code 0} if the translated exception is {@code null}.
   * 
   * @throws TranslatedException
   *   the translated exception if the {@code supplier} throws an {@link Exception} that matches one of a set of {@link Translation} rules.
   * @throws NullPointerException
   *   if {@code supplier} is {@code null}
   * @throws UncaughtException
   *   wrapping the {@link Exception} thrown by {@code supplier} if no translation rules matches it.
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import static lightsvc.err.Exceptions.Translation.when;
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       var value = Exceptions.<strong>translateShort</strong>(
   *         ()->myMethod(),
   *         when(Exceptions.hasType(IOException.class)).to(cause->new MyMethodException("Something gone wrong!", cause))
   *       );
   *     }
   *     
   *     public static class MyMethodException extends RuntimeException {
   *       public MyMethodException(String message, Throwable cause) {
   *         super(message, cause);
   *       }
   *     }
   *     
   *     public static <strong>short</strong> myMethod() throws IOException {
   *       throw new IOException();
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  @SafeVarargs
  // SonarLint: Checked exceptions should not be thrown. (This method does not limit the user choice. Is due to the user the type of exception it can use.)
  @SuppressWarnings("java:S1162")
  public static <TranslatedException extends Exception> short translateShort(
    final ShortSupplier<Exception> supplier,
    final Translation<TranslatedException>... translations
  ) throws NullPointerException, TranslatedException, UncaughtException {
    Objects.requireNonNull(supplier, "supplier must not be null.");
    try {
      return supplier.get();
    } catch (Exception e) {
      var translatedException = Exceptions.translate(e, translations);
      if (translatedException == null) {
        return (short) 0;
      }
      throw translatedException;
    }
  }

  /**
   * If {@code supplier} throws an exception, tests if it matches one of a set of {@link Translation} rules and if it matches, translate it to another
   * exception according to that rule.
   * 
   * @param <TranslatedException>
   *   The type of exception that could be thrown by a matched translation. (Automatically determined.)
   * @param supplier
   *   the lambda code that possible could throw an exception.
   * @param translations
   *   is a set of rules that translate exceptions. The {@link Translation} predicate is tested and triggered in the order of this parameter.
   * 
   * @return char value that {@code supplier} returns if it not throws any exception, or {@code '\0'} if the translated exception is {@code null}.
   * 
   * @throws TranslatedException
   *   the translated exception if the {@code supplier} throws an {@link Exception} that matches one of a set of {@link Translation} rules.
   * @throws NullPointerException
   *   if {@code supplier} is {@code null}
   * @throws UncaughtException
   *   wrapping the {@link Exception} thrown by {@code supplier} if no translation rules matches it.
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import static lightsvc.err.Exceptions.Translation.when;
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       var value = Exceptions.<strong>translateChar</strong>(
   *         ()->myMethod(),
   *         when(Exceptions.hasType(IOException.class)).to(cause->new MyMethodException("Something gone wrong!", cause))
   *       );
   *     }
   *     
   *     public static class MyMethodException extends RuntimeException {
   *       public MyMethodException(String message, Throwable cause) {
   *         super(message, cause);
   *       }
   *     }
   *     
   *     public static <strong>char</strong> myMethod() throws IOException {
   *       throw new IOException();
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  @SafeVarargs
  // SonarLint: Checked exceptions should not be thrown. (This method does not limit the user choice. Is due to the user the type of exception it can use.)
  @SuppressWarnings("java:S1162")
  public static <TranslatedException extends Exception> char translateChar(
    final CharSupplier<Exception> supplier,
    final Translation<TranslatedException>... translations
  ) throws NullPointerException, TranslatedException, UncaughtException {
    Objects.requireNonNull(supplier, "supplier must not be null.");
    try {
      return supplier.get();
    } catch (Exception e) {
      var translatedException = Exceptions.translate(e, translations);
      if (translatedException == null) {
        return '\0';
      }
      throw translatedException;
    }
  }

  /**
   * If {@code supplier} throws an exception, tests if it matches one of a set of {@link Translation} rules and if it matches, translate it to another
   * exception according to that rule.
   * 
   * @param <TranslatedException>
   *   The type of exception that could be thrown by a matched translation. (Automatically determined.)
   * @param supplier
   *   the lambda code that possible could throw an exception.
   * @param translations
   *   is a set of rules that translate exceptions. The {@link Translation} predicate is tested and triggered in the order of this parameter.
   * 
   * @return int value that {@code supplier} returns if it not throws any exception, or {@code 0} if the translated exception is {@code null}.
   * 
   * @throws TranslatedException
   *   the translated exception if the {@code supplier} throws an {@link Exception} that matches one of a set of {@link Translation} rules.
   * @throws NullPointerException
   *   if {@code supplier} is {@code null}
   * @throws UncaughtException
   *   wrapping the {@link Exception} thrown by {@code supplier} if no translation rules matches it.
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import static lightsvc.err.Exceptions.Translation.when;
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       var value = Exceptions.<strong>translateInt</strong>(
   *         ()->myMethod(),
   *         when(Exceptions.hasType(IOException.class)).to(cause->new MyMethodException("Something gone wrong!", cause))
   *       );
   *     }
   *     
   *     public static class MyMethodException extends RuntimeException {
   *       public MyMethodException(String message, Throwable cause) {
   *         super(message, cause);
   *       }
   *     }
   *     
   *     public static <strong>int</strong> myMethod() throws IOException {
   *       throw new IOException();
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  @SafeVarargs
  // SonarLint: Checked exceptions should not be thrown. (This method does not limit the user choice. Is due to the user the type of exception it can use.)
  @SuppressWarnings("java:S1162")
  public static <TranslatedException extends Exception> int translateInt(
    final IntSupplier<Exception> supplier,
    final Translation<TranslatedException>... translations
  ) throws NullPointerException, TranslatedException, UncaughtException {
    Objects.requireNonNull(supplier, "supplier must not be null.");
    try {
      return supplier.get();
    } catch (Exception e) {
      var translatedException = Exceptions.translate(e, translations);
      if (translatedException == null) {
        return 0;
      }
      throw translatedException;
    }
  }

  /**
   * If {@code supplier} throws an exception, tests if it matches one of a set of {@link Translation} rules and if it matches, translate it to another
   * exception according to that rule.
   * 
   * @param <TranslatedException>
   *   The type of exception that could be thrown by a matched translation. (Automatically determined.)
   * @param supplier
   *   the lambda code that possible could throw an exception.
   * @param translations
   *   is a set of rules that translate exceptions. The {@link Translation} predicate is tested and triggered in the order of this parameter.
   * 
   * @return long value that {@code supplier} returns if it not throws any exception, or {@code 0} if the translated exception is {@code null}.
   * 
   * @throws TranslatedException
   *   the translated exception if the {@code supplier} throws an {@link Exception} that matches one of a set of {@link Translation} rules.
   * @throws NullPointerException
   *   if {@code supplier} is {@code null}
   * @throws UncaughtException
   *   wrapping the {@link Exception} thrown by {@code supplier} if no translation rules matches it.
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import static lightsvc.err.Exceptions.Translation.when;
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       var value = Exceptions.<strong>translateLong</strong>(
   *         ()->myMethod(),
   *         when(Exceptions.hasType(IOException.class)).to(cause->new MyMethodException("Something gone wrong!", cause))
   *       );
   *     }
   *     
   *     public static class MyMethodException extends RuntimeException {
   *       public MyMethodException(String message, Throwable cause) {
   *         super(message, cause);
   *       }
   *     }
   *     
   *     public static <strong>long</strong> myMethod() throws IOException {
   *       throw new IOException();
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  @SafeVarargs
  // SonarLint: Checked exceptions should not be thrown. (This method does not limit the user choice. Is due to the user the type of exception it can use.)
  @SuppressWarnings("java:S1162")
  public static <TranslatedException extends Exception> long translateLong(
    final LongSupplier<Exception> supplier,
    final Translation<TranslatedException>... translations
  ) throws NullPointerException, TranslatedException, UncaughtException {
    Objects.requireNonNull(supplier, "supplier must not be null.");
    try {
      return supplier.get();
    } catch (Exception e) {
      var translatedException = Exceptions.translate(e, translations);
      if (translatedException == null) {
        return 0L;
      }
      throw translatedException;
    }
  }

  /**
   * If {@code supplier} throws an exception, tests if it matches one of a set of {@link Translation} rules and if it matches, translate it to another
   * exception according to that rule.
   * 
   * @param <TranslatedException>
   *   The type of exception that could be thrown by a matched translation. (Automatically determined.)
   * @param supplier
   *   the lambda code that possible could throw an exception.
   * @param translations
   *   is a set of rules that translate exceptions. The {@link Translation} predicate is tested and triggered in the order of this parameter.
   * 
   * @return float value that {@code supplier} returns if it not throws any exception, or {@code 0f} if the translated exception is {@code null}.
   * 
   * @throws TranslatedException
   *   the translated exception if the {@code supplier} throws an {@link Exception} that matches one of a set of {@link Translation} rules.
   * @throws NullPointerException
   *   if {@code supplier} is {@code null}
   * @throws UncaughtException
   *   wrapping the {@link Exception} thrown by {@code supplier} if no translation rules matches it.
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import static lightsvc.err.Exceptions.Translation.when;
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       var value = Exceptions.<strong>translateFloat</strong>(
   *         ()->myMethod(),
   *         when(Exceptions.hasType(IOException.class)).to(cause->new MyMethodException("Something gone wrong!", cause))
   *       );
   *     }
   *     
   *     public static class MyMethodException extends RuntimeException {
   *       public MyMethodException(String message, Throwable cause) {
   *         super(message, cause);
   *       }
   *     }
   *     
   *     public static <strong>float</strong> myMethod() throws IOException {
   *       throw new IOException();
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  @SafeVarargs
  // SonarLint: Checked exceptions should not be thrown. (This method does not limit the user choice. Is due to the user the type of exception it can use.)
  @SuppressWarnings("java:S1162")
  public static <TranslatedException extends Exception> float translateFloat(
    final FloatSupplier<Exception> supplier,
    final Translation<TranslatedException>... translations
  ) throws NullPointerException, TranslatedException, UncaughtException {
    Objects.requireNonNull(supplier, "supplier must not be null.");
    try {
      return supplier.get();
    } catch (Exception e) {
      var translatedException = Exceptions.translate(e, translations);
      if (translatedException == null) {
        return 0F;
      }
      throw translatedException;
    }
  }

  /**
   * If {@code supplier} throws an exception, tests if it matches one of a set of {@link Translation} rules and if it matches, translate it to another
   * exception according to that rule.
   * 
   * @param <TranslatedException>
   *   The type of exception that could be thrown by a matched translation. (Automatically determined.)
   * @param supplier
   *   the lambda code that possible could throw an exception.
   * @param translations
   *   is a set of rules that translate exceptions. The {@link Translation} predicate is tested and triggered in the order of this parameter.
   * 
   * @return double value that {@code supplier} returns if it not throws any exception, or {@code 0d} if the translated exception is {@code null}.
   * 
   * @throws TranslatedException
   *   the translated exception if the {@code supplier} throws an {@link Exception} that matches one of a set of {@link Translation} rules.
   * @throws NullPointerException
   *   if {@code supplier} is {@code null}
   * @throws UncaughtException
   *   wrapping the {@link Exception} thrown by {@code supplier} if no translation rules matches it.
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import static lightsvc.err.Exceptions.Translation.when;
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       var value = Exceptions.<strong>translateDouble</strong>(
   *         ()->myMethod(),
   *         when(Exceptions.hasType(IOException.class)).to(cause->new MyMethodException("Something gone wrong!", cause))
   *       );
   *     }
   *     
   *     public static class MyMethodException extends RuntimeException {
   *       public MyMethodException(String message, Throwable cause) {
   *         super(message, cause);
   *       }
   *     }
   *     
   *     public static <strong>double</strong> myMethod() throws IOException {
   *       throw new IOException();
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  @SafeVarargs
  // SonarLint: Checked exceptions should not be thrown. (This method does not limit the user choice. Is due to the user the type of exception it can use.)
  @SuppressWarnings("java:S1162")
  public static <TranslatedException extends Exception> double translateDouble(
    final DoubleSupplier<Exception> supplier,
    final Translation<TranslatedException>... translations
  ) throws NullPointerException, TranslatedException, UncaughtException {
    Objects.requireNonNull(supplier, "supplier must not be null.");
    try {
      return supplier.get();
    } catch (Exception e) {
      var translatedException = Exceptions.translate(e, translations);
      if (translatedException == null) {
        return 0D;
      }
      throw translatedException;
    }
  }

  /**
   * If {@code supplier} throws an exception, tests if it matches one of a set of {@link Translation} rules and if it matches, translate it to another
   * exception according to that rule.
   * 
   * @param <TranslatedException>
   *   The type of exception that could be thrown by a matched translation. (Automatically determined.)
   * @param runnable
   *   the lambda code that possible could throw an exception.
   * @param translations
   *   is a set of rules that translate exceptions. The {@link Translation} predicate is tested and triggered in the order of this parameter.
   * 
   * @throws TranslatedException
   *   the translated exception if the {@code runnable} throws an {@link Exception} that matches one of a set of {@link Translation} rules.
   * @throws NullPointerException
   *   if {@code runnable} is {@code null}
   * @throws UncaughtException
   *   wrapping the {@link Exception} thrown by {@code runnable} if no translation rules matches it.
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import static lightsvc.err.Exceptions.Translation.when;
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       var value = Exceptions.<strong>translate</strong>(
   *         ()->myMethod(),
   *         when(Exceptions.hasType(IOException.class)).to(cause->new MyMethodException("Something gone wrong!", cause))
   *       );
   *     }
   *     
   *     public static class MyMethodException extends RuntimeException {
   *       public MyMethodException(String message, Throwable cause) {
   *         super(message, cause);
   *       }
   *     }
   *     
   *     public static <strong>void</strong> myMethod() throws IOException {
   *       throw new IOException();
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  @SafeVarargs
  // SonarLint: Checked exceptions should not be thrown. (This method does not limit the user choice. Is due to the user the type of exception it can use.)
  @SuppressWarnings("java:S1162")
  public static <TranslatedException extends Exception> void translate(
    final Runnable<Exception> runnable,
    final Translation<TranslatedException>... translations
  ) throws NullPointerException, TranslatedException, UncaughtException {
    Objects.requireNonNull(runnable, "runnable must not be null.");
    try {
      runnable.run();
    } catch (Exception e) {
      var translatedException = Exceptions.translate(e, translations);
      if (translatedException == null) {
        return;
      }
      throw translatedException;
    }
  }

  /**
   * If {@code supplier} throws an exception, tests if it matches one of a set of {@link Translation} rules and if it matches, translate it to another
   * exception according to that rule.
   * 
   * @param <ReturnType>
   *   The return type of {@code supplier}.
   * @param <TranslatedException>
   *   The type of exception that could be thrown by a matched translation. (Automatically determined.)
   * @param supplier
   *   the lambda code that possible could throw an exception.
   * @param translations
   *   is a set of rules that translate exceptions. The {@link Translation} predicate is tested and triggered in the order of this parameter.
   * 
   * @return value that {@code supplier} returns if it not throws any exception, or {@code 0} if the translated exception is {@code null}.
   * 
   * @throws TranslatedException
   *   the translated exception if the {@code supplier} throws an {@link Exception} that matches one of a set of {@link Translation} rules.
   * @throws NullPointerException
   *   if {@code supplier} is {@code null}
   * @throws UncaughtException
   *   wrapping the {@link Exception} thrown by {@code supplier} if no translation rules matches it.
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import static lightsvc.err.Exceptions.Translation.when;
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *   <strong>
   *     public static void main(String[] args) {
   *       var value = Exceptions.<strong>translate</strong>(
   *         ()->myMethod(),
   *         when(Exceptions.hasType(IOException.class)).to(cause->new MyMethodException("Something gone wrong!", cause))
   *       );
   *     }
   *     
   *     public static class MyMethodException extends RuntimeException {
   *       public MyMethodException(String message, Throwable cause) {
   *         super(message, cause);
   *       }
   *     }
   *     
   *     // <strong>Any Object Type</strong> instead of String return type still applies for <strong>Supplier</strong>.
   *     public static <strong>String</strong> myMethod() throws IOException {
   *       throw new IOException();
   *     }
   *   </strong>
   *   }
   *   </code>
   *   </pre>
   */
  @SafeVarargs
  // SonarLint: Checked exceptions should not be thrown. (This method does not limit the user choice. Is due to the user the type of exception it can use.)
  @SuppressWarnings("java:S1162")
  public static <ReturnType, TranslatedException extends Exception> ReturnType translate(
    final Supplier<ReturnType, Exception> supplier,
    final Translation<TranslatedException>... translations
  ) throws NullPointerException, TranslatedException, UncaughtException {
    Objects.requireNonNull(supplier, "supplier must not be null.");
    try {
      return supplier.get();
    } catch (Exception e) {
      var translatedException = Exceptions.translate(e, translations);
      if (translatedException == null) {
        return null;
      }
      throw translatedException;
    }
  }

  /**
   * <p>
   * A {@code boolean} supplier functional method supporting exceptions.
   * </p>
   * <p>
   * Used in conjunction with {@link Exceptions#translateBoolean(BooleanSupplier, Translation...)} method. Normally, this type is automatically determined in a
   * lambda
   * expression.
   * </p>
   * 
   * @param <ExceptionType>
   *   The type of exception that can be thrown
   * 
   * @see Exceptions#shouldNotHappen(BooleanSupplier)
   * @see Exceptions#translateBoolean(BooleanSupplier, Translation...)
   */
  @FunctionalInterface
  public interface BooleanSupplier<ExceptionType extends Exception> {

    /**
     * A {@code boolean} supplier functional method that can possibly throw any {@code Exceptions}.
     * 
     * @return any boolean.
     * 
     * @throws ExceptionType
     *   any possible {@code Exception}, if necessary.
     */
    @SuppressWarnings("java:S2047") // SonarLint: The names of methods with boolean return values should start with "is" or "has". (Not for suppliers)
    boolean get() throws ExceptionType;

  }

  /**
   * <p>
   * A {@code byte} supplier functional method supporting exceptions.
   * </p>
   * <p>
   * Used in conjunction with {@link Exceptions#translateByte(ByteSupplier, Translation...)} method. Normally, this type is automatically determined in a lambda
   * expression.
   * </p>
   * 
   * @param <ExceptionType>
   *   The type of exception that can be thrown
   * 
   * @see Exceptions#shouldNotHappen(ByteSupplier)
   * @see Exceptions#translateByte(ByteSupplier, Translation...)
   */
  @FunctionalInterface
  public interface ByteSupplier<ExceptionType extends Exception> {

    /**
     * A {@code byte} supplier functional method that can possibly throw any {@code Exceptions}.
     * 
     * @return any byte.
     * 
     * @throws ExceptionType
     *   any possible {@code Exception}, if necessary.
     */
    byte get() throws ExceptionType;

  }

  /**
   * <p>
   * A {@code short} supplier functional method supporting exceptions.
   * </p>
   * <p>
   * Used in conjunction with {@link Exceptions#translateShort(ShortSupplier, Translation...)} method. Normally, this type is automatically determined in a
   * lambda
   * expression.
   * </p>
   * 
   * @param <ExceptionType>
   *   The type of exception that can be thrown
   * 
   * @see Exceptions#shouldNotHappen(ShortSupplier)
   * @see Exceptions#translateShort(ShortSupplier, Translation...)
   */
  @FunctionalInterface
  public interface ShortSupplier<ExceptionType extends Exception> {

    /**
     * A {@code short} supplier functional method that can possibly throw any {@code Exceptions}.
     * 
     * @return any short.
     * 
     * @throws ExceptionType
     *   any possible {@code Exception}, if necessary.
     */
    short get() throws ExceptionType;

  }

  /**
   * <p>
   * A {@code char} supplier functional method supporting exceptions.
   * </p>
   * <p>
   * Used in conjunction with {@link Exceptions#translateChar(CharSupplier, Translation...)} method. Normally, this type is automatically determined in a lambda
   * expression.
   * </p>
   * 
   * @param <ExceptionType>
   *   The type of exception that can be thrown
   * 
   * @see Exceptions#shouldNotHappen(CharSupplier)
   * @see Exceptions#translateChar(CharSupplier, Translation...)
   */
  @FunctionalInterface
  public interface CharSupplier<ExceptionType extends Exception> {

    /**
     * A {@code char} supplier functional method that can throw any {@code Exceptions}.
     * 
     * @return any char.
     * 
     * @throws ExceptionType
     *   any possible {@code Exception}, if necessary.
     */
    char get() throws ExceptionType;

  }

  /**
   * <p>
   * A {@code float} supplier functional method supporting exceptions.
   * </p>
   * <p>
   * Used in conjunction with {@link Exceptions#translateFloat(FloatSupplier, Translation...)} method. Normally, this type is automatically determined in a
   * lambda
   * expression.
   * </p>
   * 
   * @param <ExceptionType>
   *   The type of exception that can be thrown
   * 
   * @see Exceptions#shouldNotHappen(FloatSupplier)
   * @see Exceptions#translateFloat(FloatSupplier, Translation...)
   */
  @FunctionalInterface
  public interface FloatSupplier<ExceptionType extends Exception> {

    /**
     * A {@code float} supplier functional method that can throw any {@code Exceptions}.
     * 
     * @return any float.
     * 
     * @throws ExceptionType
     *   any possible {@code Exception}, if necessary.
     */
    float get() throws ExceptionType;

  }

  /**
   * <p>
   * A {@code double} supplier functional method supporting exceptions.
   * </p>
   * <p>
   * Used in conjunction with {@link Exceptions#translateDouble(DoubleSupplier, Translation...)} method. Normally, this type is automatically determined in a
   * lambda
   * expression.
   * </p>
   * 
   * @param <ExceptionType>
   *   The type of exception that can be thrown
   * 
   * @see Exceptions#shouldNotHappen(DoubleSupplier)
   * @see Exceptions#translateDouble(DoubleSupplier, Translation...)
   */
  @FunctionalInterface
  public interface DoubleSupplier<ExceptionType extends Exception> {

    /**
     * A {@code double} supplier functional method that can throw any {@code Exceptions}.
     * 
     * @return any double.
     * 
     * @throws ExceptionType
     *   any possible {@code Exception}, if necessary.
     */
    double get() throws ExceptionType;

  }

  /**
   * <p>
   * A {@code int} supplier functional method supporting exceptions.
   * </p>
   * <p>
   * Used in conjunction with {@link Exceptions#translateInt(IntSupplier, Translation...)} method. Normally, this type is automatically determined in a lambda
   * expression.
   * </p>
   * 
   * @param <ExceptionType>
   *   The type of exception that can be thrown
   * 
   * @see Exceptions#shouldNotHappen(IntSupplier)
   * @see Exceptions#translateInt(IntSupplier, Translation...)
   */
  @FunctionalInterface
  public interface IntSupplier<ExceptionType extends Exception> {

    /**
     * A {@code int} supplier functional method that can throw any {@code Exceptions}.
     * 
     * @return any int.
     * 
     * @throws ExceptionType
     *   any possible {@code Exception}, if necessary.
     */
    int get() throws ExceptionType;

  }

  /**
   * <p>
   * A {@code long} supplier functional method supporting exceptions.
   * </p>
   * <p>
   * Used in conjunction with {@link Exceptions#translateLong(LongSupplier, Translation...)} method. Normally, this type is automatically determined in a lambda
   * expression.
   * </p>
   * 
   * @param <ExceptionType>
   *   The type of exception that can be thrown
   * 
   * @see Exceptions#shouldNotHappen(LongSupplier)
   * @see Exceptions#translateLong(LongSupplier, Translation...)
   */
  @FunctionalInterface
  public interface LongSupplier<ExceptionType extends Exception> {

    /**
     * A {@code long} supplier functional method that can throw any {@code Exceptions}.
     * 
     * @return any long.
     * 
     * @throws ExceptionType
     *   any possible {@code Exception}, if necessary.
     */
    long get() throws ExceptionType;

  }

  /**
   * <p>
   * A functional runnable that support exceptions.
   * </p>
   * <p>
   * Used in conjunction with {@link Exceptions#translate(Runnable, Translation...)} method. Normally, this type is automatically determined in a lambda
   * expression.
   * </p>
   * 
   * @param <ExceptionType>
   *   The type of exception that can be thrown
   * 
   * @see Exceptions#shouldNotHappen(Runnable)
   * @see Exceptions#translate(Runnable, Translation...)
   */
  @FunctionalInterface
  public interface Runnable<ExceptionType extends Exception> {

    /**
     * A functional method that can throw any {@code Exceptions}.
     * 
     * @throws ExceptionType
     *   any possible {@code Exception}, if necessary.
     */
    void run() throws ExceptionType;

  }

  /**
   * <p>
   * A {@link Object} supplier functional method supporting exceptions.
   * </p>
   * <p>
   * Used in conjunction with {@link Exceptions#translate(Supplier, Translation...)} method. Normally, this type is automatically determined in a lambda
   * expression.
   * </p>
   * 
   * @param <ReturnType>
   *   The type of value that will be returned if no exception is thrown.
   * @param <ExceptionType>
   *   The type of exception that can be thrown
   * 
   * @see Exceptions#shouldNotHappen(Supplier)
   * @see Exceptions#translate(Supplier, Translation...)
   */
  @FunctionalInterface
  public interface Supplier<ReturnType, ExceptionType extends Exception> {

    /**
     * A supplier functional method that can throw any {@code Exceptions}.
     * 
     * @return any {@link Object}.
     * 
     * @throws ExceptionType
     *   any possible {@code Exception}, if necessary.
     */
    ReturnType get() throws ExceptionType;

  }

  /**
   * <p>
   * Used in conjunction with {@link Exceptions#translate(Supplier, Translation...)} family of methods. This class represents a exceptions translation rule,
   * where a translation is triggered when a {@code exceptionPredicate} matches against an {@link Exception} thrown by the translate method supplier and
   * translated to another exception returned by {@code toFn}.
   * </p>
   * <p>
   * The static method {@link #when(Predicate)} provides a more semantic way than the use of this class constructor.
   * </p>
   * 
   * @param <TranslatedException>
   *   The type of exception that could be thrown by a matched translation. (Automatically determined.)
   * @param exceptionPredicate
   *   tests translate method supplier thrown {@link Exception} against this predicate rule.
   * @param toFn
   *   translate the supplier thrown {@link Exception} to a new exception.
   * 
   * @see Exceptions#translate(Supplier, Translation...)
   * @see #when(Predicate)
   * @see Builder#to(Function)
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   import static lightsvc.err.Exceptions.Translation.when;
   *   import java.io.IOException;
   *   import lightsvc.err.Exceptions;
   *   
   *   public class Example {
   *     public static void main(String[] args) {
   *       var value = Exceptions.translate(
   *         ()->myMethod(),
   *         <strong>when</strong>(Exceptions.hasType(IOException.class)).<strong>to</strong>(cause->new MyMethodException("Something gone wrong!", cause))
   *       );
   *     }
   *     
   *     public static class MyMethodException extends RuntimeException {
   *       public MyMethodException(String message, Throwable cause) {
   *         super(message, cause);
   *       }
   *     }
   *     
   *     public static String myMethod() throws IOException {
   *       throw new IOException();
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  @SuppressWarnings("javadoc")
  public static record Translation<TranslatedException extends Exception>(
    Predicate<Exception> exceptionPredicate,
    Function<Exception, TranslatedException> toFn
  ) implements Predicate<Exception> {

    /**
     * Constructor that validade if both {@code exceptionPredicate} and {@code toFn} are valid.
     * 
     * @see Exceptions#translate(Supplier, Translation...)
     * @see #when(Predicate)
     * @see Builder#to(Function)
     */
    public Translation {
      Objects.requireNonNull(exceptionPredicate, "exceptionPredicate must not be null.");
      Objects.requireNonNull(toFn, "toFn must not be null.");
    }

    /**
     * <p>
     * Construct an instance of {@link Builder} that allows a more semantic way to create a {@link Translation}.
     * </p>
     * <p>
     * It is suggested to use this method imported statically.
     * </p>
     * 
     * @param exceptionPredicate
     *   tests translate method supplier thrown {@link Exception} against this predicate rule.
     * 
     * @return an {@link Builder} that provides a more semantic way to create a {@link Translation}.
     * 
     * @see Exceptions#translate(Supplier, Translation...)
     * @see Builder#inCauseChain()
     * @see Builder#to(Function)
     */
    public static Translation.Builder when(Predicate<Exception> exceptionPredicate) {
      Objects.requireNonNull(exceptionPredicate, "exceptionPredicate must not be null.");
      return new Builder(exceptionPredicate);
    }

    /**
     * <p>
     * Tests if an {@code exception} matches against this translation {@code exceptionPredicate}.
     * </p>
     * <p>
     * Used internally and not commonly used directly by applications that use this library.
     * </p>
     * 
     * @param exception
     *   the {@link Exception} to be tested against this translation.
     * 
     * @return {@code true} if {@code exception} matches {@code exceptionPredicate}, otherwise {@code false}.
     * 
     * @see Predicate#test(Object)
     */
    @Override
    public boolean test(Exception exception) {
      Objects.requireNonNull(exception, "exception must not be null.");
      return this.exceptionPredicate.test(exception);
    }

    /**
     * <p>
     * An intermediate object that allows a more semantic way to create a {@link Translation}.
     * </p>
     * <p>
     * Created by {@link Translation#when(Predicate)} method, and not commonly used directly by applications that use this library.
     * </p>
     * 
     * @see Exceptions#translate(Supplier, Translation...)
     * @see #when(Predicate)
     * @see Builder#to(Function)
     */
    public static class Builder {

      protected Predicate<Exception> exceptionPredicate;

      /**
       * <p>
       * Constructor of {@link Builder}, an intermediate object that allows a more semantic way to create a {@link Translation}.
       * </p>
       * <p>
       * Created by {@link Translation#when(Predicate)} method, and not commonly used directly by applications that use this library.
       * </p>
       * 
       * @param exceptionPredicate
       *   will be passed to {@link Translation} when subsequent method {@link #to(Function)} is called.
       * 
       * @see Exceptions#translate(Supplier, Translation...)
       * @see #when(Predicate)
       * @see Builder#to(Function)
       */
      public Builder(Predicate<Exception> exceptionPredicate) {
        Objects.requireNonNull(exceptionPredicate, "exceptionPredicate must not be null.");
        this.exceptionPredicate = exceptionPredicate;
      }

      /**
       * Changes the {@link Builder} {@code exceptionPredicate} to test the {@link Exception} passed against all stacktrace cause chain instead of only the
       * immediate one.
       * 
       * @return the a new, modified, {@link Builder} that tests all stacktrace cause chain exceptions.
       * 
       * @see Exceptions#translate(Supplier, Translation...)
       * @see #when(Predicate)
       * @see Builder#to(Function)
       * 
       * @example
       * 
       *   <pre>
       *   <code>
       *   import static lightsvc.err.Exceptions.Translation.when;
       *   import java.io.IOException;
       *   import lightsvc.err.Exceptions;
       *   
       *   public class Example {
       *     public static void main(String[] args) {
       *       Exceptions.<strong>translate</strong>(
       *         ()->myMethod(),
       *         <strong>when</strong>(Exceptions.hasType(IOException.class)).<strong>inCauseChain()</strong>.
       *         <strong>to</strong>(cause->new MyMethodException("Something gone wrong!", cause))
       *       );
       *     }
       *     
       *     public static class MyMethodException extends RuntimeException {
       *       public MyMethodException(String message, Throwable cause) {
       *         super(message, cause);
       *       }
       *     }
       *     
       *     public static void myMethod() throws Exception {
       *       throw new Exception("Exception wrapping IOException", new IOException());
       *     }
       *   }
       *   </code>
       *   </pre>
       */
      public Builder inCauseChain() {
        return new Builder(Exceptions.inCauseChain(this.exceptionPredicate));
      }

      /**
       * Creates a {@link Translation} using {@code exceptionPredicate}, previously passed in {@link Translation#when(Predicate)}, and {@code toFn}.
       * 
       * @param <TranslatedException>
       *   The type of exception that could be thrown by a matched translation. (Automatically determined.)
       * @param toFn
       *   the supplier of the translated exception used when creating the {@link Translation}.
       * 
       * @return a new {@link Translation} using {@code exceptionPredicate} and {@code toFn}.
       * 
       * @see Exceptions#translate(Supplier, Translation...)
       * @see #when(Predicate)
       * 
       * @example
       * 
       *   <pre>
       *   <code>
       *   import static lightsvc.err.Exceptions.Translation.when;
       *   import java.io.IOException;
       *   import lightsvc.err.Exceptions;
       *   
       *   public class Example {
       *     public static void main(String[] args) {
       *       Exceptions.<strong>translate</strong>(
       *         ()->myMethod(),
       *         <strong>when</strong>(Exceptions.hasType(IOException.class)).<strong>to</strong>(cause->new MyMethodException("Something gone wrong!", cause))
       *       );
       *     }
       *     
       *     public static class MyMethodException extends RuntimeException {
       *       public MyMethodException(String message, Throwable cause) {
       *         super(message, cause);
       *       }
       *     }
       *     
       *     public static void myMethod() throws IOException {
       *       throw new IOException();
       *     }
       *   }
       *   </code>
       *   </pre>
       */
      public <TranslatedException extends Exception> Translation<TranslatedException> to(final Function<Exception, TranslatedException> toFn) {
        Objects.requireNonNull(toFn, "toFn must not be null.");
        return new Translation<>(this.exceptionPredicate, toFn);
      }

    }

  }

}
