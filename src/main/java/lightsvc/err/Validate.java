// This file is part of lightsvc Java Microservices Components (lightsvc).
//
// lightsvc Java Microservices Components (lightsvc) is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lightsvc Java Microservices Components (lightsvc) is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with lightsvc Java Microservices Components (lightsvc). If not, see <http://www.gnu.org/licenses/>.
package lightsvc.err;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;

import lightsvc.err.Exceptions.Supplier;
import lightsvc.util.Strings;

/**
 * Common semantic one-liners validation methods. Can be used to replace if-throw constructs in a shorter semantic way.
 * 
 * @since 1.0
 *
 * @author Tiago van den Berg
 */
// SonarLint: Files should not have too many lines of code. (Special case. Big utility class full of static methods).
// SonarLint: Classes should not have too many methods. (Special case. Big utility class full of static methods).
// SonarLint: Classes should not be coupled too many other classes. (Special case. Big utility class full of static methods).
@SuppressWarnings({
  "java:S104", "java:S1200", "java:S1448"
})
public final class Validate {

  private Validate() {
    throw new UnsupportedOperationException("lightsvc.err.Validate cannot be instantiated.");
  }

  /**
   * Validate if {@code value} is {@code true}, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the {@code boolean} value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code false}.
   */
  // SonarLint: Checked exceptions should not be thrown. (Special case. Left to user code not use an checked exception on usage).
  @SuppressWarnings("java:S1162")
  public static <ExceptionType extends Exception> boolean is(
    final boolean value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Objects.requireNonNull(exceptionSupplier, ()->"exceptionSupplier must not be null.");
    if (!value) {
      throw exceptionSupplier.get();
    }
    return value;
  }

  /**
   * Validate if {@code value} is {@code false}, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the {@code boolean} value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code true}.
   */
  // SonarLint: Checked exceptions should not be thrown. (Special case. Left to u code not use an checked exception on usage).
  @SuppressWarnings("java:S1162")
  public static <ExceptionType extends Exception> boolean not(
    final boolean value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Objects.requireNonNull(exceptionSupplier, ()->"exceptionSupplier must not be null.");
    if (value) {
      throw exceptionSupplier.get();
    }
    return value;
  }

  /**
   * Validate if {@code value} is equals or greater than {@code minValue}, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minValue
   *   the minimum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is smaller than {@code minValue}.
   */
  public static <ExceptionType extends Exception> byte minValue(
    final byte minValue,
    final byte value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is(value >= minValue, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is equals or greater than {@code minValue}, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minValue
   *   the minimum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is smaller than {@code minValue}.
   */
  public static <ExceptionType extends Exception> short minValue(
    final short minValue,
    final short value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is(value >= minValue, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is equals or greater than {@code minValue}, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minValue
   *   the minimum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is smaller than {@code minValue}.
   */
  public static <ExceptionType extends Exception> int minValue(
    final int minValue,
    final int value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is(value >= minValue, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is equals or greater than {@code minValue}, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minValue
   *   the minimum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is smaller than {@code minValue}.
   */
  public static <ExceptionType extends Exception> long minValue(
    final long minValue,
    final long value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is(value >= minValue, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is equals or greater than {@code minValue}, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minValue
   *   the minimum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is smaller than {@code minValue}.
   */
  public static <ExceptionType extends Exception> float minValue(
    final float minValue,
    final float value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is(value >= minValue, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is equals or greater than {@code minValue}, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minValue
   *   the minimum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or is smaller than {@code minValue}.
   */
  public static <ExceptionType extends Exception> double minValue(
    final double minValue,
    final double value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is(value >= minValue, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is not {@code null} and is equals or greater than {@code minValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minValue
   *   the minimum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code minValue} or {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or is smaller than {@code minValue}.
   */
  public static <ExceptionType extends Exception> BigInteger minValue(
    final BigInteger minValue,
    final BigInteger value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Objects.requireNonNull(minValue, "minValue must not be null");
    Validate.is(value != null && value.compareTo(minValue) >= 0, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is not {@code null} and is equals or greater than {@code minValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minValue
   *   the minimum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code minValue} or {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or is smaller than {@code minValue}.
   */
  public static <ExceptionType extends Exception> BigDecimal minValue(
    final BigDecimal minValue,
    final BigDecimal value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Objects.requireNonNull(minValue, "minValue must not be null");
    Validate.is(value != null && value.compareTo(minValue) >= 0, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is equals or smaller than {@code maxValue}, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxValue
   *   the maximum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is greater than {@code maxValue}.
   */
  public static <ExceptionType extends Exception> byte maxValue(
    final byte maxValue,
    final byte value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is(value <= maxValue, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is equals or smaller than {@code maxValue}, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxValue
   *   the maximum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is greater than {@code maxValue}.
   */
  public static <ExceptionType extends Exception> short maxValue(
    final short maxValue,
    final short value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is(value <= maxValue, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is equals or smaller than {@code maxValue}, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxValue
   *   the maximum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is greater than {@code maxValue}.
   */
  public static <ExceptionType extends Exception> int maxValue(
    final int maxValue,
    final int value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is(value <= maxValue, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is equals or smaller than {@code maxValue}, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxValue
   *   the maximum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is greater than {@code maxValue}.
   */
  public static <ExceptionType extends Exception> long maxValue(
    final long maxValue,
    final long value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is(value <= maxValue, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is equals or smaller than {@code maxValue}, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxValue
   *   the maximum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is greater than {@code maxValue}.
   */
  public static <ExceptionType extends Exception> float maxValue(
    final float maxValue,
    final float value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is(value <= maxValue, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is equals or smaller than {@code maxValue}, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxValue
   *   the maximum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is greater than {@code maxValue}.
   */
  public static <ExceptionType extends Exception> double maxValue(
    final double maxValue,
    final double value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is(value <= maxValue, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is not {@code null} and is equals or smaller than {@code minValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxValue
   *   the minimum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code maxValue} or {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or is greater than {@code maxValue}.
   */
  public static <ExceptionType extends Exception> BigInteger maxValue(
    final BigInteger maxValue,
    final BigInteger value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Objects.requireNonNull(maxValue, "maxValue must not be null");
    Validate.is(value != null && value.compareTo(maxValue) <= 0, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is not {@code null} and is equals or smaller than {@code maxValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxValue
   *   the maximum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code maxValue} or {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or is greater than {@code maxValue}.
   */
  public static <ExceptionType extends Exception> BigDecimal maxValue(
    final BigDecimal maxValue,
    final BigDecimal value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Objects.requireNonNull(maxValue, "maxValue must not be null");
    Validate.is(value != null && value.compareTo(maxValue) <= 0, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is {@code null}, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is not {@code null}.
   */
  public static <Value, ExceptionType extends Exception> Value isNull(
    final Value value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is(value == null, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is not {@code null}, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null}.
   */
  public static <Value, ExceptionType extends Exception> Value notNull(
    final Value value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is(value != null, exceptionSupplier);
    return value;
  }

  /**
   * Validate if an {@link Optional} {@code value} is present, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value} inner value ({@link Optional#get()}).
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or not present.
   */
  // SonarLint: "Optional" should not be used for parameters. (Special case where this method is used to unwrap an optional provided by an previous statement
  // return.)
  @SuppressWarnings("java:S3553")
  public static <Value, ExceptionType extends Exception> Value isPresent(
    final Optional<Value> value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.notNull(value, exceptionSupplier);
    Validate.is(value.isPresent(), exceptionSupplier);
    return value.get();
  }

  /**
   * Validate if a {@link String} {@code value} is not {@code null} and not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or empty.
   */
  public static <ExceptionType extends Exception> String notEmpty(
    final String value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && !value.isEmpty(), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code boolean} array {@code value} is not {@code null} and not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or empty.
   */
  public static <ExceptionType extends Exception> boolean[] notEmpty(
    final boolean[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length > 0), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code byte} array {@code value} is not {@code null} and not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or empty.
   */
  public static <ExceptionType extends Exception> byte[] notEmpty(
    final byte[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length > 0), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code short} array {@code value} is not {@code null} and not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or empty.
   */
  public static <ExceptionType extends Exception> short[] notEmpty(
    final short[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length > 0), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code char} array {@code value} is not {@code null} and not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or empty.
   */
  public static <ExceptionType extends Exception> char[] notEmpty(
    final char[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length > 0), exceptionSupplier);
    return value;
  }

  /**
   * Validate if an {@code int} array {@code value} is not {@code null} and not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or empty.
   */
  public static <ExceptionType extends Exception> int[] notEmpty(
    final int[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length > 0), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code long} array {@code value} is not {@code null} and not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or empty.
   */
  public static <ExceptionType extends Exception> long[] notEmpty(
    final long[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length > 0), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code float} array {@code value} is not {@code null} and not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or empty.
   */
  public static <ExceptionType extends Exception> float[] notEmpty(
    final float[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length > 0), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code double} array {@code value} is not {@code null} and not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or empty.
   */
  public static <ExceptionType extends Exception> double[] notEmpty(
    final double[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length > 0), exceptionSupplier);
    return value;
  }

  /**
   * Validate if an array is not {@code null} and not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} array (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or empty.
   */
  public static <Value, ExceptionType extends Exception> Value[] notEmpty(
    final Value[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length > 0), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@link Collection} is not {@code null} and not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} element (automatically determined).
   * @param <ACollection>
   *   the type of {@code value} {@link Collection} (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or empty.
   */
  public static <Value, ACollection extends Collection<Value>, ExceptionType extends Exception> ACollection notEmpty(
    final ACollection value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && !value.isEmpty(), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@link Map} is not {@code null} and not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <Key>
   *   the type of {@code value} {@link Entry} key (automatically determined).
   * @param <Value>
   *   the type of {@code value} {@link Entry} value (automatically determined).
   * @param <AMap>
   *   the type of {@code value} {@link Map} (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or empty.
   */
  public static <Key, Value, AMap extends Map<Key, Value>, ExceptionType extends Exception> AMap notEmpty(
    final AMap value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && !value.isEmpty(), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@link String} {@code value} is not {@code null} and not blank (contains more than white-space and control characters), or throw an exception
   * supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value} stripped of blank characters.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null}, empty or blank (contains only white-space and control characters).
   */
  public static <ExceptionType extends Exception> String notBlank(
    final String value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.notEmpty(value, exceptionSupplier);
    final var trimmedValue = value.strip();
    Validate.notEmpty(trimmedValue, exceptionSupplier);
    return trimmedValue;
  }

  /**
   * Validate if a {@link String} {@code value} not {@code null} and contains only digits, letters, punctuation and spaces (and not control characters), or
   * throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or contains control characters.
   */
  public static <ExceptionType extends Exception> String notContainsControlCharacters(
    final String value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is(value != null && !Strings.containsIsoControlCodes(value), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code value} is not {@code null} and assignable from a specific @{code type}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <Type>
   *   the {@link Class} type of {@code type} (automatically determined by {@code type}).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param type
   *   the expected {@link Class} type of {@code value}.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value} cast to {@code type}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or is not assignable to {@code type}.
   */
  public static <Type, ExceptionType extends Exception> Type type(
    final Class<Type> type,
    final Object value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Objects.requireNonNull(type, ()->"type must not be null.");
    Validate.is(value != null && type.isAssignableFrom(value.getClass()), exceptionSupplier);
    @SuppressWarnings("unchecked")
    var result = (Type) value;
    return result;
  }

  /**
   * Validate if a {@code enumValueName} is not {@code null} and can be converted to an {@link Enum}, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <Type>
   *   the {@link Class} type of {@code type} (automatically determined by {@code type}).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param type
   *   the expected {@link Class} type of {@code value}.
   * @param enumValueName
   *   the existent name of an {@link Enum} value.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return the found {@link Enum} value.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code enumValueName} is {@code null} or not found in {@code type}.
   */
  // SonarLint: Checked exceptions should not be thrown. (Special case. Left to u code not use an checked exception on usage).
  @SuppressWarnings("java:S1162")
  public static <Type extends Enum<Type>, ExceptionType extends Exception> Type enumValueName(
    final Class<Type> type,
    final String enumValueName,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Objects.requireNonNull(type, ()->"type must not be null.");
    Validate.notNull(enumValueName, exceptionSupplier);
    Type enumValue;
    try {
      enumValue = Enum.valueOf(type, enumValueName);
    } catch (@SuppressWarnings("unused")
    final IllegalArgumentException e) {
      throw exceptionSupplier.get();
    }
    return enumValue;
  }

  /**
   * Validate if an {@code boolean} array is not {@code null} and not empty and contains only one element, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return the first element of {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null}, empty or has more than one element.
   */
  @SuppressWarnings("null")
  public static <ExceptionType extends Exception> boolean singleton(
    final boolean[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length == 1), exceptionSupplier);
    return value[0];
  }

  /**
   * Validate if an {@code byte} array is not {@code null} and not empty and contains only one element, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return the first element of {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null}, empty or has more than one element.
   */
  @SuppressWarnings("null")
  public static <ExceptionType extends Exception> byte singleton(
    final byte[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length == 1), exceptionSupplier);
    return value[0];
  }

  /**
   * Validate if an {@code short} array is not {@code null} and not empty and contains only one element, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return the first element of {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null}, empty or has more than one element.
   */
  @SuppressWarnings("null")
  public static <ExceptionType extends Exception> short singleton(
    final short[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length == 1), exceptionSupplier);
    return value[0];
  }

  /**
   * Validate if an {@code char} array is not {@code null} and not empty and contains only one element, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return the first element of {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null}, empty or has more than one element.
   */
  @SuppressWarnings("null")
  public static <ExceptionType extends Exception> char singleton(
    final char[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length == 1), exceptionSupplier);
    return value[0];
  }

  /**
   * Validate if an {@code int} array is not {@code null} and not empty and contains only one element, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return the first element of {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null}, empty or has more than one element.
   */
  @SuppressWarnings("null")
  public static <ExceptionType extends Exception> int singleton(
    final int[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length == 1), exceptionSupplier);
    return value[0];
  }

  /**
   * Validate if an {@code long} array is not {@code null} and not empty and contains only one element, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return the first element of {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null}, empty or has more than one element.
   */
  @SuppressWarnings("null")
  public static <ExceptionType extends Exception> long singleton(
    final long[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length == 1), exceptionSupplier);
    return value[0];
  }

  /**
   * Validate if an {@code float} array is not {@code null} and not empty and contains only one element, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return the first element of {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null}, empty or has more than one element.
   */
  @SuppressWarnings("null")
  public static <ExceptionType extends Exception> float singleton(
    final float[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length == 1), exceptionSupplier);
    return value[0];
  }

  /**
   * Validate if an {@code double} array is not {@code null} and not empty and contains only one element, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return the first element of {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null}, empty or has more than one element.
   */
  @SuppressWarnings("null")
  public static <ExceptionType extends Exception> double singleton(
    final double[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length == 1), exceptionSupplier);
    return value[0];
  }

  /**
   * Validate if an array is not {@code null} and not empty and contains only one element, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} array (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return the first element of {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null}, empty or has more than one element.
   */
  @SuppressWarnings("null")
  public static <Value, ExceptionType extends Exception> Value singleton(
    final Value[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length == 1), exceptionSupplier);
    return value[0];
  }

  /**
   * Validate if a {@link Collection} is not {@code null} and not empty and contains only one element, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} {@link Collection} element (automatically determined).
   * @param <ACollection>
   *   the type of {@code value} {@link Collection} (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return the first element of {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null}, empty or has more than one element.
   */
  @SuppressWarnings("null")
  public static <Value, ACollection extends Collection<Value>, ExceptionType extends Exception> Value singleton(
    final ACollection value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.size() == 1), exceptionSupplier);
    return value.iterator().next();
  }

  /**
   * Validate if a {@code value} is not {@code null} and has at least a minimum length, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has less characters than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> String minLength(
    final int minLength,
    final String value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length() >= minLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code boolean} array is not {@code null} and has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has less elements than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> boolean[] minLength(
    final int minLength,
    final boolean[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length >= minLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code byte} array is not {@code null} and has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has less elements than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> byte[] minLength(
    final int minLength,
    final byte[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length >= minLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code short} array is not {@code null} and has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has less elements than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> short[] minLength(
    final int minLength,
    final short[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length >= minLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code char} array is not {@code null} and has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has less elements than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> char[] minLength(
    final int minLength,
    final char[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length >= minLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code int} array is not {@code null} and has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has less elements than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> int[] minLength(
    final int minLength,
    final int[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length >= minLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code long} array is not {@code null} and has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has less elements than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> long[] minLength(
    final int minLength,
    final long[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length >= minLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code float} array is not {@code null} and has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has less elements than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> float[] minLength(
    final int minLength,
    final float[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length >= minLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code double} array is not {@code null} and has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has less elements than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> double[] minLength(
    final int minLength,
    final double[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length >= minLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a array is not {@code null} and has at least a minimum quantity of elements, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} array (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has less elements than the minimum quantity expected.
   */
  public static <Value, ExceptionType extends Exception> Value[] minLength(
    final int minLength,
    final Value[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length >= minLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@link Collection} is not {@code null} and has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} element (automatically determined).
   * @param <ACollection>
   *   the type of {@code value} {@link Collection} (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has less elements than the minimum quantity expected.
   */
  public static <Value, ACollection extends Collection<Value>, ExceptionType extends Exception> ACollection minLength(
    final int minLength,
    final ACollection value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.size() >= minLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code value} is not {@code null} and is smaller than a maximum length, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> String maxLength(
    final int maxLength,
    final String value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length() <= maxLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code boolean} array is not {@code null} and has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> boolean[] maxLength(
    final int maxLength,
    final boolean[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length <= maxLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code byte} array is not {@code null} and has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> byte[] maxLength(
    final int maxLength,
    final byte[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length <= maxLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code short} array is not {@code null} and has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> short[] maxLength(
    final int maxLength,
    final short[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length <= maxLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code char} array is not {@code null} and has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> char[] maxLength(
    final int maxLength,
    final char[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length <= maxLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code int} array is not {@code null} and has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> int[] maxLength(
    final int maxLength,
    final int[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length <= maxLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code long} array is not {@code null} and has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> long[] maxLength(
    final int maxLength,
    final long[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length <= maxLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code float} array is not {@code null} and has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> float[] maxLength(
    final int maxLength,
    final float[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length <= maxLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code double} array is not {@code null} and has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> double[] maxLength(
    final int maxLength,
    final double[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length <= maxLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a array is not {@code null} and has less elements than a specified quantity, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} array (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has more elements than the maximum quantity expected.
   */
  public static <Value, ExceptionType extends Exception> Value[] maxLength(
    final int maxLength,
    final Value[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.length <= maxLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@link Collection} is not {@code null} and has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} element (automatically determined).
   * @param <ACollection>
   *   the type of {@code value} {@link Collection} (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has more elements than the maximum quantity expected.
   */
  public static <Value, ACollection extends Collection<Value>, ExceptionType extends Exception> ACollection maxLength(
    final int maxLength,
    final ACollection value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Validate.is((value != null) && (value.size() <= maxLength), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code value} is not {@code null} and matches to a regular exception pattern, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param pattern
   *   the expected {@code value} content pattern.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code pattern} or {@code exceptionSupplier} are {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or does not match {@code pattern}.
   */
  @SuppressWarnings("java:S1192") // SonarLint: String literals should not be duplicated. (For local contexts they should).
  public static <ExceptionType extends Exception> String regex(
    final String pattern,
    final String value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Objects.requireNonNull(pattern, ()->"pattern must not be null.");
    Validate.is(value != null && Pattern.matches(pattern, value), exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@code value} is not {@code null} and matches to a regular exception pattern, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param pattern
   *   the expected {@code value} content pattern.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code pattern} or {@code exceptionSupplier} are {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or does not match {@code pattern}.
   */
  public static <ExceptionType extends Exception> String regex(
    final Pattern pattern,
    final String value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Objects.requireNonNull(pattern, ()->"pattern must not be null.");
    Validate.is(value != null && pattern.matcher(value).matches(), exceptionSupplier);
    return value;
  }

  /**
   * A helper method that calls other validation methods only if the {@code value} is not null, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} (automatically determined).
   * @param <Return>
   *   the type of return (automatically determined by {@code otherValidateMethod}).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param otherValidateMethod
   *   other validation method that will be called if {@code value} in not {@code null}.
   * 
   * @return {@code otherValidateMethod} returned value.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null}, empty or has more than one element.
   */
  public static <Value, Return, ExceptionType extends Exception> Return optional(
    final Value value,
    final Exceptions.Supplier<Return, ExceptionType> otherValidateMethod
  ) throws NullPointerException, ExceptionType {
    Objects.requireNonNull(otherValidateMethod, ()->"otherValidateMethod must not be null.");
    if (value == null) {
      return null;
    }
    return otherValidateMethod.get();
  }

  /**
   * Validate if {@code value} is {@code null} or it is equals or greater than {@code minValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minValue
   *   the minimum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is smaller than {@code minValue}.
   */
  public static <ExceptionType extends Exception> Byte optionalMinValue(
    final byte minValue,
    final Byte value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    if (value == null) {
      return null;
    }
    Validate.minValue(minValue, value.byteValue(), exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is {@code null} or it is equals or greater than {@code minValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minValue
   *   the minimum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is smaller than {@code minValue}.
   */
  public static <ExceptionType extends Exception> Short optionalMinValue(
    final short minValue,
    final Short value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    if (value == null) {
      return null;
    }
    Validate.minValue(minValue, value.shortValue(), exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is {@code null} or it is equals or greater than {@code minValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minValue
   *   the minimum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is smaller than {@code minValue}.
   */
  public static <ExceptionType extends Exception> Integer optionalMinValue(
    final int minValue,
    final Integer value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    if (value == null) {
      return null;
    }
    Validate.minValue(minValue, value.intValue(), exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is {@code null} or it is equals or greater than {@code minValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minValue
   *   the minimum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is smaller than {@code minValue}.
   */
  public static <ExceptionType extends Exception> Long optionalMinValue(
    final long minValue,
    final Long value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    if (value == null) {
      return null;
    }
    Validate.minValue(minValue, value.longValue(), exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is {@code null} or it is equals or greater than {@code minValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minValue
   *   the minimum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is smaller than {@code minValue}.
   */
  public static <ExceptionType extends Exception> Float optionalMinValue(
    final float minValue,
    final Float value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    if (value == null) {
      return null;
    }
    Validate.minValue(minValue, value.floatValue(), exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is {@code null} or it is equals or greater than {@code minValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minValue
   *   the minimum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is smaller than {@code minValue}.
   */
  public static <ExceptionType extends Exception> Double optionalMinValue(
    final double minValue,
    final Double value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    if (value == null) {
      return null;
    }
    Validate.minValue(minValue, value.doubleValue(), exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is {@code null} or it is equals or greater than {@code minValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minValue
   *   the minimum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code minValue} or {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is smaller than {@code minValue}.
   */
  public static <ExceptionType extends Exception> BigInteger optionalMinValue(
    final BigInteger minValue,
    final BigInteger value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Objects.requireNonNull(minValue, "minValue must not be null");
    Objects.requireNonNull(exceptionSupplier, "exceptionSupplier must not be null");
    if (value == null) {
      return null;
    }
    Validate.minValue(minValue, value, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is {@code null} or it is equals or greater than {@code minValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minValue
   *   the minimum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code minValue} or {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is smaller than {@code minValue}.
   */
  public static <ExceptionType extends Exception> BigDecimal optionalMinValue(
    final BigDecimal minValue,
    final BigDecimal value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Objects.requireNonNull(minValue, "minValue must not be null");
    Objects.requireNonNull(exceptionSupplier, "exceptionSupplier must not be null");
    if (value == null) {
      return null;
    }
    Validate.minValue(minValue, value, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is {@code null} or it is equals or smaller than {@code maxValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxValue
   *   the maximum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is greater than {@code maxValue}.
   */
  public static <ExceptionType extends Exception> Byte optionalMaxValue(
    final byte maxValue,
    final Byte value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    if (value == null) {
      return null;
    }
    Validate.maxValue(maxValue, value.byteValue(), exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is {@code null} or it is equals or smaller than {@code maxValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxValue
   *   the maximum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is greater than {@code maxValue}.
   */
  public static <ExceptionType extends Exception> Short optionalMaxValue(
    final short maxValue,
    final Short value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    if (value == null) {
      return null;
    }
    Validate.maxValue(maxValue, value.shortValue(), exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is {@code null} or it is equals or smaller than {@code maxValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxValue
   *   the maximum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is greater than {@code maxValue}.
   */
  public static <ExceptionType extends Exception> Integer optionalMaxValue(
    final int maxValue,
    final Integer value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    if (value == null) {
      return null;
    }
    Validate.maxValue(maxValue, value.intValue(), exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is {@code null} or it is equals or smaller than {@code maxValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxValue
   *   the maximum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is greater than {@code maxValue}.
   */
  public static <ExceptionType extends Exception> Long optionalMaxValue(
    final long maxValue,
    final Long value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    if (value == null) {
      return null;
    }
    Validate.maxValue(maxValue, value.longValue(), exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is {@code null} or it is equals or smaller than {@code maxValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxValue
   *   the maximum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is greater than {@code maxValue}.
   */
  public static <ExceptionType extends Exception> Float optionalMaxValue(
    final float maxValue,
    final Float value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    if (value == null) {
      return null;
    }
    Validate.maxValue(maxValue, value.floatValue(), exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is {@code null} or it is equals or smaller than {@code maxValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxValue
   *   the maximum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is greater than {@code maxValue}.
   */
  public static <ExceptionType extends Exception> Double optionalMaxValue(
    final double maxValue,
    final Double value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    if (value == null) {
      return null;
    }
    Validate.maxValue(maxValue, value.doubleValue(), exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is {@code null} or it is equals or smaller than {@code maxValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxValue
   *   the maximum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code maxValue} or {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is greater than {@code maxValue}.
   */
  public static <ExceptionType extends Exception> BigInteger optionalMaxValue(
    final BigInteger maxValue,
    final BigInteger value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Objects.requireNonNull(maxValue, "maxValue must not be null");
    Objects.requireNonNull(exceptionSupplier, "exceptionSupplier must not be null");
    if (value == null) {
      return null;
    }
    Validate.maxValue(maxValue, value, exceptionSupplier);
    return value;
  }

  /**
   * Validate if {@code value} is {@code null} or it is equals or greater than {@code maxValue}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxValue
   *   the maximum value that {@code value} will be tested against.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code maxValue} or {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is greater than {@code maxValue}.
   */
  public static <ExceptionType extends Exception> BigDecimal optionalMaxValue(
    final BigDecimal maxValue,
    final BigDecimal value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Objects.requireNonNull(maxValue, "maxValue must not be null");
    Objects.requireNonNull(exceptionSupplier, "exceptionSupplier must not be null");
    if (value == null) {
      return null;
    }
    Validate.maxValue(maxValue, value, exceptionSupplier);
    return value;
  }

  /**
   * Validate if a {@link String} {@code value} is {@code null} or is not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is empty.
   */
  public static <ExceptionType extends Exception> String optionalNotEmpty(
    final String value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.notEmpty(value, exceptionSupplier));
  }

  /**
   * Validate if a {@code boolean} array {@code value} is {@code null} or is not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is empty.
   */
  public static <ExceptionType extends Exception> boolean[] optionalNotEmpty(
    final boolean[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.notEmpty(value, exceptionSupplier));
  }

  /**
   * Validate if a {@code byte} array {@code value} is {@code null} or is not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is empty.
   */
  public static <ExceptionType extends Exception> byte[] optionalNotEmpty(
    final byte[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.notEmpty(value, exceptionSupplier));
  }

  /**
   * Validate if a {@code short} array {@code value} is {@code null} or is not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is empty.
   */
  public static <ExceptionType extends Exception> short[] optionalNotEmpty(
    final short[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.notEmpty(value, exceptionSupplier));
  }

  /**
   * Validate if a {@code char} array {@code value} is {@code null} or is not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is empty.
   */
  public static <ExceptionType extends Exception> char[] optionalNotEmpty(
    final char[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.notEmpty(value, exceptionSupplier));
  }

  /**
   * Validate if an {@code int} array {@code value} is {@code null} or is not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is empty.
   */
  public static <ExceptionType extends Exception> int[] optionalNotEmpty(
    final int[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.notEmpty(value, exceptionSupplier));
  }

  /**
   * Validate if a {@code long} array {@code value} is {@code null} or is not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is empty.
   */
  public static <ExceptionType extends Exception> long[] optionalNotEmpty(
    final long[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.notEmpty(value, exceptionSupplier));
  }

  /**
   * Validate if a {@code float} array {@code value} is {@code null} or is not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is empty.
   */
  public static <ExceptionType extends Exception> float[] optionalNotEmpty(
    final float[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.notEmpty(value, exceptionSupplier));
  }

  /**
   * Validate if a {@code double} array {@code value} is {@code null} or is not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is empty.
   */
  public static <ExceptionType extends Exception> double[] optionalNotEmpty(
    final double[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.notEmpty(value, exceptionSupplier));
  }

  /**
   * Validate if an array is {@code null} or is not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} array (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is empty.
   */
  public static <Value, ExceptionType extends Exception> Value[] optionalNotEmpty(
    final Value[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.notEmpty(value, exceptionSupplier));
  }

  /**
   * Validate if a {@link Collection} is {@code null} or is not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} element (automatically determined).
   * @param <ACollection>
   *   the type of {@code value} {@link Collection} (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is empty.
   */
  public static <Value, ACollection extends Collection<Value>, ExceptionType extends Exception> ACollection optionalNotEmpty(
    final ACollection value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.notEmpty(value, exceptionSupplier));
  }

  /**
   * Validate if a {@link Map} is {@code null} or is not empty, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <Key>
   *   the type of {@code value} {@link Entry} key (automatically determined).
   * @param <Value>
   *   the type of {@code value} {@link Entry} value (automatically determined).
   * @param <AMap>
   *   the type of {@code value} {@link Map} (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is empty.
   */
  public static <Key, Value, AMap extends Map<Key, Value>, ExceptionType extends Exception> AMap optionalNotEmpty(
    final AMap value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.notEmpty(value, exceptionSupplier));
  }

  /**
   * Validate if a {@link String} {@code value} is {@code null} or is not blank (contains more than white-space and control characters), or throw an exception
   * supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value} stripped of blank characters.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is empty or blank (contains only white-space and control characters).
   */
  public static <ExceptionType extends Exception> String optionalNotBlank(
    final String value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.notBlank(value, exceptionSupplier));
  }

  /**
   * Validate if a {@link String} {@code value} is {@code null} or contains only digits, letters, punctuation and spaces (and not control characters), or throw
   * an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} contains control characters.
   */
  public static <ExceptionType extends Exception> String optionalNotContainsControlCharacters(
    final String value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.notContainsControlCharacters(value, exceptionSupplier));
  }

  /**
   * Validate if a {@code value} is {@code null} or is assignable from a specific @{code type}, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <Type>
   *   the {@link Class} type of {@code type} (automatically determined by {@code type}).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param type
   *   the expected {@link Class} type of {@code value}.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value} cast to {@code type}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is not assignable to {@code type}.
   */
  public static <Type, ExceptionType extends Exception> Type optionalType(
    final Class<Type> type,
    final Object value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.type(type, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code enumValueName} is {@code null} or can be converted to an {@link Enum}, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <Type>
   *   the {@link Class} type of {@code type} (automatically determined by {@code type}).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param type
   *   the expected {@link Class} type of {@code value}.
   * @param enumValueName
   *   the existent name of an {@link Enum} value.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return the found {@link Enum} value.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code enumValueName} is {@code null} or not found in {@code type}.
   */
  public static <Type extends Enum<Type>, ExceptionType extends Exception> Type optionalEnumValueName(
    final Class<Type> type,
    final String enumValueName,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(enumValueName, ()->Validate.enumValueName(type, enumValueName, exceptionSupplier));
  }

  /**
   * Validate if an array is {@code null} or is not empty and contains only one element, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} array (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return the first element of {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is empty or has more than one element.
   */
  public static <Value, ExceptionType extends Exception> Value optionalSingleton(
    final Value[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.singleton(value, exceptionSupplier));
  }

  /**
   * Validate if a {@link Collection} is {@code null} or is not empty and contains only one element, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} {@link Collection} element (automatically determined).
   * @param <ACollection>
   *   the type of {@code value} {@link Collection} (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return the first element of {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is empty or has more than one element.
   */
  public static <Value, ACollection extends Collection<Value>, ExceptionType extends Exception> Value optionalSingleton(
    final ACollection value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.singleton(value, exceptionSupplier));
  }

  /**
   * Validate if a {@code value} is {@code null} or it has at least a minimum length, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has less characters than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> String optionalMinLength(
    final int minLength,
    final String value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.minLength(minLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code boolean} array is {@code null} or it has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has less elements than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> boolean[] optionalMinLength(
    final int minLength,
    final boolean[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.minLength(minLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code byte} array is {@code null} or it has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has less elements than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> byte[] optionalMinLength(
    final int minLength,
    final byte[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.minLength(minLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code short} array is {@code null} or it has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has less elements than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> short[] optionalMinLength(
    final int minLength,
    final short[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.minLength(minLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code char} array is {@code null} or it has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has less elements than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> char[] optionalMinLength(
    final int minLength,
    final char[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.minLength(minLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code int} array is {@code null} or it has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has less elements than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> int[] optionalMinLength(
    final int minLength,
    final int[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.minLength(minLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code long} array is {@code null} or it has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has less elements than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> long[] optionalMinLength(
    final int minLength,
    final long[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.minLength(minLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code float} array is {@code null} or it has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has less elements than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> float[] optionalMinLength(
    final int minLength,
    final float[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.minLength(minLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code double} array is {@code null} or it has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has less elements than the minimum quantity expected.
   */
  public static <ExceptionType extends Exception> double[] optionalMinLength(
    final int minLength,
    final double[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.minLength(minLength, value, exceptionSupplier));
  }

  /**
   * Validate if a array is {@code null} or it has at least a minimum quantity of elements, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} array (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has less elements than the minimum quantity expected.
   */
  public static <Value, ExceptionType extends Exception> Value[] optionalMinLength(
    final int minLength,
    final Value[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.minLength(minLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@link Collection} is {@code null} or it has at least a minimum quantity of elements, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} element (automatically determined).
   * @param <ACollection>
   *   the type of {@code value} {@link Collection} (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param minLength
   *   the expected minimum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has less elements than the minimum quantity expected.
   */
  public static <Value, ACollection extends Collection<Value>, ExceptionType extends Exception> ACollection optionalMinLength(
    final int minLength,
    final ACollection value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.minLength(minLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code value} is {@code null} or it is smaller than a maximum length, or throw an exception supplied by {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> String optionalMaxLength(
    final int maxLength,
    final String value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.maxLength(maxLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code boolean} array is not {@code null} and has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} is {@code null} or has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> boolean[] optionalMaxLength(
    final int maxLength,
    final boolean[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.maxLength(maxLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code byte} array is {@code null} or it has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> byte[] optionalMaxLength(
    final int maxLength,
    final byte[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.maxLength(maxLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code short} array is {@code null} or it has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> short[] optionalMaxLength(
    final int maxLength,
    final short[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.maxLength(maxLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code char} array is {@code null} or it has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> char[] optionalMaxLength(
    final int maxLength,
    final char[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.maxLength(maxLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code int} array is {@code null} or it has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> int[] optionalMaxLength(
    final int maxLength,
    final int[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.maxLength(maxLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code long} array is {@code null} or it has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> long[] optionalMaxLength(
    final int maxLength,
    final long[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.maxLength(maxLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code float} array is {@code null} or it has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> float[] optionalMaxLength(
    final int maxLength,
    final float[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.maxLength(maxLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code double} array is {@code null} or it has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has more elements than the maximum quantity expected.
   */
  public static <ExceptionType extends Exception> double[] optionalMaxLength(
    final int maxLength,
    final double[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.maxLength(maxLength, value, exceptionSupplier));
  }

  /**
   * Validate if a array is {@code null} or it has less elements than a specified quantity, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} array (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has more elements than the maximum quantity expected.
   */
  public static <Value, ExceptionType extends Exception> Value[] optionalMaxLength(
    final int maxLength,
    final Value[] value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.maxLength(maxLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@link Collection} is {@code null} or it has less elements than a specified quantity, or throw an exception supplied by
   * {@code exceptionSupplier} otherwise.
   * 
   * @param <Value>
   *   the type of {@code value} element (automatically determined).
   * @param <ACollection>
   *   the type of {@code value} {@link Collection} (automatically determined).
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param maxLength
   *   the expected maximum size.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code exceptionSupplier} is {@code null}.
   * @throws ExceptionType
   *   when {@code value} has more elements than the maximum quantity expected.
   */
  public static <Value, ACollection extends Collection<Value>, ExceptionType extends Exception> ACollection optionalMaxLength(
    final int maxLength,
    final ACollection value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    return Validate.optional(value, ()->Validate.maxLength(maxLength, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code value} is {@code null} or it matches to a regular exception pattern, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param pattern
   *   the expected {@code value} content pattern.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code pattern} or {@code exceptionSupplier} are {@code null}.
   * @throws ExceptionType
   *   when {@code value} does not match {@code pattern}.
   */
  public static <ExceptionType extends Exception> String optionalRegex(
    final String pattern,
    final String value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Objects.requireNonNull(pattern, ()->"pattern must not be null.");
    return Validate.optional(value, ()->Validate.regex(pattern, value, exceptionSupplier));
  }

  /**
   * Validate if a {@code value} is {@code null} or it matches to a regular exception pattern, or throw an exception supplied by {@code exceptionSupplier}
   * otherwise.
   * 
   * @param <ExceptionType>
   *   the type of thrown exception if validation fail (automatically determined by {@code exceptionSupplier}).
   * @param pattern
   *   the expected {@code value} content pattern.
   * @param value
   *   the value or expression that will be checked.
   * @param exceptionSupplier
   *   generate an exception when validation fails.
   * 
   * @return {@code value}.
   * 
   * @throws NullPointerException
   *   when {@code pattern} or {@code exceptionSupplier} are {@code null}.
   * @throws ExceptionType
   *   when {@code value} does not match {@code pattern}.
   */
  public static <ExceptionType extends Exception> String optionalRegex(
    final Pattern pattern,
    final String value,
    final Supplier<ExceptionType, ExceptionType> exceptionSupplier
  ) throws NullPointerException, ExceptionType {
    Objects.requireNonNull(pattern, ()->"pattern must not be null.");
    return Validate.optional(value, ()->Validate.regex(pattern, value, exceptionSupplier));
  }

}
