// This file is part of lightsvc Java Microservices Components (lightsvc).
//
// lightsvc Java Microservices Components (lightsvc) is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lightsvc Java Microservices Components (lightsvc) is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with lightsvc Java Microservices Components (lightsvc). If not, see <http://www.gnu.org/licenses/>.
package lightsvc.err;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

import lightsvc.err.Exceptions.Supplier;
import lightsvc.err.Exceptions.Translation;

/**
 * <p>
 * An special Exception meant to represent an unique error that happens only in one location of an application codebase.
 * </p>
 * <p>
 * This exception and its utility methods are meant to replace the standard Java exception handling mechanism {@code try...catch and rethrows} in a smaller and
 * semantic way.
 * </p>
 * <p>
 * For each operation there must be an {@link Enum} with its values representing every different operation error that can be returned. Each {@link Enum} value
 * must be mapped to a specific line of code, and this value is stored in the {@link Err} exception in its {@link #value} attribute.
 * </p>
 * <p>
 * Additionally, It is necessary to ensure that, inside each operation, any exception thrown by its subroutines should be translated to an error of current
 * operation. That way, no abstraction of the operation internals are leaked to the caller and upper layers of code and the caller handle only the
 * operation interface errors, not worrying with subroutine errors.
 * </p>
 * <p>
 * With both uniquely identifiable error and the error handling facade, the dependency on a full stack trace is greatly reduced. The {@link #value} can be used
 * in auditing, reporting
 * and logging, and, with it alone, it is possible to trace it back to a specific line of code with little code inspection.
 * </p>
 */
public class Err extends RuntimeException {

  @java.io.Serial
  private static final long serialVersionUID = 2024011121L;
  /**
   * A {@link Enum} representing a uniquely identifiable error in an application.
   */
  public final Enum<?> value;
  /**
   * Any value attached to an error that can detail the exception.
   */
  public final Serializable data;

  /**
   * {@link #Err()} constructor errors.
   */
  @SuppressWarnings("javadoc")
  public enum ErrNew {
    value_mustNotBeNull,
  }

  /**
   * Creates an error with unique {@link #value} identifier, attached {@link #data} and cause.
   * 
   * @param value
   *   A {@link Enum} representing a uniquely identifiable error in an application.
   * @param data
   *   Any value attached to an error that can detail the exception. It can be used in messages or special error handlers in upper layers of code.
   * @param cause
   *   an Throwable i case it was cause by another exception. Normally set by a translation.
   * 
   * @throws Err
   *   {@link ErrNew#value_mustNotBeNull} if {@code value} is {@code null}.
   * 
   * @see {@link #value}
   * @see {@link #data}
   */
  @SuppressWarnings("javadoc")
  public Err(final Enum<?> value, final Serializable data, final Throwable cause) throws Err {
    super(((Supplier<String, Err>) ()-> {
      Validate.notNull(value, ()->new Err(ErrNew.value_mustNotBeNull));
      return Err.messageFromEnumValue(value);
    }).get(), cause, true, cause == null); // Java constructor rules suck!
    this.value = value;
    this.data = data;
  }

  /**
   * Creates an error without attached data and cause.
   * 
   * @param value
   *   A {@link Enum} representing a uniquely identifiable error in an application.
   * 
   * @throws Err
   *   {@link ErrNew#value_mustNotBeNull} if {@code value} is {@code null}.
   */
  public Err(final Enum<?> value) throws Err {
    this(value, null, null);
  }

  /**
   * Extract the Java fully qualified {@link Enum#name()} from {@code value} using "." notation instead of "$" for subclasses.
   * 
   * @param value
   *   to extract the Java fully qualified name
   * 
   * @return the Java fully qualified {@link Enum#name()} if {@code value} is not {@code null} or {@code null} otherwise.
   */
  public static String messageFromEnumValue(final Enum<?> value) {
    if (value == null) {
      return null;
    }
    return value.getDeclaringClass().getName().replace('$', '.') + "." + value.name();
  }

  /**
   * {@link #hasValue()} method errors.
   */
  @SuppressWarnings("javadoc")
  public enum ErrHasValue {
    values_mustNotBeEmpty,
    values_items_mustNotBeNull,
  }

  /**
   * <p>
   * Create a {@link Predicate} that tests if an {@link Err} type exception {@link Err#value} is one of {@code values}.
   * </p>
   * <p>
   * It is meant to be used with {@link Exceptions#translate(Supplier, lightsvc.err.Exceptions.Translation...)} methods and
   * {@link Exceptions.Translation#exceptionPredicate()} to test if an {@link Exception} is one specific {@link #value} of {@link Err}.
   * </p>
   * 
   * @param values
   *   that are tested with {@link Err#value} type exceptions in the returned {@link Predicate}
   * 
   * @return a {@link Predicate} that return {@code true} if an {@link Err} type exception {@link Err#value} is one of {@code values} or {@code false}
   *   otherwise.
   * 
   * @throws Err
   *   {@link ErrHasValue#values_mustNotBeEmpty} if {@code values} is {@code null} or empty.
   * @throws Err
   *   {@link ErrHasValue#values_items_mustNotBeNull} if any items of {@code values} are {@code null}.
   */
  @SafeVarargs
  @SuppressWarnings("java:S6201") // SonarLint: Pattern Matching instanceof should be used. (Codecov does not understand pattern matching instanceof)
  public static Predicate<Exception> hasValue(final Enum<?>... values) throws Err {
    Validate.notEmpty(values, ()->new Err(ErrHasValue.values_mustNotBeEmpty));
    Validate.is(Arrays.stream(values).allMatch(Objects::nonNull), ()->new Err(ErrHasValue.values_items_mustNotBeNull));
    return e->(e instanceof Err) && Arrays.stream(values).anyMatch(((Err) e).value::equals);
  }

  /**
   * {@link #from(Enum...)} method errors.
   */
  @SuppressWarnings("javadoc")
  public enum ErrFrom {
    values_mustNotBeEmpty,
    values_items_mustNotBeNull,
  }

  /**
   * <p>
   * Construct an instance of {@link TranslationBuilder} that allows a more semantic way to create a {@link Translation}.
   * </p>
   * <p>
   * It is suggested to use this method imported statically.
   * </p>
   * 
   * @param values
   *   tests translate method supplier thrown {@link Err} against this values.
   * 
   * @return an {@link TranslationBuilder} that provides a more semantic way to create a {@link Translation}.
   * 
   * @throws Err
   *   {@link ErrFrom#values_mustNotBeEmpty} if values is {@code null} or empty.
   * @throws Err
   *   {@link ErrFrom#values_items_mustNotBeNull} if any item inside values is {@code null}.
   * 
   * @see Exceptions#translate(Supplier, Translation...)
   * @see TranslationBuilder#to(Enum)
   */
  @SafeVarargs
  public static TranslationBuilder from(final Enum<?>... values) throws Err {
    // Cannot use translation instead of the following validators to avoid recursion.
    Validate.notEmpty(values, ()->new Err(ErrFrom.values_mustNotBeEmpty));
    Validate.is(Arrays.stream(values).allMatch(Objects::nonNull), ()->new Err(ErrFrom.values_items_mustNotBeNull));
    return new TranslationBuilder(values);
  }

  /**
   * <p>
   * An intermediate object that allows a more semantic way to create a {@link Translation} using {@link Err}.
   * </p>
   * <p>
   * Created by {@link #from(Enum...)} method, and not commonly used directly by applications that use this library.
   * </p>
   * 
   * @see Exceptions#translate(Supplier, Translation...)
   * @see #from(Enum...)
   * @see TranslationBuilder#to(Enum)
   */
  // SonarLint: Inner classes should not have too many lines of code. (Many shortcut methods in this case. Removing them will degrade developer experience.)
  @SuppressWarnings("java:S2972")
  public static class TranslationBuilder extends Exceptions.Translation.Builder {

    /**
     * {@link #TranslationBuilder(Enum...)} constructor errors.
     */
    @SuppressWarnings("javadoc")
    public enum ErrNew {
      values_mustNotBeEmpty,
      values_items_mustNotBeNull,
    }

    /**
     * <p>
     * Constructor of {@link TranslationBuilder}, an intermediate object that allows a more semantic way to create a {@link Translation} using {@link Err}.
     * </p>
     * <p>
     * Created by {@link Err#from(Enum...)} method, and not commonly used directly by applications that use this library.
     * </p>
     * 
     * @param values
     *   the {@link Enum} values to be used as predicate in translation.
     * 
     * @throws Err
     */
    protected TranslationBuilder(final Enum<?>... values) throws Err {
      super(
        // Cannot use translation instead of the following validators to avoid recursion. Java constructor super() usage sucks!
        ((Supplier<Predicate<Exception>, Err>) ()-> {
          Validate.notEmpty(values, ()->new Err(ErrNew.values_mustNotBeEmpty));
          Validate.is(Arrays.stream(values).allMatch(Objects::nonNull), ()->new Err(ErrNew.values_items_mustNotBeNull));
          return Err.hasValue(values);
        }).get()
      );
    }

    /**
     * {@link TranslationBuilder#to(Enum)} methods family errors.
     */
    @SuppressWarnings("javadoc")
    public enum ErrTo {
      value_mustNotBeNull,
      dataFn_mustNotBeNull,
    }

    /**
     * Creates a {@link Translation} using {@code values} as a predicate passed in {@link Err#from(Enum...)} and translate it to a new Err using {@code value}
     * and {@code dataFn}
     * 
     * @param value
     *   the output translated value if the predicate triggers the translation.
     * @param dataFn
     *   a {@link Function} to supply any attached data to the {@link Err} if the predicate triggers the translation.
     * 
     * @return a new {@link Translation} using {@link Err#from(Enum...)} {@values} as predicate and a new Err using {@code value} and {@code dataFn} as a return
     *   if the translation is triggered.
     * 
     * @throws Err
     * 
     * @see Exceptions#translate(Supplier, Translation...)
     * @see Err#from(Enum...)
     * 
     * @example
     * 
     *   <pre>
     *   <code>
     *     package lightsvc.examples;
     *     
     *     import static lightsvc.err.Exceptions.Translation.when;
     *     
     *     import java.io.IOException;
     *     
     *     import lightsvc.err.Err;
     *     import lightsvc.err.Exceptions;
     *     import lightsvc.err.Validate;
     *     import lightsvc.util.DefaultValue;
     *     
     *     public class Example {
     *     
     *       public static void main(String[] args) {
     *         var childName = "";
     *         // Will throw an exception Err: ErrParentMethod.childName_mustBeNotEmpty
     *         parentMethod(childName);
     *       }
     *     
     *       public enum ErrParentMethod {
     *         childName_mustBeNotEmpty
     *       }
     *     
     *       public static void parentMethod(String childName) {
     *         Exceptions.translate(
     *           ()->childMethod(childName), //
     *           Err.from(ErrChildMethod.name_mustBeNotEmpty).<strong>to(
     *             ErrParentMethod.childName_mustBeNotEmpty, 
     *             err->"Attached data: " + DefaultValue.of(err.data, "").toString()
     *           )</strong>
     *         );
     *       }
     *     
     *       public enum ErrChildMethod {
     *         name_mustBeNotEmpty
     *       }
     *     
     *       public static void childMethod(String name) throws IOException {
     *         Validate.notEmpty(name, ()->new Err(ErrChildMethod.name_mustBeNotEmpty, name, null));
     *       }
     *     
     *     }
     *   </code>
     *   </pre>
     */
    public Translation<Err> to(final Enum<?> value, final Function<Err, ? extends Serializable> dataFn) throws Err {
      Validate.notNull(value, ()->new Err(ErrTo.value_mustNotBeNull));
      Validate.notNull(dataFn, ()->new Err(ErrTo.dataFn_mustNotBeNull));
      return super.to(e->Exceptions.shouldNotHappen(()->new Err(value, dataFn.apply((Err) e), e)));
    }

    /**
     * Creates a {@link Translation} using {@code values} as a predicate passed in {@link Err#from(Enum...)} and translate it to a new Err using {@code value}
     * and {@code dataFn}
     * 
     * @param value
     *   the output translated value if the predicate triggers the translation.
     * @param data
     *   any data to be attached to the {@link Err}.
     * 
     * @return a new {@link Translation} using {@link Err#from(Enum...)} {@values} as predicate and a new Err using {@code value} and {@code data} as a return
     *   if the translation is triggered.
     * 
     * @throws Err
     * 
     * @see Exceptions#translate(Supplier, Translation...)
     * @see Err#from(Enum...)
     * 
     * @example
     * 
     *   <pre>
     *   <code>
     *     package lightsvc.examples;
     *     
     *     import static lightsvc.err.Exceptions.Translation.when;
     *     
     *     import java.io.IOException;
     *     
     *     import lightsvc.err.Err;
     *     import lightsvc.err.Exceptions;
     *     import lightsvc.err.Validate;
     *     
     *     public class Example {
     *     
     *       public static void main(String[] args) {
     *         var childName = "";
     *         // Will throw an exception Err: ErrParentMethod.childName_mustBeNotEmpty
     *         parentMethod(childName);
     *       }
     *     
     *       public enum ErrParentMethod {
     *         childName_mustBeNotEmpty
     *       }
     *     
     *       public static void parentMethod(String childName) {
     *         Exceptions.translate(
     *           ()->childMethod(childName), //
     *           Err.from(ErrChildMethod.name_mustBeNotEmpty).<strong>to(ErrParentMethod.childName_mustBeNotEmpty, "Attached data")</strong>
     *         );
     *       }
     *     
     *       public enum ErrChildMethod {
     *         name_mustBeNotEmpty
     *       }
     *     
     *       public static void childMethod(String name) throws IOException {
     *         Validate.notEmpty(name, ()->new Err(ErrChildMethod.name_mustBeNotEmpty);
     *       }
     *     
     *     }
     *   </code>
     *   </pre>
     */
    public Translation<Err> to(final Enum<?> value, final Serializable data) throws Err {
      Validate.notNull(value, ()->new Err(ErrTo.value_mustNotBeNull));
      return super.to(e->new Err(value, data, e));
    }

    /**
     * Creates a {@link Translation} using {@code values} as a predicate passed in {@link Err#from(Enum...)} and translate it to a new Err using {@code value}
     * and {@code dataFn}
     * 
     * @param value
     *   the output translated value if the predicate triggers the translation.
     * 
     * @return a new {@link Translation} using {@link Err#from(Enum...)} {@values} as predicate and a new Err using {@code value} as a return
     *   if the translation is triggered.
     * 
     * @throws Err
     * 
     * @see Exceptions#translate(Supplier, Translation...)
     * @see Err#from(Enum...)
     * 
     * @example
     * 
     *   <pre>
     *   <code>
     *     package lightsvc.examples;
     *     
     *     import static lightsvc.err.Exceptions.Translation.when;
     *     
     *     import java.io.IOException;
     *     
     *     import lightsvc.err.Err;
     *     import lightsvc.err.Exceptions;
     *     import lightsvc.err.Validate;
     *     
     *     public class Example {
     *     
     *       public static void main(String[] args) {
     *         var childName = "";
     *         // Will throw an exception Err: ErrParentMethod.childName_mustBeNotEmpty
     *         parentMethod(childName);
     *       }
     *     
     *       public enum ErrParentMethod {
     *         childName_mustBeNotEmpty
     *       }
     *     
     *       public static void parentMethod(String childName) {
     *         Exceptions.translate(
     *           ()->childMethod(childName), //
     *           Err.from(ErrChildMethod.name_mustBeNotEmpty).<strong>to(ErrParentMethod.childName_mustBeNotEmpty)</strong>
     *         );
     *       }
     *     
     *       public enum ErrChildMethod {
     *         name_mustBeNotEmpty
     *       }
     *     
     *       public static void childMethod(String name) throws IOException {
     *         Validate.notEmpty(name, ()->new Err(ErrChildMethod.name_mustBeNotEmpty);
     *       }
     *     
     *     }
     *   </code>
     *   </pre>
     */
    public Translation<Err> to(final Enum<?> value) throws Err {
      Validate.notNull(value, ()->new Err(ErrTo.value_mustNotBeNull));
      return super.to(e->new Err(value, null, e));
    }

  }

}
