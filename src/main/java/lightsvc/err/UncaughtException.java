// This file is part of lightsvc Java Microservices Components (lightsvc).
//
// lightsvc Java Microservices Components (lightsvc) is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lightsvc Java Microservices Components (lightsvc) is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with lightsvc Java Microservices Components (lightsvc). If not, see <http://www.gnu.org/licenses/>.
package lightsvc.err;

/**
 * <p>
 * An exception type representing errors that were not handled, or not expected to happen in a specific piece of code.
 * </p>
 * <p>
 * It wraps the original exception It is considered a bad practice to handle exceptions of this type in other place than the uppermost layer of code of a
 * program (usually a high level javax.servlet.Filter).
 * </p>
 * 
 * @see Exceptions#shouldNotHappen(lightsvc.err.Exceptions.Supplier)
 * @see Exceptions#translate(lightsvc.err.Exceptions.Supplier, lightsvc.err.Exceptions.Translation...)
 * 
 * @author Tiago van den Berg
 */
public class UncaughtException extends RuntimeException {

  private static final long serialVersionUID = 2023091401;

  /**
   * Constructor that wraps the original exception.
   * 
   * @param cause
   *   Exception to be wrapped.
   * 
   * @see RuntimeException#RuntimeException(Throwable)
   */
  public UncaughtException(Throwable cause) {
    super("", cause);
  }

}
