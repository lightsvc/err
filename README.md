# lighsvc err components

## Description
This project contains...

## Usage

### Create a dependency on Maven

In your project pom.xml:

    <dependencies>
        
        <!-- Other project dependencies... -->
    
        <!-- Insert this dependency: -->
        <dependency>
            <groupId>lightsvc</groupId>
            <artifactId>err</artifactId>
            <version>---USE THE LAST VERSION HERE---</version>
        </dependency>
        
        <!-- Other project dependencies... -->
    </dependencies>

## Support

* Check the [docs](https://gitlab.com/lightsvc/err/-/tree/master/docs), especially the [How-To's](https://gitlab.com/lightsvc/err/-/tree/master/docs/how-tos.md "How-to's"). They provide solutions to the most common questions.
* Report bugs and open features request at [GitLab's integrated issue tracker](https://gitlab.com/lightsvc/err/-/issues) --- Please ask first in community chats or search for other similar issues already open.

## Contributing

All lightsvc components are open to contributions. 

Check the support channels to contact the community.

## Authors and acknowledgment

- Tiago van den Berg

## License

Licensed under the GNU Lesser General Public License, Version 3 <COPYING.LESSER or https://www.gnu.org/licenses/lgpl-3.0.html> 
